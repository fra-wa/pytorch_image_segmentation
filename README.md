## This is an old copied version of my AiSeg project

For
- 3D support
- multi GPU
- more than 100% faster
- user interface
- and much more

**check out the official project here:** https://gitlab.com/fra-wa/aiseg

# Easy Pytorch Segmentation

___

A project to train and use 2D image segmentation CNNs

### Features 
- **38 2D supported, tested and easy extractable models (way more with different backbones)**
  - **You need a model? Simply [copy](#copy) it (but cite the authors and this project!)**
- **Online and offline data augmentation** 
- **Training by one command**
- **Inference by one command**
- **One-liner for installation**

# Table of Contents:

---

1. [**Overview**](#overview)
    - [Models](#models)
2. [**Usage**](#usage)
    - [Installation](#install)
    - [Data preparation](#data)
    - [Data augmentation](#augmentation)
    - [Training](#training)
    - [Inference: Use a trained model](#inference)
3. [**Citation**](#citation)

<a name="overview"></a>
# Overview
- If **source is given**, the model is not implemented by myself but copied, bug freed and the code is changed more or 
  less to fit into this project (GroupNorm was also added).
- If **paper is given**, I reimplemented the network or due to bugs in the original implementation and/or I heavily 
  refactored a networks code.
- If **a backbone** is given, you can pass one of them during the training guide.

### Which one to choose?:
I recommend:
- UNet / UNet 3+ with one of the ResNe(X)t backbones or DeepLabV3+

<a name="models"></a>
### Networks

| Model Name | Possible Backbones| Source |
|---|---|---|
| Attention UNet    |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) |
| BiSeNet           |                                                                           | [source: zllrunning](https://github.com/zllrunning/face-parsing.PyTorch) |
| BiSeNetV2         |                                                                           | [source: CoinCheung](https://github.com/CoinCheung/BiSeNet) |
| DANet             | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet) |
| DRANet            | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet) |
| DeepLab V3+       | drn, xception, resnet101                                                  | [source: jfzhang95](https://github.com/jfzhang95/pytorch-deeplab-xception) |
| DenseASPP121      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP) |
| DenseASPP161      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP) |
| DenseASPP169      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP) |
| DenseASPP201      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP) |
| DenseNet57        |                                                                           | [source: ilsang](https://github.com/ilsang/PyTorch-SE-Segmentation) |
| DenseNet67        |                                                                           | [source: ilsang](https://github.com/ilsang/PyTorch-SE-Segmentation) |
| DenseNet103       |                                                                           | [source: ilsang](https://github.com/ilsang/PyTorch-SE-Segmentation) |
| DFANet            |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) |
| DFN               |                                                                           | [source: ycszen](https://github.com/ycszen/TorchSeg) |
| DUC - HDC         |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| ENet              |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| ERFNet            |                                                                           | [source: Eromera](https://github.com/Eromera/erfnet_pytorch) |
| ESPNet            |                                                                           | [source: sacmehta](https://github.com/sacmehta/ESPNet) |
| ExtremeC3Net      |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation) |
| Fast-SCNN         |                                                                           | [source: DeepVoltaire](https://github.com/DeepVoltaire/Fast-SCNN) |
| FCN 8             |                                                                           | [source: wkentaro](https://github.com/wkentaro/pytorch-fcn) |
| FCN 16            |                                                                           | [source: wkentaro](https://github.com/wkentaro/pytorch-fcn) |
| FCN 32            |                                                                           | [source: wkentaro](https://github.com/wkentaro/pytorch-fcn) |
| GCN               | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| GSCNN             | wider_resnet38_a2 (not changeable)                                        | [source: nv-tlabs](https://github.com/nv-tlabs/GSCNN) |
| HRNet             |                                                                           | [source: HRNet](https://github.com/HRNet/HRNet-Semantic-Segmentation) |
| ICNet             | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) |
| LadderNet         |                                                                           | [source: juntang-zhuang](https://github.com/juntang-zhuang/LadderNet) |
| LEDNet            |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) |
| OCNet             | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch) |
| PSANet based      | resnet50, resnet101, resnet152, resnext50, resnext101                     | Slightly customized. [paper](https://arxiv.org/ftp/arxiv/papers/2102/2102.07880.pdf) |
| PSPNet            | resnet50, resnet101, resnet152, resnext50, resnext101                     | Refactored, added better decoding [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| PSPDenseNet       | densenet121, densenet161, densenet169, densenet201                        | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| R2-Attention UNet |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) |
| R2UNet            |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation) |
| SegNet            |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| SegResNet         | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| SINet             |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation) |
| UNet              |                                                                           | [paper](https://arxiv.org/pdf/1505.04597.pdf) |
| UNet ResNe(X)t    | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Build by myself but not new. |
| UNet ++           |                                                                           | [source: 4uiiurz1](https://github.com/4uiiurz1/pytorch-nested-unet) |
| UNet 3+           |                                                                           | [paper](https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf) |
| UNet 3+ ResNe(X)t | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Customized, no code was provided. [paper](https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf) |
| UperNet           | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |

<a name="copy"></a>
### How to copy:
To **use them in your project**: copy the base_model.py, the model_constants.py, the utils.py and of course
the related model folder.

Also: **Cite the authors!** That's it.

Attention: binary classification of background and foreground expects num_classes=2! Change that if needed.

<a name="usage"></a>
# Usage

<a name="install"></a>
## Installation
1. Use Python >= 3.8
2. [Create a virtual python environment](https://docs.python.org/3/tutorial/venv.html) and activate it
3. cd to `../pytorch_image_segmentation/` and call `python install_requirements.py` to install all needed packages and to get 
   the correct pytorch library.
   
<a name="data"></a>
## Data preparation
- Create a folder at the same level as `pytorch_image_segmentation` called `segmentation_datasets` and prepare your data as follows:
- Images and masks must have the same name (not ending)! Masks format must be lossless (png, bmp, tiff, ...)! 
- If you have validation data, put your it inside here: `your_dataset_folder/validation`
- If you have no explicit validation data, the validation data will be extracted from the training data. To reproduce 
  the separation, set reproduce to True during the guide! I recommend preselecting the validation data to enable 
  training continuation from checkpoints without reproduce=True (this sets the random seeds to 0).
- Here is an example of the folder structure (with validation data):
    ```
    segmentation_datasets
    └───your_dataset
        └───images
        │   img_0001.jpg
        │   img_0002.jpg
        │   ...
        │
        └───masks
        │   mask_0001.tif
        │   mask_0002.tif
        │   ...
        │ 
        └───validation (optional, otherwise validation will be created from images and masks)
            └───images
            │   val_img_0001.png
            │   val_img_0002.png
            │   ...
            │
            └───masks
                val_mask_0001.png
                val_mask_0002.png
                ...
    ```

<a name="augmentation"></a>
## Online Augmentation

**What it does**

During training, you can activate online data augmentation which will augment each image randomly using a given 
probability for each augmentation function:

First, you can specify a global probability (during the guide): This is the probability of an input getting 
processed by the online augmentation pipeline (input is one part of the current batch).

Inside the pipeline, an image will be augmented using a chain of different augmentation techniques. Each technique will
be applied with a given probability. You can set your own probabilities using a config file. See 
`pytorch_image_segmentation/configs/online_aug` for examples.

## Offline Augmentation

**What it does**

If you dont want to use random online augmentation, you can augment your data before training.

By default, you are able to create **more than 2k** images **out of a single grayscale** image and **more than 6k**
images **out of one rgb** image. At the guide, enable help to see what is possible.

The augmentation uses many own procedures combined with the great [albumentations](https://pypi.org/project/albumentations/) 
library. Check them out!

**start the augmentation**

1. Activate the virtual environment
2. cd to `../pytorch_image_segmentation/pytorch_segmentation/` and call:
   - 2D: `python start_image_augmentation.py`
3. Follow the guide (choose your parameters and start)

A hint for you (copied out of a paper to train a GAN. You should consider testing if a specific augmentation helps!):
> " [...] color transforms were modestly beneficial, while image-space filtering,
noise, and cutout were not particularly useful. In this case, the best results were obtained using strong
augmentations." [<cite>[Tero Karras et al.]</cite>](https://arxiv.org/pdf/2006.06676.pdf)

#### performed 2D Operations:
1. Geometric (total: 288 created images):
   - First 1 image will be rotated 11 times, each by 30 degrees (full 360° rotation) (original + 11 rotations = 12)
   - Now all images are random resized &#8594; 12 rotations + 12 resized images &#8594; 24
   - Base operations:
        - rotate
        - resize
   - Now additional operations are performed on each of the 24 images:
        - tilt backwards
        - tilt forward
        - tilt left
        - tilt right
        - elastic distortion
        - grid distortion
        - optical distortion
        - random crop
        - random grid shuffle
        - squeeze height
        - squeeze width
2. Pixel manipulations (performed on each original and geometric augmented image):
    - Gray (8 operations &#8594; 288 + 288 * 8 = 2592):
        - to gray and adaptive histogram
        - contrast and brightness
        - gaussian filter
        - gaussian noise
        - to gray and histogram normalization
        - to gray and non local means filter
        - sharpen
        - random erasing
    - RGB: all 8 gray operations plus the following 12 (288 + 288 * 8 + 288 * 12 = 6048):
        - channel shuffle
        - color to hsv
        - iso noise
        - random fog
        - random rain
        - random shadow
        - random snow
        - random sun flair
        - rgb shift
        - solarize
        - to gray
        - to sephia
        
#### Info:
<sup>The augmentation will do some operations which might create too small images. In this case the images are 
not saved and the real number of created samples might be smaller.</sup>

<sup>If you set a maximum number of created images, for example 250, only a few random operations will be 
executed. However, a rotation will be done in every case. The additional operations will be selected randomly so that 
you get the most out of your images.</sup>

<a name="training"></a>
## Training

**start the training**

1. Activate the virtual environment
2. cd to `../pytorch_image_segmentation/pytorch_segmentation/` and call `python start_training.py`
3. Follow the guide (choose your parameters and start the training)

<sup>One can also change the parameters directly at the start_training.py, but it's safer to use the guide</sub>

<sup>You can find valid configurations/parameters for each model at `configs/example_configs_a100_40gb/` for an A100 
with 40 GB VRAM</sub>

When started the process, the program will preprocess your training data to fit best to your model (slicing images into 
sub images, if training data shape of images is too large or resizing training data if images are too small).
The original data is kept.

**continue the training**

Since checkpoints are saved, you can continue the training at any time.

1. Activate the virtual environment
2. cd to `../pytorch_image_segmentation/pytorch_segmentation/` and call `python continue_training.py`
3. Follow the guide (choose your parameters and start the training)
   
<a name="inference"></a>
## How to use a trained model?:
- cd to `../pytorch_image_segmentation/pytorch_segmentation/` and call `python start_prediction.py`
- follow the guide. Basically you only need to change the folder and the path to the model (.pt file).
- It will automatically segment the whole folder.
- If you choose: save_color=False, you will get a mask where each class belongs to one gray value like: 0, 1, 2, ..., n.
   Otherwise, you will get a human-readable mask with nice colors or, by setting nice_color=False, simpler colors for 
  post-processing:
  ![decoding_colors](/readme_images/decoding_colors.png)

<a name="citation"></a>
# Citation

If you use this code or the results for your research, please provide the link to this repository.
