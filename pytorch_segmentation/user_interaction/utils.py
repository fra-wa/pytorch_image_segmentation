import os


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        return v


def confirm_input_by_choice(choices, answer):
    """

    Args:
        choices: list like ['choice 1', 'choice 2', ..] choices will be converted to string.
        answer: the initial answer of the user

    Returns: the answer as a string

    """
    # work around to accept enter or '' as enter key choice
    if '' in choices and 'enter' not in choices:
        choices.append('enter')
    elif 'enter' in choices and '' not in choices:
        choices.append('')

    for idx, choice in enumerate(choices):
        choice = str(choice)
        choices[idx] = choice.lower()

    choice_string = ', '.join(choice for choice in choices if choice)

    while answer not in choices:
        answer = input(f'{answer} is not a valid choice. Select one of these: {choice_string}!\n')
        answer = answer.lower()

    return answer


def convert_input_to_expected_type(answer, expected_type, exception_key=None):
    if exception_key is not None and answer == exception_key:
        return answer

    try:
        if expected_type == bool:
            answer = str2bool(answer)
        else:
            answer = expected_type(answer)  # this is a type conversion like type(type_of_some_var)(var_to_convert)
    except ValueError:
        pass

    return answer


def confirm_input_by_type(answer, expected_type, exception_key=None):
    """

    Args:
        expected_type: type to convert the answer into
        answer: the initial answer given by the user
        exception_key: a key to stop the confirmation and simply return the pressed key

    Returns: the answer converted to the expected type

    """
    if not isinstance(expected_type, type):
        raise ValueError('expected_type must be a type class.')

    answer = convert_input_to_expected_type(answer, expected_type, exception_key)

    while not isinstance(answer, expected_type) and not exception_key == answer:
        if exception_key is not None:
            answer = input(f'Your input: {answer} could not be parsed to type "{expected_type.__name__}" nor is it '
                           f'"{exception_key}". Please pass a value like that type or {exception_key}.\n')
        else:
            answer = input(f'Your input: {answer} could not be parsed to type "{expected_type.__name__}". '
                           f'Please pass a value like that type.\n')

        answer = convert_input_to_expected_type(answer, expected_type)

    return answer


def confirm_path_input(passed_path):
    showed_help = False
    while not os.path.isdir(passed_path):
        print(f'There is no directory at: {passed_path}')
        if not showed_help:
            print('\nThe path must look like the default one. Press ctrl + c to stop.')
            showed_help = True
        passed_path = input('Please pass a valid path to an existing directory.\n')
    return passed_path
