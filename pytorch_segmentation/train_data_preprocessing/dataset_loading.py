import os

from .image_manipulation import multiprocess_images_and_masks_simple, check_correct_mask_color_space
from .image_manipulation import multiprocess_sequences_for_recurrent_and_3d
from .. import constants

from ..file_handling.utils import create_folder
from ..file_handling.utils import get_file_paths_in_folder
from ..file_handling.utils import get_sub_folders
from ..multicore_utils import multiprocess_function, get_max_process_count
from ..multicore_utils import slice_multicore_parts
from ..utils import get_cpu_memory
from ..utils import get_max_threads


def get_image_and_mask_sequences(images_folder, masks_folder, valid_images_folder, valid_masks_folder):
    """
    loads all volumes or sequences of all passed folders to a list of sub lists where each sublist represents a
    sequence or volume

    Args:
        images_folder:
        masks_folder:
        valid_images_folder:
        valid_masks_folder:

    Returns:

    """
    try:
        image_sequence_folders = get_sub_folders(images_folder, depth=1)
        mask_sequence_folders = get_sub_folders(masks_folder, depth=1)
    except FileNotFoundError:
        raise FileNotFoundError('Set up of image and masks sequences is wrong. Read the readme at: '
                                f'Usage to set up your training data correctly!')

    if not image_sequence_folders or not mask_sequence_folders:
        raise FileNotFoundError('Set up of image and masks sequences is wrong. Read the readme at: '
                                f'Usage to set up your training data correctly!')

    image_sequences = []
    mask_sequences = []

    for sub_folder in image_sequence_folders:
        images = get_file_paths_in_folder(sub_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        image_sequences.append(images)

    for sub_folder in mask_sequence_folders:
        masks = get_file_paths_in_folder(sub_folder, extension=constants.SUPPORTED_MASK_TYPES)
        mask_sequences.append(masks)

    if len(mask_sequences) != len(image_sequences):
        raise RuntimeError('Image and mask sequences are not equal')

    try:
        valid_images_folder = get_sub_folders(valid_images_folder, depth=1)
        valid_masks_folder = get_sub_folders(valid_masks_folder, depth=1)
    except FileNotFoundError:
        return image_sequences, mask_sequences, [], []

    valid_image_sequences = []
    valid_mask_sequences = []

    for sub_folder in valid_images_folder:
        valid_images = get_file_paths_in_folder(sub_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        valid_image_sequences.append(valid_images)

    for sub_folder in valid_masks_folder:
        valid_masks = get_file_paths_in_folder(sub_folder, extension=constants.SUPPORTED_MASK_TYPES)
        valid_mask_sequences.append(valid_masks)

    if len(valid_mask_sequences) != len(valid_image_sequences):
        raise RuntimeError('Validation image and mask sequences are not equal')

    return image_sequences, mask_sequences, valid_image_sequences, valid_mask_sequences


def multi_preprocess_train_data_for_simple_segmentation(image_paths,
                                                        mask_paths,
                                                        images_folder,
                                                        masks_folder,
                                                        load_gray,
                                                        input_size,
                                                        threads,
                                                        ):
    image_paths_map = slice_multicore_parts(image_paths, total_maps=threads)
    mask_paths_map = slice_multicore_parts(mask_paths, total_maps=threads)

    # check that slicing was correct
    for sub_i_list, sub_m_list in zip(image_paths_map, mask_paths_map):
        for im, ma in zip(sub_i_list, sub_m_list):
            name_i = os.path.basename(im).split('.')[0]
            name_m = os.path.basename(ma).split('.')[0]
            assert name_i == name_m

    images_folder_map = len(image_paths_map) * [images_folder]
    masks_folder_map = len(image_paths_map) * [masks_folder]
    load_gray_map = len(image_paths_map) * [load_gray]
    in_size_map = len(image_paths_map) * [input_size]
    process_nr_map = [i + 1 for i in range(len(image_paths_map))]

    maps = [
        image_paths_map,
        mask_paths_map,
        images_folder_map,
        masks_folder_map,
        load_gray_map,
        in_size_map,
        process_nr_map,
    ]

    results = multiprocess_function(multiprocess_images_and_masks_simple, maps)

    new_image_paths = []
    new_mask_paths = []

    for image_paths, mask_paths in results:
        new_image_paths += image_paths
        new_mask_paths += mask_paths

    return new_image_paths, new_mask_paths


def preprocess_train_data_for_simple_segmentation(image_paths,
                                                  mask_paths,
                                                  input_size,
                                                  dataset_name,
                                                  channels,
                                                  valid_image_paths=None,
                                                  valid_mask_paths=None,
                                                  ):
    """
    Loads the provided images and masks
    Confirms, that sizes are correctly
    Enlarges sizes if needed (bad...) or splites the images and masks into sub images and masks and saves more training
     files.
    Returns new file paths

    Args:
        image_paths:
        mask_paths:
        input_size:
        dataset_name:
        channels: image channels --> 1 gray, 3 rgb
        valid_image_paths: If validation data is given, pass the list of file paths
        valid_mask_paths: If validation data is given, pass the list of file paths

    Returns: list of image paths, list of mask paths

    """
    if valid_image_paths is None:
        valid_image_paths = []
    if valid_mask_paths is None:
        valid_mask_paths = []

    print('Slicing or resizing images to expected input size...')
    load_gray = True if channels == 1 else False
    if len(image_paths) != len(mask_paths):
        raise ValueError('Masks and images are not of equal size!')

    preprocessed_folder = os.path.join(constants.DATASET_FOLDER, dataset_name, 'in_size_' + str(input_size))
    images_folder = os.path.join(preprocessed_folder, 'images')
    masks_folder = os.path.join(preprocessed_folder, 'masks')
    validation_folder = os.path.join(preprocessed_folder, 'validation')
    validation_images_folder = os.path.join(validation_folder, 'images')
    validation_masks_folder = os.path.join(validation_folder, 'masks')

    if os.path.isdir(preprocessed_folder):
        print(f'Found preprocessed data! If something changed, delete the folder {preprocessed_folder}')
        return get_image_and_mask_sequences(
            images_folder, masks_folder, validation_images_folder, validation_masks_folder
        )

    create_folder(preprocessed_folder)
    create_folder(images_folder)
    create_folder(masks_folder)

    if valid_image_paths:
        create_folder(validation_folder)
        create_folder(validation_images_folder)
        create_folder(validation_masks_folder)

    threads = get_max_threads()

    if threads == 1:
        # use same process, user seems to have less ram
        new_image_paths, new_mask_paths = multiprocess_images_and_masks_simple(
            image_paths, mask_paths, images_folder, masks_folder, load_gray, input_size, 1
        )
        if valid_image_paths:
            new_valid_image_paths, new_valid_mask_paths = multiprocess_images_and_masks_simple(
                valid_image_paths,
                valid_mask_paths,
                validation_images_folder,
                validation_masks_folder,
                load_gray,
                input_size,
                1,
            )
        else:
            new_valid_image_paths = []
            new_valid_mask_paths = []
        return new_image_paths, new_mask_paths, new_valid_image_paths, new_valid_mask_paths

    new_image_paths, new_mask_paths = multi_preprocess_train_data_for_simple_segmentation(
        image_paths, mask_paths, images_folder, masks_folder, load_gray, input_size, threads
    )
    if valid_image_paths:
        print('Preprocessing validation data.')
        threads = get_max_threads()
        new_valid_image_paths, new_valid_mask_paths = multi_preprocess_train_data_for_simple_segmentation(
            valid_image_paths,
            valid_mask_paths,
            validation_images_folder,
            validation_masks_folder,
            load_gray,
            input_size,
            threads,
        )
    else:
        new_valid_image_paths = []
        new_valid_mask_paths = []

    return new_image_paths, new_mask_paths, new_valid_image_paths, new_valid_mask_paths


def multi_preprocess_training_data_recurrent_and_3d(image_sequences,
                                                    mask_sequences,
                                                    images_folder,
                                                    masks_folder,
                                                    load_gray,
                                                    input_size,
                                                    threads):
    image_sequences_map = slice_multicore_parts(image_sequences, total_maps=threads)
    mask_sequences_maps = slice_multicore_parts(mask_sequences, total_maps=threads)

    # check that slicing was correct
    for idx, sequence_list in enumerate(image_sequences_map):
        for j, sequence_folder_list in enumerate(sequence_list):
            name = os.path.basename(os.path.dirname(sequence_folder_list[0]))
            assert name == os.path.basename(os.path.dirname(mask_sequences_maps[idx][j][0]))

    images_folder_map = len(image_sequences_map) * [images_folder]
    masks_folder_map = len(image_sequences_map) * [masks_folder]
    load_gray_map = len(image_sequences_map) * [load_gray]
    in_size_map = len(image_sequences_map) * [input_size]
    process_nr_map = [i + 1 for i in range(len(image_sequences_map))]

    maps = [
        images_folder_map,
        masks_folder_map,
        image_sequences_map,
        mask_sequences_maps,
        load_gray_map,
        in_size_map,
        process_nr_map,
    ]

    multiprocess_function(multiprocess_sequences_for_recurrent_and_3d, maps)


def preprocess_training_data_recurrent_and_3d(image_sequences,
                                              mask_sequences,
                                              channels,
                                              input_size,
                                              dataset_name,
                                              valid_image_sequences=None,
                                              valid_mask_sequences=None,
                                              ):
    """

    Args:
        image_sequences: returned by get_image_and_mask_sequences
        mask_sequences: returned by get_image_and_mask_sequences
        channels: 1 for gray, 3 rgb
        input_size: in pixels
        dataset_name: name of the dataset in unet_datasets/dataset_name
        valid_image_sequences: If validation dataset is given
        valid_mask_sequences: If validation dataset is given

    """
    if valid_image_sequences is None:
        valid_image_sequences = []
    if valid_mask_sequences is None:
        valid_mask_sequences = []

    load_gray = True if channels == 1 else False
    print('Slicing or resizing images to expected input size...')
    preprocessed_folder = os.path.join(constants.DATASET_FOLDER, dataset_name, 'in_size_' + str(input_size))

    images_folder = os.path.join(preprocessed_folder, 'images')
    masks_folder = os.path.join(preprocessed_folder, 'masks')
    validation_folder = os.path.join(preprocessed_folder, 'validation')
    validation_images_folder = os.path.join(validation_folder, 'images')
    validation_masks_folder = os.path.join(validation_folder, 'masks')

    create_folder(preprocessed_folder)
    create_folder(images_folder)
    create_folder(masks_folder)

    if valid_mask_sequences:
        create_folder(validation_folder)
        create_folder(validation_images_folder)
        create_folder(validation_masks_folder)

    # calculate with 2gb per process due to python modules and used images.
    # this is very conservative
    ram_used, ram_total = get_cpu_memory(in_gb=True)
    available = ram_total - ram_used
    threads = int(available/constants.MULTICORE_RAM_RESERVATION)
    usable_processes, total_processes = get_max_process_count(True)

    if threads > usable_processes:
        threads = usable_processes

    if threads == 1:
        # use same process, user seems to have less ram
        multiprocess_sequences_for_recurrent_and_3d(
            images_folder, masks_folder, image_sequences, mask_sequences, load_gray, input_size, 1
        )
        if valid_mask_sequences:
            multiprocess_sequences_for_recurrent_and_3d(
                validation_images_folder,
                validation_masks_folder,
                valid_image_sequences,
                valid_mask_sequences,
                load_gray,
                input_size,
                1,
            )
        return get_image_and_mask_sequences(
            images_folder, masks_folder, validation_images_folder, validation_masks_folder
        )

    multi_preprocess_training_data_recurrent_and_3d(
        image_sequences, mask_sequences, images_folder, masks_folder, load_gray, input_size, threads
    )
    if valid_image_sequences:
        print('Preprocessing validation data.')
        threads = get_max_threads()
        multi_preprocess_training_data_recurrent_and_3d(
            valid_image_sequences,
            valid_mask_sequences,
            validation_images_folder,
            validation_masks_folder,
            load_gray,
            input_size,
            threads,
        )
    print('Finished multiprocessing, loading new sequences')
    # load new image and mask sequences
    return get_image_and_mask_sequences(images_folder, masks_folder, validation_images_folder, validation_masks_folder)


def prepare_dataset(params):
    """
    Prepares the given dataset.
    If input images and masks are not of the correct model input size,
    this will create sub images or enlarges the images and masks.

    Returns:
        images (list): list of representing the data.
            In case of simple 2D data: list of file paths representing each input training image
            In case of 3D data: List of lists representing each volume or time sequence.
                Each volume or sequence is represented by a list of file paths representing the input images
        masks (list): list of representing the data; Structured like images
        valid_images (list): list of representing the data (might be empty if no valid data was given.
            Validation data will then be taken from the images and masks); Structured like images
        valid_masks (list): list of representing the data (might be empty if no valid data was given.
            Validation data will then be taken from the images and masks); Structured like images

    """

    if params.architecture not in constants.SUPPORTED_ARCHITECTURES:
        raise RuntimeError(f'{params.architecture} is not a supported architecture.')
    dataset_folder = os.path.join(constants.DATASET_FOLDER, params.dataset_name)

    images_folder = os.path.join(dataset_folder, 'images')
    masks_folder = os.path.join(dataset_folder, 'masks')
    valid_images_folder = os.path.join(dataset_folder, 'validation', 'images')
    valid_masks_folder = os.path.join(dataset_folder, 'validation', 'masks')

    preprocessed_folder = os.path.join(
        constants.DATASET_FOLDER,
        params.dataset_name,
        'in_size_' + str(params.input_size)
    )
    if os.path.isdir(preprocessed_folder):
        print(f'Found preprocessed folder of for current model: {preprocessed_folder}\n'
              f'If you changed something and an error occurs, remove the preprocessed folder')

        images_folder = os.path.join(preprocessed_folder, 'images')
        masks_folder = os.path.join(preprocessed_folder, 'masks')
        valid_images_folder = os.path.join(preprocessed_folder, 'validation', 'images')
        valid_masks_folder = os.path.join(preprocessed_folder, 'validation', 'masks')

        images = get_file_paths_in_folder(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_file_paths_in_folder(masks_folder, extension=constants.SUPPORTED_MASK_TYPES)

        try:
            print('Found user specified validation data.')
            valid_images = get_file_paths_in_folder(
                valid_images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
            valid_masks = get_file_paths_in_folder(valid_masks_folder, extension=constants.SUPPORTED_MASK_TYPES)
        except FileNotFoundError:
            valid_images = []
            valid_masks = []

        if not images or not masks:
            raise RuntimeError('Could not load any images and masks. Ensure setup of data. Read Readme.')
        return images, masks, valid_images, valid_masks

    print('\n--- Preparing dataset ---\n')

    try:
        images = get_file_paths_in_folder(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_file_paths_in_folder(masks_folder, extension=constants.SUPPORTED_MASK_TYPES)
        if len(images) != len(masks):
            raise RuntimeError('Count of images and masks are not equal!')
    except FileNotFoundError:
        raise FileNotFoundError('Read the readme at: Usage to set up your training data correctly!')

    try:
        print('Found user specified validation data.')
        valid_images = get_file_paths_in_folder(
            valid_images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES
        )
        valid_masks = get_file_paths_in_folder(valid_masks_folder, extension=constants.SUPPORTED_MASK_TYPES)
        if len(valid_images) != len(valid_masks):
            raise RuntimeError('Count of validation images and masks are not equal!')
    except FileNotFoundError:
        valid_images = []
        valid_masks = []

    is_valid = check_correct_mask_color_space(masks + valid_masks, params.classes, dataset_folder)
    if is_valid:
        print(f'Masks of folder {os.path.dirname(masks[0])} are valid')
        print(f'Masks of folder {os.path.dirname(valid_masks[0])} are valid')

    return preprocess_train_data_for_simple_segmentation(
        images, masks, params.input_size, params.dataset_name, params.channels, valid_images, valid_masks
    )
