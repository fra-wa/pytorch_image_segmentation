import numpy as np
import random
import time
import torch

from .data_loading import set_up_loaders, get_dataset_mean_and_std
from .data_sets import Dataset2D
from .profiler import TrainingProfiler
from .saving import get_backup_model_folder
from .saving import get_trained_model_folder
from .saving import save_history
from .saving import save_model
from .training_execution import train_loop, valid_loop
from .. import constants
from ..data_augmentation.online_augmentation_pipelines import ImageAugmentationPipeline
from ..data_augmentation.online_aug_config_loader import load_online_aug_config

from ..file_handling.move_to_backup import move_to_backup_folder
from ..file_handling.utils import create_folder
from ..file_handling.utils import folder_contains_files
from pytorch_segmentation.models.loading import get_model
from pytorch_segmentation.models.loading import load_checkpoint_to_continue_training
from ..train_data_preprocessing.dataset_loading import prepare_dataset
from ..user_interaction.parameters import FixedParameters
from ..user_interaction.user_information import show_best_five_stats
from ..user_interaction.user_information import show_stats
from ..utils import show_memory


def get_loss(params):
    if params.classes == 2:
        loss = torch.nn.BCEWithLogitsLoss().to(params.device)
    else:
        loss = torch.nn.CrossEntropyLoss().to(params.device)
    return loss


def start_training(parameters, continue_training=False, interact_with_user=True, fixed_end_epoch=False):
    """

    Args:
        parameters: Parameters instance (user interaction)
        continue_training:
        interact_with_user: for debug and continuation
        fixed_end_epoch: For continuation. If True, the params.epochs will be the maximum epoch and not added to the
            latest epoch. E.g. if params.epochs = 100 and latest checkpoint loaded was from epoch 98, then only 2
            further epochs will be executed.

    Returns:

    """
    if interact_with_user:
        params = parameters.start_interaction_guide()
    else:
        try:
            params = parameters.get_fixed_parameters()
        except AttributeError:
            # already got fixed parameters
            params = parameters

    if not isinstance(params, FixedParameters):
        raise RuntimeError('Expecting fixed parameters!')

    if params.device == 'cuda':
        params.device = f'cuda:{params.gpu}'

    if params.reproduce:
        print(
            '-------------------\n'
            'Attention: Some cuDNN functions implemented are non deterministic anyways.\n'
            'The end result of two runs will be different but close to each other.\n'
            'EXAMPLE Error at cross_entropy_loss:\n'
            "RuntimeError: SpatialClassNLLCriterion_updateOutput does not have a deterministic implementation,\n"
            "but you set 'torch.use_deterministic_algorithms(True)'\n"
            "The code uses deterministic functions if it is possible. If the error above is fixed, create an issue\n"
            '-------------------\n'
        )

        torch.manual_seed(constants.GLOBAL_SEED)
        torch.cuda.manual_seed(constants.GLOBAL_SEED)
        torch.cuda.manual_seed_all(constants.GLOBAL_SEED)  # if you are using multi-GPU.
        torch.backends.cudnn.benchmark = False
        torch.backends.cudnn.deterministic = True

        np.random.seed(constants.GLOBAL_SEED)
        random.seed(constants.GLOBAL_SEED)

    if params.show_ram:
        print(f'Initial ram usage of device: {params.device}')
        show_memory(params.device)

    if continue_training:
        params, model, optimizer_state, history = load_checkpoint_to_continue_training(params)
        optimizer = torch.optim.Adam(model.parameters())
        optimizer.load_state_dict(optimizer_state)

        # crucial!!
        # issue: using more VRam when loading from checkpoint than starting from scratch:
        # since loading is in external func, there is no problem but the state dict is heavy!
        # https://discuss.pytorch.org/t/gpu-memory-usage-increases-by-90-after-torch-load/9213/9
        # 0/11441 MiB  --> initial
        # 6270/11441 MiB  --> loaded checkpoint
        # 6270/11441 MiB  --> set up optimizer
        # 2060/11441 MiB  --> deleted optimizer_state dict
        del optimizer_state
        torch.cuda.empty_cache()

        start_epoch = history[-1][0]
        if not fixed_end_epoch:
            params.epochs = params.epochs + start_epoch

        history = {
            'epochs': [line[0] for line in history],
            'train_loss': [line[1] for line in history],
            'valid_loss': [line[2] for line in history],
            'train_acc': [line[3] for line in history],
            'valid_acc': [line[4] for line in history],
            # compatibility with old version (had no profiler information)
            'ma_train_acc': [line[5] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'ma_train_acc_slope': [line[6] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'aug_strength': [line[7] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'train_acc_ema': [line[8] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'valid_acc_ema': [line[9] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'train_ema_slopes': [line[10] for line in history] if len(history[0]) == 12 else start_epoch * [None],
            'valid_ema_slopes': [line[11] for line in history] if len(history[0]) == 12 else start_epoch * [None],
        }

    else:
        model = get_model(
            model_name=params.architecture,
            channels=params.channels,
            classes=params.classes,
            device=params.device,
            backbone=params.backbone,
            pretrained=params.pretrained,
            normalization=params.norm,
            in_size=params.input_size,
            print_information=True,
        )
        optimizer = torch.optim.Adam(model.parameters())
        start_epoch = 0
        history = {
            'epochs': [],
            'train_loss': [],
            'valid_loss': [],
            'train_acc': [],
            'valid_acc': [],
            'ma_train_acc': [],  # ma: moving average
            'ma_train_acc_slope': [],
            'aug_strength': [],
            'train_acc_ema': [],  # ema: exponential moving average
            'valid_acc_ema': [],
            'train_ema_slopes': [],
            'valid_ema_slopes': [],
        }

    if start_epoch == params.epochs:
        print(f'Already trained the model for {start_epoch} epochs. Stopping.')
        return

    if (params.norm == 'bn' and params.batch_size < 16) or (params.norm == 'gn' and params.batch_size >= 16):
        print(
            f'Warning: Using a batch size of {params.batch_size} and normalization {params.norm}.\n'
            f'Recommended usage: batch size >= 16: use BatchNorm (bn), batch size < 16: use GroupNorm (gn)'
        )

    print(
        f'The model has {round(model.get_parameter_count()/1000000, 1)} mio parameters ({model.get_parameter_count()}).'
    )

    loss_func = get_loss(params)

    dataset_mean, dataset_std = get_dataset_mean_and_std(params)
    image_paths, mask_paths, valid_images_paths, valid_masks_paths = prepare_dataset(params)
    training_loader, validation_loader = set_up_loaders(
        image_paths,
        mask_paths,
        params,
        valid_images_paths=valid_images_paths,
        valid_masks_paths=valid_masks_paths,
        dataset_mean=dataset_mean,
        dataset_std=dataset_std,
    )

    if not isinstance(training_loader.dataset, Dataset2D):
        raise ValueError('Error! Wrong Dataloader.Dataset found. Dataset must be Dataset2D!')

    model_folder = get_trained_model_folder(params)

    if not continue_training:
        # clean existing folder and move to backup
        backup_folder = get_backup_model_folder(params)

        create_folder(model_folder)
        create_folder(backup_folder)

        if folder_contains_files(model_folder, extension='all'):
            move_to_backup_folder(model_folder, backup_folder)

    epochs = params.epochs

    if params.show_ram:
        print(f'Ram usage after model and parameter loading of device: {params.device}')
        show_memory(params.device)
        print('')

    stop_indicator_calc_range = params.epochs // 5
    if stop_indicator_calc_range < 10:
        stop_indicator_calc_range = 10
    elif stop_indicator_calc_range > 25:
        stop_indicator_calc_range = 25
    stop_after_x_epochs_no_change = 10

    profiler = TrainingProfiler(
        history_dict=history,
        ema_range=params.aug_ema_range,
        initial_aug_strength=params.aug_strength,
        aug_start_epoch=params.aug_start_epoch,
        stop_after_x_epochs_no_ma_increment=stop_after_x_epochs_no_change,
        training_moving_avg_range=stop_indicator_calc_range,
        stop_slope_threshold=0.02,
    )

    augmentation_pipeline = None
    if params.online_aug:
        pipeline_kwargs = load_online_aug_config(params.dataset_name)
    else:
        pipeline_kwargs = {}

    if params.online_aug and params.architecture in constants.TWO_D_NETWORKS:
        augmentation_pipeline = ImageAugmentationPipeline(**pipeline_kwargs)

    if params.online_aug:
        augmentation_pipeline.global_strength = params.aug_strength

    training_loader.dataset.aug_pipeline = augmentation_pipeline

    header = None
    epoch = 0
    start_time = time.time()
    print('')
    for epoch in range(start_epoch, epochs):
        print(f'Epoch: {epoch + 1}/{epochs}')
        train_loss, train_acc, model = train_loop(params, model, training_loader, loss_func, optimizer)
        valid_loss, valid_acc, model = valid_loop(params, model, validation_loader, loss_func)

        profiler.update(epoch + 1, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc)

        i_should_stop = profiler.training_should_be_stopped() and params.auto_train_stop
        history, header = profiler.get_history()

        if (epoch + 1) % params.save_every == 0 or epoch + 1 == epochs or i_should_stop:
            save_model(params, history, model, optimizer, model_folder, dataset_mean, dataset_std)

        show_stats(history, epochs, start_epoch, start_time)
        if i_should_stop:
            print(
                f'Training accuracy is no more increasing for {stop_after_x_epochs_no_change} epochs. Stopped training.'
            )
            break

    if epoch >= 3:
        save_history(
            history, model_folder, header=header, ema_range=params.aug_ema_range, ma_range=stop_indicator_calc_range
        )
    show_best_five_stats(history)
