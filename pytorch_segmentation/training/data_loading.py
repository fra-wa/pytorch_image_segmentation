import cv2
import math
import numpy as np
import os
import random
import torch

from torch.utils.data.dataloader import DataLoader

from .data_sets import Dataset2D
from .. import constants
from ..file_handling.read import read_csv_to_list
from ..file_handling.utils import get_sub_folders, get_file_paths_in_folder
from ..file_handling.write import save_csv
from ..multicore_utils import slice_multicore_parts, multiprocess_function
from ..user_interaction.parameters import FixedParameters


class NotEnoughTrainingDataError(Exception):
    def __init__(self, *args, **kwargs):
        super(NotEnoughTrainingDataError, self).__init__(*args)


def multicore_get_files(folders, process_nr):
    files = []
    for i, folder in enumerate(folders):
        if process_nr == 0:
            print(f'Process 1: {i + 1}/{len(folders)}', end='\r')
        files += get_file_paths_in_folder(folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    if process_nr == 0:
        print('\nWaiting for other processes to finish.')
    return files


def multicore_std_and_mean_adder(files, channels, process_nr):
    if channels == 1:
        mean = 0
        std = 0

        for i, file in enumerate(files):
            if process_nr == 0:
                print(f'Process 1: {i + 1}/{len(files)}', end='\r')
            img = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
            mean += np.mean(img)
            std += np.std(img)
    else:
        mean = [0, 0, 0]
        std = [0, 0, 0]

        for i, file in enumerate(files):
            if process_nr == 0:
                print(f'Process 1: {i + 1}/{len(files)}', end='\r')
            img = cv2.imread(file, cv2.IMREAD_COLOR)
            mean[0] += np.mean(img[:, :, 2])
            mean[1] += np.mean(img[:, :, 1])
            mean[2] += np.mean(img[:, :, 0])

            std[0] += np.std(img[:, :, 2])
            std[1] += np.std(img[:, :, 1])
            std[2] += np.std(img[:, :, 0])
    if process_nr == 0:
        print('\nFinished, waiting for other processes to finish.')
    return mean, std, len(files)


def get_dataset_mean_and_std(params, checkpoint=None):
    if not isinstance(params, FixedParameters):
        raise ValueError('params must be a FixedParameters instance.')

    mean = None
    std = None

    if not params.norm_on_dataset:
        return mean, std

    if checkpoint is not None:
        try:
            mean = checkpoint['dataset_mean']
            std = checkpoint['dataset_std']
            return mean, std
        except KeyError:
            pass

    dataset_folder = os.path.join(constants.DATASET_FOLDER, params.dataset_name)
    mean_and_std_file = os.path.join(dataset_folder, 'dataset_mean_and_std.csv')
    if os.path.isfile(mean_and_std_file):
        mean_and_std = read_csv_to_list(mean_and_std_file)
        mean = mean_and_std[0]
        std = mean_and_std[1]
        if len(mean) == params.channels:
            return mean, std

    images_folder = os.path.join(dataset_folder, 'images')
    valid_images_folder = os.path.join(dataset_folder, 'validation', 'images')

    folders_to_check = get_sub_folders(images_folder)

    if folders_to_check:
        if os.path.isdir(valid_images_folder):
            folders_to_check += get_sub_folders(valid_images_folder)

        folders_map = slice_multicore_parts(folders_to_check)
        process_nr_map = list(range(len(folders_map)))

        print('Loading data folders')
        results = multiprocess_function(multicore_get_files, [folders_map, process_nr_map])

        files = []
        for result in results:
            files += result
    else:
        files = get_file_paths_in_folder(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

    files_map = slice_multicore_parts(files)
    channels_map = len(files) * [params.channels]
    process_nr_map = list(range(len(files)))

    print(f'Calculating std and mean of dataset.')
    mean_std_file_count = multiprocess_function(multicore_std_and_mean_adder, [files_map, channels_map, process_nr_map])

    if params.channels == 1:
        mean = 0
        std = 0
        file_count = 0

        for result in mean_std_file_count:
            mean += result[0]
            std += result[1]
            file_count += result[2]

        mean = (mean / file_count) / 255
        std = (std / file_count) / 255

        save_csv(
            mean_and_std_file, [[mean], [std]], f'mean (line 1) and std (line 2) of dataset: {params.dataset_name}'
        )
        mean = [mean]
        std = [std]

    else:
        # rgb!
        mean = [0, 0, 0]
        std = [0, 0, 0]
        file_count = 0

        for result in mean_std_file_count:
            mean[0] += result[0][0]
            mean[1] += result[0][1]
            mean[2] += result[0][2]

            std[0] += result[1][0]
            std[1] += result[1][1]
            std[2] += result[1][2]

            file_count += result[2]

        for i in range(3):
            mean[i] = (mean[i] / file_count) / 255
            std[i] = (std[i] / file_count) / 255

        save_csv(
            mean_and_std_file, [mean, std], f'mean (line 1) and std (line 2) of dataset: {params.dataset_name}'
        )

    return mean, std


def slice_sequence_or_files(sequence_or_files, training_percentage=None):
    """
    Splits up 80% training, 30% valid

    If uneven, test sequence will have one element less than valid
    """
    if training_percentage is None:
        training_percentage = 80

    train_size = int(round(training_percentage / 100, 2) * len(sequence_or_files))

    train_sequence = sequence_or_files[:train_size]
    valid_sequence = sequence_or_files[train_size:]

    if not train_sequence or not valid_sequence:
        raise NotEnoughTrainingDataError('Not enough training data to properly create train and valid datasets')

    return train_sequence, valid_sequence


def slice_data_to_sub_volumes_or_sequences(sequences, depth):
    """
    Creates as much volumes as possible out of the sequence

    Args:
        sequences: list of sub lists where each sub list contains the paths to slices of one volume/sequence
        depth: depth of the volumes

    Returns: list of sub list where each sub list represents one volume

    """
    volumes = []

    for vol_sequence in sequences:
        if len(vol_sequence) / depth < 1:
            folder_name = os.path.basename(os.path.dirname(vol_sequence[0]))
            raise NotEnoughTrainingDataError(f'Input volume {folder_name} is too small for depth: {depth}')

        total_volumes = int(len(vol_sequence) / depth)

        for i in range(total_volumes):
            start = i * depth
            end = (i + 1) * depth
            sub_volume = vol_sequence[start:end]
            volumes.append(sub_volume)

    return volumes


def split_volume_sequences(volume_sequences, percentage):
    """
    Splits list of volumes into a two parts by a given percentage.

    Args:
        volume_sequences: list of sub list where each sub list represents a single volume of a fixed depth
        percentage: where to split

    Returns: first the x percent of volumes, second the remaining volumes
    """

    total = len(volume_sequences)
    sequence_size = math.ceil(percentage / 100 * total)
    extracted_sequences = []
    current_size = 0
    idx = 0
    # realign training data
    while current_size < sequence_size:
        current_size += 1
        extracted_sequences.append(volume_sequences[idx])
        idx += 1

    # remove already added sequences
    for i in range(idx):
        volume_sequences.pop(0)

    # extracted and remaining sequences
    return extracted_sequences, volume_sequences


def slice_sub_volumes_to_train_data(sequences, train_data_percentage=80):
    input_train_volumes, input_valid_volumes = split_volume_sequences(sequences, train_data_percentage)

    try:
        if not input_valid_volumes:
            input_valid_volumes = [input_train_volumes[-1]]
            input_train_volumes.pop(-1)
        if not input_train_volumes[0]:
            raise IndexError

    except IndexError:
        raise NotEnoughTrainingDataError(
            'Too less data! Could not split training data into train, valid and test sequences.'
        )
    return input_train_volumes, input_valid_volumes


def create_train_valid_sequences_or_volumes(inputs,
                                            targets,
                                            is_3d,
                                            depth_or_sequence_size=None,
                                            shuffle_data_split=False,
                                            train_data_percentage=80,
                                            ):
    """
    Args:
        inputs: list of sub lists where each sub list represents one volume/time sequence
        targets: list of sub lists where each sub list represents one volume/time sequence
        is_3d: if the sequences should contain 3 dimensional data or 2D --> example a 3d block or a sequence for rnn's
        depth_or_sequence_size: depth or rnn sequence size
        shuffle_data_split: if True, the initial split of the file paths into training and valid data is randomized
        train_data_percentage: defaults to 80% train data, 30% valid data if no validation data is passed
    """

    image_train_sequences = []
    image_valid_sequences = []

    mask_train_sequences = []
    mask_valid_sequences = []

    if shuffle_data_split:
        combined = list(zip(inputs, targets))
        random.shuffle(combined)
        inputs = [element[0] for element in combined]
        targets = [element[1] for element in combined]

    if is_3d:
        image_volumes = slice_data_to_sub_volumes_or_sequences(inputs, depth_or_sequence_size)
        mask_volumes = slice_data_to_sub_volumes_or_sequences(targets, depth_or_sequence_size)

        image_train_sequences, image_valid_sequences = slice_sub_volumes_to_train_data(
            image_volumes, train_data_percentage
        )
        mask_train_sequences, mask_valid_sequences = slice_sub_volumes_to_train_data(
            mask_volumes, train_data_percentage
        )
    else:
        # attention: sequence represents 3d data. Used by RNN's
        for image_sequence, mask_sequence in zip(inputs, targets):
            image_train, image_valid = slice_sequence_or_files(image_sequence, train_data_percentage)
            mask_train, mask_valid = slice_sequence_or_files(mask_sequence, train_data_percentage)

            image_train_sequences.append(image_train)
            image_valid_sequences.append(image_valid)

            mask_train_sequences.append(mask_train)
            mask_valid_sequences.append(mask_valid)

    return image_train_sequences, mask_train_sequences, image_valid_sequences, mask_valid_sequences


def seed_worker_reproducible(worker_id):
    """
    This ensures that each worker has a different "predictable" randomness so to say if you pass reproduce=True
    """
    worker_seed = constants.GLOBAL_SEED * (worker_id + 1)
    while worker_seed >= 2**32:
        worker_seed -= 2**32
    assert worker_seed >= 0
    assert worker_seed < 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


def set_up_loaders(image_paths,
                   mask_paths,
                   params,
                   test_case=False,
                   valid_images_paths=None,
                   valid_masks_paths=None,
                   dataset_mean=None,
                   dataset_std=None,
                   train_data_percentage=80,
                   ):
    """
    Creates the train, validation and test loader

    Args:
        image_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        mask_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        params:
        test_case: True at Unit testing
        valid_images_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        valid_masks_paths: list of file paths or if recurrent or 3d: list containing sub lists of file paths
        dataset_mean: mean of the actual dataset. If None, a default mean will be used
        dataset_std: standard deviation of the dataset. If None, a default std will be used
        train_data_percentage: if no validation set is given, this is the split ratio into train and valid

    Returns: train_loader, valid_loader

    """
    if not test_case and not isinstance(params, FixedParameters):
        raise RuntimeError('params must be a FixedParameters instance!')
    if valid_masks_paths is None:
        valid_masks_paths = []
    if valid_images_paths is None:
        valid_images_paths = []

    # spawning worker threads takes longer than loading a few images. Large batch sizes are different!
    # Therefore assumption is: bs >= 16 is faster using a worker which preloads data
    # This is system dependent!
    # furthermore I compare against input size of 256x256
    # each worker loads 1 batch! multiple workers might not speed up anything.
    num_workers = 0 if params.batch_size * params.input_size**2 < 16 * 256**2 else 1
    if valid_images_paths:
        train_images = image_paths
        train_masks = mask_paths
        valid_images = valid_images_paths
        valid_masks = valid_masks_paths
    else:
        if not test_case:
            # shuffle data split
            combined = list(zip(image_paths, mask_paths))
            random.shuffle(combined)
            image_paths = [element[0] for element in combined]
            mask_paths = [element[1] for element in combined]

        train_images, valid_images = slice_sequence_or_files(image_paths, training_percentage=train_data_percentage)
        train_masks, valid_masks = slice_sequence_or_files(mask_paths, training_percentage=train_data_percentage)

    train_dataset = Dataset2D(
        train_images,
        train_masks,
        params.channels,
        dataset_mean=dataset_mean,
        dataset_std=dataset_std,
    )
    valid_dataset = Dataset2D(
        valid_images,
        valid_masks,
        params.channels,
        dataset_mean=dataset_mean,
        dataset_std=dataset_std,
    )

    if params.online_aug:
        # when using augmentation, 2 or 4 workers are likely to be faster
        num_workers = 2

    if params.reproduce:
        generator = torch.Generator()
        generator.manual_seed(constants.GLOBAL_SEED)
        worker_init_fn = seed_worker_reproducible
    else:
        generator = None
        worker_init_fn = None

    print(f'Using {num_workers} parallel workers during training.\n')

    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=params.batch_size,
        shuffle=False if test_case else True,
        num_workers=num_workers,
        drop_last=params.drop_last,
        worker_init_fn=worker_init_fn,
        generator=generator,
        prefetch_factor=2,
    )
    valid_loader = DataLoader(
        dataset=valid_dataset,
        batch_size=params.batch_size,
        shuffle=False if test_case else True,
        num_workers=num_workers,
        drop_last=params.drop_last,
        worker_init_fn=worker_init_fn,
        generator=generator,
        prefetch_factor=2,
    )
    return train_loader, valid_loader
