import numpy as np
import os
import torch
import warnings


from matplotlib import pyplot as plt

from .. import constants
from ..file_handling.write import save_csv
from ..user_interaction.parameters import FixedParameters
from ..utils import ceil, floor, round_to_base_number


def get_model_folder_name(params):
    if constants.ARCHITECTURES_AND_BACKBONES[params.architecture]:
        sub_folder = f'{params.dataset_name}_{params.architecture}_{params.backbone}'
    else:
        sub_folder = f'{params.dataset_name}_{params.architecture}'
    return sub_folder


def get_trained_model_folder(params):
    sub_folder = get_model_folder_name(params)
    return os.path.join(constants.TRAINED_MODELS_FOLDER, sub_folder)


def get_backup_model_folder(params):
    sub_folder = get_model_folder_name(params)
    return os.path.join(constants.TRAIN_MODELS_BACKUP_FOLDER, sub_folder)


def save_plot(file_path,
              x_axis,
              y_data,
              colors,
              legend,
              legend_loc,
              x_label,
              y_label,
              x_lim,
              y_lim,
              major_ticks_y,
              minor_ticks_y,
              major_ticks_x=None,
              minor_ticks_x=None,
              ):

    for i, y_values in enumerate(y_data):
        plt.plot(x_axis, y_values, color=colors[i])

    plt.legend(legend, loc=legend_loc)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlim(x_lim[0], x_lim[1])

    ax = plt.gca()

    if major_ticks_x is None:
        major_ticks_x = np.arange(0, x_axis[-1], 5)
    if minor_ticks_x is None:
        minor_ticks_x = np.arange(0, x_axis[-1], 1)

    ax.set_xticks(major_ticks_x)
    ax.set_xticks(minor_ticks_x, minor=True)

    ax.set_yticks(major_ticks_y)
    ax.set_yticks(minor_ticks_y, minor=True)

    # set opacity of those ticks
    ax.grid(which='minor', alpha=0.5, linestyle='--')
    ax.grid(which='major', alpha=1, linestyle='-')

    # show grid
    plt.grid(True, which='both')

    plt.savefig(file_path)
    plt.close()


def _get_base_5_tick(tick_size_string):
    if not isinstance(tick_size_string, str):
        raise ValueError('_get_base_5_tick expects a string.')

    if float(tick_size_string).is_integer():
        tick_size_tmp = float(tick_size_string)
        tick_size_tmp = round_to_base_number(tick_size_tmp, base=5)
        if tick_size_tmp != 0:
            return tick_size_tmp
        else:
            return float(tick_size_string)

    number, decimals = tick_size_string.split('.')[:]
    number = int(number)

    leading_decimal_zeroes = len(decimals) - len(str(int(decimals)))

    tmp_decimals = int(decimals)
    tmp_decimals = round_to_base_number(tmp_decimals, base=5)

    if tmp_decimals == 0 and number == 0:
        return int(decimals) / 10

    if tmp_decimals == 10**len(decimals):
        number += 1
        tmp_decimals = 0

    return float(f'{number}.{leading_decimal_zeroes * "0"}{tmp_decimals}')


def get_tick_sizes(data_lim_min, data_lim_max, expected_major_ticks=4, expected_minor_ticks=None, round_to_next_5=True):
    """

    Args:
        data_lim_min:
        data_lim_max:
        expected_major_ticks:
        expected_minor_ticks:
        round_to_next_5: if True: example: tick size = 0.22 --> tick size = 0.2; tick size = 0.23 --> tick size = 0.25

    Returns:

    """
    difference = data_lim_max - data_lim_min
    major_tick_size = difference / expected_major_ticks
    position_to_round = 1 + _get_position_to_add(major_tick_size)
    major_tick_size = round(major_tick_size, position_to_round)

    if expected_minor_ticks is None:
        minor_tick_size = round(major_tick_size / 2, position_to_round + 1)
    else:
        minor_tick_size = difference / expected_minor_ticks
        position_to_round = 1 + _get_position_to_add(minor_tick_size)
        minor_tick_size = round(minor_tick_size, position_to_round)

    if round_to_next_5:  # either 0, 5 or 10 --> 18 -> 20; 0.018 -> 0.02; 0.1 -> 0.1; 0.11 -> 0.1
        major_tick_size_string = str(float(major_tick_size))
        major_tick_size = _get_base_5_tick(major_tick_size_string)
        minor_tick_size = major_tick_size / 2

    return major_tick_size, minor_tick_size


def _get_position_to_add(number):
    """Used for get_y_lim"""
    if number < 0:
        raise ValueError('Number must be larger than 0')
    if 0 < number < 1:
        factor = 1
        val = number
        while val < 1:
            factor *= 10
            val = val * factor

        position_to_add = len(str(factor)) - 1
    else:
        divisor = 1
        val = number
        while val > 1:
            divisor *= 10
            val = val / divisor
        # why -2? example: 123 --> 3 is pos 0, 2 is pos -1, 1 is pos -2 --> we want 130 not 200
        position_to_add = -1 * (len(str(divisor)) - 2)
    return position_to_add


def get_y_lim(y_data, fix_min_to_zero=True):
    """
    Gets y axis limitations.
    Example:
        1.:
            y_data = [-123, -10, 1, 2]
            limits will be:
            y_min = -130
            y_max = 10
        2.:
            y_data = [87, 91, 120]
            limits will be:
            y_min = 80
            y_max = 130

    Args:
        y_data:
        fix_min_to_zero: you can set y min to zero if wanted. Useful at loss

    Returns: min value, max value
    """
    if not isinstance(y_data, (list, np.ndarray)):
        raise ValueError('y_data must be a list or numpy nd array')
    if not isinstance(y_data, np.ndarray):
        y_data = np.array(y_data)

    y_data[y_data is None] = np.nan

    max_val = np.nanmax(y_data)
    min_val = np.nanmin(y_data)

    max_is_negative = False
    if max_val < 0:
        max_val *= -1
        max_is_negative = True

    min_is_negative = False
    if min_val < 0:
        min_val *= -1
        min_is_negative = True

    decimal_to_add = _get_position_to_add(max_val)
    decimal_to_add_min = _get_position_to_add(min_val) + 1

    decimal_to_add = min(decimal_to_add_min, decimal_to_add)
    max_val = -1 * max_val + 1 / 10 ** decimal_to_add if max_is_negative else max_val + 1 / 10 ** decimal_to_add
    max_val = floor(max_val, decimal_to_add)

    if fix_min_to_zero:
        return 0, max_val

    min_val = -1 * min_val - 1 / 10 ** decimal_to_add if min_is_negative else min_val - 1 / 10 ** decimal_to_add
    min_val = ceil(min_val, decimal_to_add)

    return min_val, max_val


def save_history(history,
                 model_folder,
                 header=None,
                 ema_range=None,
                 ma_range=None,
                 skip_first_x_values_for_graph_y_lim=None,
                 ):

    skip_keys = ['loss', 'accuracy', 'slope']
    if skip_first_x_values_for_graph_y_lim is None:
        skip_first_x_values_for_graph_y_lim = {
            'loss': 5,
            'accuracy': 0,
            'slope': 10,
        }
    elif not isinstance(skip_first_x_values_for_graph_y_lim, dict):
        raise ValueError('skip_first_x_values_for_graph_y_lim must be a dict')
    elif not all([key in skip_first_x_values_for_graph_y_lim.keys() for key in skip_keys]):
        raise KeyError(f'Each key: {", ".join(skip_keys)} must be in skip_first_x_values_for_graph_y_lim keys.\n'
                       f'Your keys: {", ".join(skip_first_x_values_for_graph_y_lim.keys())}')

    if header is None:
        header = 'epochs, train_losses, valid_losses, train_accuracies, valid_accuracies, ma_train_acc, ' \
                 'ma_train_avg_slopes, training_ema, validation_ema, train_ema_slopes, valid_ema_slopes, ' \
                 'augmentation_strengths'

    if len(header.split(',')) != len(history[0]):
        warnings.warn('Header of history is not of same size then elements in history!')

    save_csv(
        os.path.join(model_folder, 'history.csv'),
        history,
        header,
        number_format='.6f',
    )

    history = np.array(history)
    history[history == None] = np.nan
    history = history.astype(np.float64)

    if skip_first_x_values_for_graph_y_lim['loss'] > 0:
        start = skip_first_x_values_for_graph_y_lim['loss']
    else:
        start = 0

    y_loss_range = get_y_lim(np.hstack((history[start:, 1], history[start:, 2])), fix_min_to_zero=True)
    major_tick_size, minor_tick_size = get_tick_sizes(y_loss_range[0], y_loss_range[1])

    major_tick_size_x, minor_tick_size_x = get_tick_sizes(history[0, 0], history[-1, 0])

    save_plot(
        file_path=os.path.join(model_folder, 'loss_curve.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 1], history[:, 2]],
        colors=['tab:olive', 'tab:blue'],
        legend=['Tr Loss', 'Val Loss'],
        legend_loc='upper right',
        x_label='Epoch Number',
        y_label='Loss',
        x_lim=[0, history[-1, 0]],
        y_lim=y_loss_range,
        major_ticks_y=np.arange(y_loss_range[0], y_loss_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_loss_range[0], y_loss_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )

    if skip_first_x_values_for_graph_y_lim['accuracy'] > 0:
        start = skip_first_x_values_for_graph_y_lim['accuracy']
    else:
        start = 0

    if np.isnan(history[-1, 7]):
        y_accuracy_range = get_y_lim(
            np.hstack((history[start:, 3], history[start:, 4], history[start:, 5])),
            fix_min_to_zero=False
        )
    else:
        y_accuracy_range = get_y_lim(
            np.hstack((
                history[start:, 3],
                history[start:, 4],
                history[start:, 5],
                history[start:, 7],
                history[start:, 8]),
            ),
            fix_min_to_zero=False
        )

    major_tick_size, minor_tick_size = get_tick_sizes(y_accuracy_range[0], y_accuracy_range[1])
    save_plot(
        file_path=os.path.join(model_folder, 'accuracy_curve.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 3], history[:, 4]],
        colors=['tab:olive', 'tab:blue'],
        legend=['Training Accuracy', 'Validation Accuracy'],
        legend_loc='lower right',
        x_label='Epoch Number',
        y_label='Accuracy [%]',
        x_lim=[0, history[-1, 0]],
        y_lim=y_accuracy_range,
        major_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )

    save_plot(
        file_path=os.path.join(model_folder, 'training_moving_average_accuracy.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 5]],
        colors=['tab:olive'],
        legend=['Training Accuracy MA'],
        legend_loc='lower right',
        x_label='Epoch Number',
        y_label=f'Moving Average (last {ma_range} epochs) [%]',
        x_lim=[0, history[-1, 0]],
        y_lim=y_accuracy_range,
        major_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )

    if skip_first_x_values_for_graph_y_lim['slope'] > 0:
        start = skip_first_x_values_for_graph_y_lim['slope']
    else:
        start = 0

    if np.isnan(history[-1, 7]):
        y_ma_slope_range = get_y_lim(history[start:, 6], fix_min_to_zero=False)
    else:
        y_ma_slope_range = get_y_lim(
            np.hstack((history[start:, 6], history[start:, 9], history[start:, 10])),
            fix_min_to_zero=False
        )
    major_tick_size, minor_tick_size = get_tick_sizes(y_ma_slope_range[0], y_ma_slope_range[1])
    save_plot(
        file_path=os.path.join(model_folder, 'training_moving_average_slope.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 6]],
        colors=['tab:olive'],
        legend=['Training accuracy (slope of moving average)'],
        legend_loc='upper right',
        x_label='Epoch Number',
        y_label=f'MA slope (y = mx + n) over last {ma_range} accuracies',
        x_lim=[0, history[-1, 0]],
        y_lim=y_ma_slope_range,
        major_ticks_y=np.arange(y_ma_slope_range[0], y_ma_slope_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_ma_slope_range[0], y_ma_slope_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )

    if np.isnan(history[-1, 7]):
        return

    major_tick_size, minor_tick_size = get_tick_sizes(y_accuracy_range[0], y_accuracy_range[1])
    save_plot(
        file_path=os.path.join(model_folder, 'exponential_moving_average_accuracies.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 7], history[:, 8]],
        colors=['tab:olive', 'tab:blue'],
        legend=['Training Accuracy EMA', 'Validation Accuracy EMA'],
        legend_loc='lower right',
        x_label='Epoch Number',
        y_label=f'EMA (last {ema_range} epochs) [%]',
        x_lim=[0, history[-1, 0]],
        y_lim=y_accuracy_range,
        major_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_accuracy_range[0], y_accuracy_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )

    # y_ema_slope_range = get_y_lim(np.hstack((history[start:, 9], history[start:, 10])), fix_min_to_zero=False)
    # using same range as moving average for equal scaling
    major_tick_size, minor_tick_size = get_tick_sizes(y_ma_slope_range[0], y_ma_slope_range[1])
    save_plot(
        file_path=os.path.join(model_folder, 'exponential_moving_average_slopes.png'),
        x_axis=history[:, 0],
        y_data=[history[:, 9], history[:, 10]],
        colors=['tab:olive', 'tab:blue'],
        legend=['Training Accuracy (slope of EMA)', 'Validation accuracy (slope of EMA)'],
        legend_loc='upper right',
        x_label='Epoch Number',
        y_label=f'EMA slope (y = mx + n) over last {ema_range} EMA accuracies)',
        x_lim=[0, history[-1, 0]],
        y_lim=y_ma_slope_range,
        major_ticks_y=np.arange(y_ma_slope_range[0], y_ma_slope_range[1], major_tick_size),
        minor_ticks_y=np.arange(y_ma_slope_range[0], y_ma_slope_range[1], minor_tick_size),
        major_ticks_x=np.arange(0, history[-1, 0], major_tick_size_x),
        minor_ticks_x=np.arange(0, history[-1, 0], minor_tick_size_x),
    )


def save_model(params, history, model, optimizer, directory, dataset_mean, dataset_std):
    if not isinstance(history, list):
        raise ValueError('history must be a list!')

    if not isinstance(params, FixedParameters):
        raise ValueError('params must be a FixedParameters instance')

    saving_dict = {
        'architecture': params.architecture,
        'image_channels': params.channels,
        'classes': params.classes,
        'image_input_size': params.input_size,  # actually not needed since no fully connected layer is included
        'history': history,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'normalization': params.norm,
        'dataset_mean': dataset_mean,
        'dataset_std': dataset_std,
        'drop_last': params.drop_last,
        'aug_strength': params.aug_strength,
        'aug_start_epoch': params.aug_start_epoch,
        'aug_ema_range': params.aug_ema_range,
    }

    if constants.ARCHITECTURES_AND_BACKBONES[params.architecture]:
        saving_dict['backbone'] = params.backbone

    if params.online_aug:
        saving_dict['online_aug'] = True
        saving_dict['aug_start_epoch'] = params.aug_start_epoch
        saving_dict['aug_ema_range'] = params.aug_ema_range

    file_number = str(history[-1][0]).zfill(len(str(params.epochs)))
    torch.save(saving_dict, os.path.join(directory, 'checkpoint_epoch_' + str(file_number) + '.pt'))
