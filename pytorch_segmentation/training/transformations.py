from torchvision import transforms

from ..constants import DEFAULT_RGB_MEAN
from ..constants import DEFAULT_RGB_STD
from ..constants import DEFAULT_GRAY_MEAN
from ..constants import DEFAULT_GRAY_STD


def get_rgb_image_transformation(mean=None, std=None):
    if mean is None:
        mean = DEFAULT_RGB_MEAN
    elif not isinstance(mean, list):
        raise ValueError('mean must be a list')

    if std is None:
        std = DEFAULT_RGB_STD
    elif not isinstance(std, list):
        raise ValueError('std must be a list')

    transformation = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean, std),
    ])
    return transformation


def get_gray_image_transformation(mean=None, std=None):
    if mean is None:
        mean = DEFAULT_GRAY_MEAN
    elif not isinstance(mean, list):
        raise ValueError('mean must be a list')

    if std is None:
        std = DEFAULT_GRAY_STD
    elif not isinstance(std, list):
        raise ValueError('std must be a list')

    transformation = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean, std),
    ])
    return transformation
