import cv2
import os

import torch

from torch.utils.data.dataset import Dataset

from .transformations import get_gray_image_transformation
from .transformations import get_rgb_image_transformation


class Dataset2D(Dataset):
    def __init__(self, img_paths, mask_paths, channels, dataset_mean=None, dataset_std=None):
        """

        Args:
            img_paths:
            mask_paths:
            channels: 1 for gray, 3 rgb, ..
        """
        if channels not in [1, 3]:
            raise ValueError('Channels must be 1 or 3')
        self.channels = channels

        self.img_paths = img_paths
        self.mask_paths = mask_paths

        self.aug_pipeline = None

        if len(self.img_paths) != len(self.mask_paths):
            raise RuntimeError(f'Length of images ({len(img_paths)}) and masks ({len(mask_paths)}) is not equal!')

        for img, mask in zip(img_paths, mask_paths):
            try:
                assert os.path.isfile(img)
                assert os.path.isfile(mask)
            except AssertionError:
                print(f'Failed at img: \n{img}\nmask:\n{mask}.\nAt least one file is missing.')
                raise AssertionError('Could not find some files')

        if channels == 1:
            self.img_transform = get_gray_image_transformation(dataset_mean, dataset_std)
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation(dataset_mean, dataset_std)
        else:
            raise NotImplementedError(f'{channels} channels are not supported.')

    def __getitem__(self, i):
        img_path = self.img_paths[i]
        mask_path = self.mask_paths[i]
        if self.channels == 1:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        elif self.channels == 3:
            img = cv2.cvtColor(cv2.imread(img_path, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

        if self.aug_pipeline is not None:
            img, mask = self.aug_pipeline(img, mask)

        img = self.img_transform(img)
        mask = torch.tensor(mask)

        return img, mask

    def __len__(self):
        return len(self.img_paths)
