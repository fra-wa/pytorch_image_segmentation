import torch

from ..utils import show_gpu_memory, show_cpu_memory


def get_targets_and_outputs(inputs, targets, params, model):
    """
    Used by test loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    inputs = inputs.to(params.device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if params.classes == 1 else torch.long
    targets = targets.to(params.device, dtype=masks_type)
    outputs = model(inputs)
    return targets, outputs


def get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func):
    """
    Used by train and validation loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    input_batch = input_batch.to(params.device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if params.classes == 2 else torch.long
    target_batch = target_batch.to(params.device, dtype=masks_type)

    output_batch = model(input_batch)

    if params.classes == 2:
        loss = loss_func(output_batch.squeeze(dim=1), target_batch)
    else:
        loss = loss_func(output_batch, target_batch)

    return target_batch, output_batch, loss


def compute_accuracy(output_logits, targets):
    # output_logits of shape: B, C, ....
    if output_logits.size(1) == 1:
        output_probabilities = torch.sigmoid(output_logits)
        outs = (output_probabilities.squeeze(1) > 0.5).float()
        correct = outs.eq(targets.squeeze()).sum().item()
    else:
        values, predictions = torch.max(output_logits, dim=1)
        correct = predictions.eq(targets).sum().item()

    total = targets.nelement()  # sums up all pixels (batch_size x width x height)
    accuracy = (correct / total) * targets.size(0)

    return accuracy


def train_loop(params, model, training_loader, loss_func, optimizer):
    model.train()

    train_loss = 0.0
    train_acc = 0.0

    print('Training loop:    ')

    for i, (input_batch, target_batch) in enumerate(training_loader):
        if params.show_ram and i <= 2:
            print(f'Batch {i + 1} / {len(training_loader)}')
            show_cpu_memory() if params.device == 'cpu' else show_gpu_memory()
        else:
            print(f'Batch {i + 1} / {len(training_loader)}' + 4 * ' ', end='\r')

        targets, outputs, loss = get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func)

        # if last batch has less elements, last batch must be weighted different. Therefore: loss x batch_size
        train_loss += loss.item() * targets.size(0)
        train_acc += compute_accuracy(outputs, targets)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    # loss was multiplied by each batch size --> divide by all training samples
    train_acc /= len(training_loader.dataset)
    train_loss /= len(training_loader.dataset)
    print('')
    return train_loss, train_acc, model


def valid_loop(params, model, validation_loader, loss_func):
    valid_loss = 0.0
    valid_acc = 0.0

    with torch.no_grad():
        model.eval()

        print('Validation loop:     ')
        for i, (input_batch, target_batch) in enumerate(validation_loader):
            print(f'Batch {i + 1} / {len(validation_loader)}' + 4 * ' ', end='\r')
            targets, outputs, loss = get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func)

            valid_loss += loss.item() * targets.size(0)
            valid_acc += compute_accuracy(outputs, targets)

    valid_acc /= len(validation_loader.dataset)
    valid_loss /= len(validation_loader.dataset)

    print('')

    return valid_loss, valid_acc, model
