import numpy as np


class TrainingProfiler:
    """
    What and usage:

    Tracks training stats

    1. I want to know whether or not the training converges or not (if i should stop the training):
        profiler = TrainingProfiler()

        # after each epoch call update and pass the history:
        profiler.update(current_history)
        i_should_stop = profiler.training_should_be_stopped()

        if i_should_stop:
            # save last stuff, break the training loop

    2. I need adaptive augmentation strength for the augmentation pipeline
        profiler = TrainingProfiler(calculate_aug_strength=True)

        # after each epoch call update and pass the history:
        profiler.update(current_history)
        augment_strength = profiler.augmentation_strengths[-1]

        i_should_stop = profiler.training_should_be_stopped()

        if i_should_stop:
            # save last stuff, break the training loop, return

    """
    def __init__(self,
                 history_dict=None,
                 ema_range=10,
                 calculate_all_stats=True,
                 initial_aug_strength=0.0,
                 aug_start_epoch=2,
                 stop_after_x_epochs_no_ma_increment=10,
                 training_moving_avg_range=10,
                 stop_slope_threshold=0.01,  # relates to a change of 0.01 % from one epoch to another
                 max_increment=0.1,
                 ):
        """
        This profiler logs the moving average of the last stop_indicator_epochs values to decide if the
        training should be stopped.
        Furthermore, it calculates the augmentation strength for the AdaptiveAugmentationPipeline based on an analysis
        of the exponential moving averages of training and validation accuracies (ema) if wanted.


        ADAPTIVE AUG IS NO MORE SUPPORTED BUT CALCULATION IS KEPT FOR EVENTUAL FUTURE USE


        Adaptive Augmentation strength calculation:

        There are three indicators to adapt the augmentation strength during training.
        Initially, the augmentation strength starts at 0 or in case of training continuation, it
        uses the history to determine the initial augmentation strength.

        The augmentation strength will be calculated by:

            aug_strength = max(current_aug_strength, strength_accuracy, strength_delta_slope, strength_slope)

        where:

            current_aug_strength ... current augmentation strength
            strength_accuracy ... strength calculated by the difference of training and validation accuracy
            strength_delta_slope ... strength calculated by the difference of the slope of the training accuracy vs
                                     slope of the validation accuracy. If the validation accuracy drops, the
                                     augmentation strength will be increased.
            strength_slope ... if the augmentation strength is 1 and the training accuracy does no more rise over a few
                               epochs

        The slopes are calculated using the exponential moving average (EMA) of a given range to give more weight
        to the more recent accuracy values but also consider the previous accuracy values and smooth the graphs to
        reduce the effect of peaks during training.

        Args:
            history_dict: Recent training history.
                Shape: [[epoch + 1, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc], ...]
            ema_range: exponential moving average (ema) range used to calculate a smooth current representation of the
                training and validation accuracy with more weight on the more recent values.
            calculate_all_stats: Whether to store ema as well or not
            aug_start_epoch: Epoch, from which the online augmentation is activated.
            stop_after_x_epochs_no_ma_increment: If the average of the training accuracy slope is
                below the stop_slope_threshold x times in a row (x = stop_after_x_epochs_no_ma_increment),
                the training can be stopped. This is only an indicator! You can decide to use it.
            training_moving_avg_range: Last x epochs used to calculate the training slope average
            stop_slope_threshold: Threshold under which the training might be stopped.
                See stop_indicator_epochs.
            max_increment: maximum increment of the adaptive augmentation strength (currently deactivated)
        """
        if ema_range < 1:
            raise ValueError('ema range must be >= 1')
        if aug_start_epoch < 0:
            raise ValueError('augmentation start epoch must be 0 or greater')

        self.epochs = []

        self.train_losses = []
        self.valid_losses = []
        self.train_accuracies = []
        self.valid_accuracies = []

        # ema: exponential moving average
        self.training_ema = []
        self.validation_ema = []
        self.train_ema_slopes = []
        self.valid_ema_slopes = []
        self.augmentation_strengths = []

        # ma: moving average
        self.ma_train_acc = []
        self.ma_train_avg_slopes = []

        self.stop_indicator_epochs = stop_after_x_epochs_no_ma_increment
        self.training_moving_avg_range = training_moving_avg_range
        self.stop_slope_threshold = stop_slope_threshold

        self.ema_range = ema_range
        self.aug_start_epoch = aug_start_epoch
        self.calculate_all_stats = calculate_all_stats
        self.calculate_aug_strength = False  # currently deactivated
        self.initial_aug_strength = initial_aug_strength
        self.max_increment = max_increment

        self.alpha = 2 / (self.ema_range + 1)
        self.history = history_dict

        # multipliers are slopes (y = slope * x + n) of linear functions which are representing the range in which the
        # aug_strength for each indicator will be increased
        # you might exchange them to more complex functions. Keep Calculation simple!
        delta_accuracy_increment_range = [2, 5]
        delta_slope_increment_range = [0.5, 3]
        slope_increment_range = [0, 1]  # training ema slope from one to another between 0 and -1 increases intensity

        self.linear_delta_acc_func = np.polyfit(delta_accuracy_increment_range, [0, 1], 1)
        self.linear_delta_slope_multiplier_func = np.polyfit(delta_slope_increment_range, [0, 1], 1)
        self.linear_train_slope_multiplier_func = np.polyfit(slope_increment_range, [1, 0], 1)

        if self.history is not None and self.history['epochs']:
            self.epochs = history_dict['epochs']
            self.train_losses = history_dict['train_loss']
            self.valid_losses = history_dict['valid_loss']
            self.train_accuracies = history_dict['train_acc']
            self.valid_accuracies = history_dict['valid_acc']
            self.training_ema = history_dict['ma_train_acc']
            self.validation_ema = history_dict['ma_train_acc_slope']
            self.train_ema_slopes = history_dict['aug_strength']
            self.valid_ema_slopes = history_dict['train_acc_ema']
            self.augmentation_strengths = history_dict['valid_acc_ema']
            self.ma_train_acc = history_dict['train_ema_slopes']
            self.ma_train_avg_slopes = history_dict['valid_ema_slopes']

            if self.calculate_aug_strength or self.calculate_all_stats:
                self._calculate_initial_values(update_moving_avg=self.calculate_all_stats)

    def _calculate_ema(self, data):
        """Calculates the ema of the latest datapoint in data."""
        ema_value = data[-1]
        if ema_value is None:
            return None

        for i, data_pt in enumerate(data[1:]):
            ema_value = self.alpha * data_pt + (1 - self.alpha) * ema_value
        return ema_value

    def _calculate_initial_values(self, update_moving_avg=False):
        """Todo: simplify. too dirty"""
        # backup function for older models. Old history had no moving or exponential moving average
        if self.training_ema[-1] is not None and len(self.training_ema) > 1:
            # already calculated data
            return
        # clean just to ensure nobody made unexpected changes at init
        self.training_ema = []
        self.validation_ema = []
        self.train_ema_slopes = []
        self.valid_ema_slopes = []
        self.augmentation_strengths = []

        self.ma_train_acc = []
        self.ma_train_avg_slopes = []

        if len(self.epochs) == 1:
            self.training_ema.append(self.history['train_acc'][0])
            if self.calculate_aug_strength or self.calculate_all_stats:
                self.validation_ema.append(self.history['valid_acc'][0])
            else:
                self.validation_ema.append(None)
            self.train_ema_slopes.append(None)
            self.valid_ema_slopes.append(None)
            self.ma_train_acc.append(self.history['train_acc'][0])
            self.ma_train_avg_slopes.append(None)
            self.augmentation_strengths.append(self.initial_aug_strength)
            return

        train_acc = self.history['train_acc']
        if self.calculate_aug_strength or self.calculate_all_stats:
            valid_acc = self.history['valid_acc']
        else:
            valid_acc = [None] * len(train_acc)

        if self.ema_range == 1:
            self.training_ema = train_acc
            self.validation_ema = valid_acc
            self.train_ema_slopes = len(train_acc) * [None]
            self.valid_ema_slopes = len(train_acc) * [None]
            self.augmentation_strengths = len(train_acc) * [self.initial_aug_strength]

        self.training_ema = [train_acc[0]]
        self.validation_ema = [valid_acc[0]]
        self.train_ema_slopes = [None]
        self.valid_ema_slopes = [None]
        self.ma_train_acc = [train_acc[0]]
        self.ma_train_avg_slopes = [None]
        self.augmentation_strengths = [self.initial_aug_strength]

        for i in range(1, len(train_acc)):
            current_range = i + 1 if i + 1 < self.training_moving_avg_range else self.training_moving_avg_range
            last_train_accuracies = train_acc[:i + 1][-current_range:]
            self.ma_train_acc.append(np.mean(last_train_accuracies))
            averages = self.ma_train_acc[-self.stop_indicator_epochs:]
            x_values = list(range(len(averages)))
            self.ma_train_avg_slopes.append(np.polyfit(x_values, averages, 1)[0])

            if self.ema_range > 1:
                current_range = i + 1 if i + 1 < self.ema_range else self.ema_range
                train_data = train_acc[:i + 1][-current_range:]
                valid_data = valid_acc[:i + 1][-current_range:]
                self.training_ema.append(self._calculate_ema(train_data))
                self.validation_ema.append(self._calculate_ema(valid_data))

                self._calculate_slopes()
                if self.calculate_aug_strength:
                    self._set_strength()
                else:
                    self.augmentation_strengths.append(self.initial_aug_strength)

    def update(self, epoch, train_loss, val_loss, train_acc, val_acc):
        self.epochs.append(epoch)
        self.train_losses.append(train_loss)
        self.valid_losses.append(val_loss)
        self.train_accuracies.append(train_acc)
        self.valid_accuracies.append(val_acc)

        if self.calculate_aug_strength:
            self._update_augmentation_strength()
        elif self.calculate_all_stats:
            self._update_ema_statistics()
            self.augmentation_strengths.append(self.initial_aug_strength)
        else:
            self.training_ema.append(None)
            self.validation_ema.append(None)
            self.train_ema_slopes.append(None)
            self.valid_ema_slopes.append(None)
            # if augmentation is forced
            self.augmentation_strengths.append(self.initial_aug_strength)
        self._update_train_moving_avg()

    def training_should_be_stopped(self):
        last_ma_train_acc_slopes = self.ma_train_avg_slopes[-self.stop_indicator_epochs:]
        if None in last_ma_train_acc_slopes:
            return False

        last_results = [slope < self.stop_slope_threshold for slope in last_ma_train_acc_slopes]
        if all(last_results):
            if self.calculate_aug_strength:
                # ensure, that augmentation was used all the time
                last_aug_strengths = self.augmentation_strengths[-self.stop_indicator_epochs:]
                if all([aug_strength == 1 for aug_strength in last_aug_strengths]):
                    return True
            else:
                return True
        return False

    def _update_train_moving_avg(self):
        if not self.epochs:
            return

        last_train_accuracies = self.train_accuracies[-self.training_moving_avg_range:]

        if len(last_train_accuracies) == 1:
            self.ma_train_acc.append(last_train_accuracies[0])
            self.ma_train_avg_slopes.append(None)
            return

        self.ma_train_acc.append(np.mean(last_train_accuracies))
        averages = self.ma_train_acc[-self.stop_indicator_epochs:]
        x_values = list(range(len(averages)))
        self.ma_train_avg_slopes.append(np.polyfit(x_values, averages, 1)[0])

    def _update_ema_statistics(self):
        self._add_current_ema()
        self._calculate_slopes()

    def _update_augmentation_strength(self):
        self._update_ema_statistics()
        self._set_strength()

    def _add_current_ema(self):
        """
        Calculates the current exponential moving average (ema) for the updated history.

        The self.ema_period has the following impacts:
        1.: as the search window (latest n (=range) elements are used to determine current ema)
        2.: to calculate the moving average alpha.
        A higher alpha discounts older observations faster if you would use the whole dataset to calculate the current
        ema. But here we have a fixed search window and an alpha calculated as: 2 / (ema_period + 1) = 2 / 11 = ~0.1818
        """
        if self.ema_range == 1:
            current_train_ema = self.train_accuracies[-1]
            current_valid_ema = self.valid_accuracies[-1]
        else:
            # search window is 10 (ema_period) --> calculate ema with last 10 data points
            current_range = len(self.epochs) if len(self.epochs) < self.ema_range else self.ema_range

            train_data = self.train_accuracies[-current_range:]
            valid_data = self.valid_accuracies[-current_range:]

            current_train_ema = self._calculate_ema(train_data)
            current_valid_ema = self._calculate_ema(valid_data)

        self.training_ema.append(current_train_ema)
        self.validation_ema.append(current_valid_ema)

    def _calculate_slopes(self):
        if len(self.training_ema) == 1:
            self.train_ema_slopes.append(None)
            self.valid_ema_slopes.append(None)
            return
        current_range = len(self.training_ema) if len(self.training_ema) < self.ema_range else self.ema_range
        x_values = list(range(current_range))

        train_slope_data = self.training_ema[-current_range:]
        train_slope = np.polyfit(x_values, train_slope_data, 1)[0]
        self.train_ema_slopes.append(train_slope)

        valid_slope_data = self.validation_ema[-current_range:]
        valid_slope = np.polyfit(x_values, valid_slope_data, 1)[0]
        self.valid_ema_slopes.append(valid_slope)

    def _set_strength(self):
        if len(self.train_ema_slopes) < self.aug_start_epoch or len(self.epochs) < 2:
            self.augmentation_strengths.append(max(0.0, self.initial_aug_strength))
            return
        if self.augmentation_strengths[-1] == 1:
            self.augmentation_strengths.append(1)
            return

        # impact of accuracy delta
        delta_acc = self.training_ema[-1] - self.validation_ema[-1]  # train minus validation accuracy
        strength_acc = delta_acc * self.linear_delta_acc_func[0] + self.linear_delta_acc_func[1]
        if strength_acc < 0:
            strength_acc = 0.0
        elif strength_acc > 1:
            strength_acc = 1.0

        # impact of slope delta
        delta_slope = self.train_ema_slopes[-1] - self.valid_ema_slopes[-1]  # train minus validation slope
        strength_slope = delta_slope * self.linear_delta_slope_multiplier_func[0] + \
            self.linear_delta_slope_multiplier_func[1]

        if strength_slope < 0:
            strength_slope = 0.0
        elif strength_slope > 1:
            strength_slope = 1.0

        # impact of training slope
        strength_slope_train = self.train_ema_slopes[-1] * self.linear_train_slope_multiplier_func[0] + \
            self.linear_train_slope_multiplier_func[1]

        if strength_slope_train < 0:
            strength_slope_train = 0.0
        elif strength_slope_train > 1:
            strength_slope_train = 1.0

        max_calculated_strength = round(max(
            self.augmentation_strengths[-1], strength_acc, strength_slope, strength_slope_train
        ), 3)
        max_strength = self.augmentation_strengths[-1] + self.max_increment
        strength = max_strength if max_calculated_strength > max_strength else max_calculated_strength
        self.augmentation_strengths.append(strength)

    def get_history(self):
        history = list(zip(
            self.epochs,
            self.train_losses,
            self.valid_losses,
            self.train_accuracies,
            self.valid_accuracies,
            self.ma_train_acc,
            self.ma_train_avg_slopes,
            self.training_ema,
            self.validation_ema,
            self.train_ema_slopes,
            self.valid_ema_slopes,
            self.augmentation_strengths,
        ))

        header = f'epochs, train_loss, val_loss, train_acc, val_acc, ma_train_acc, ma_train_avg_slopes, training_ema,'\
                 f' validation_ema, train_ema_slopes, valid_ema_slopes, augmentation_strengths # moving avg (ma) ' \
                 f'range: {self.training_moving_avg_range}; exp moving avg (ema) range: {self.ema_range}'

        return history, header
