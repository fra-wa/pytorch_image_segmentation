import os
import sys
import torch

from collections import OrderedDict

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(current_dir)
sys.path.extend([module_dir])

from pytorch_segmentation.training.training_init import start_training
from pytorch_segmentation.user_interaction.parameters import Parameters


if __name__ == '__main__':
    device_count = torch.cuda.device_count()

    parameters = OrderedDict({
        # Name: [Value, Help]
        'model_path': [
            r'', 'Path to the checkpoint or, if empty, the last checkpoint saved trained on this dataset.'
        ],
        'dataset_name': [
            '', 'Read Readme for more help! Pass the folder name of the training dataset.\n'
                'The folder must contain two sub folders named: "masks" and "images"\n'
                'VALIDATION: you can also put a folder called "validation" at the same\n'
                'level of masks and images. Inside, structure your data as before. Then this\n'
                'folder will be used for validation.'
        ],
        'batch_size': [
            6, 'Images/Volumes/Frame sequences per forward pass while training.'
        ],
        'device': [
            'cuda', 'Device to process on (cpu or cuda).'
        ],
        'epochs': [
            50, 'Number of epochs to train further.'
        ],
        'save_every': [
            1, "Save the model checkpoint every x epochs."
        ],
        'show_ram': [
            False, 'Whether to show the used ram (device based) or not'
        ],
        'reproduce': [
            False, 'If True, the random seed will be set to 0. ATTENTION: using deterministic cuDNN backend\n'
                   'is significantly SLOWER!'
        ],
        'gpu': [
            0, 'Which GPU should be used'
        ],
        'auto_train_stop': [
            False, 'If True, the TrainingProfiler will be asked to calculate whether or not to stop the training.\n'
                   'If so, either the epochs or the TrainingProfiler result will stop the training.\n'
                   'It calculates the exponential moving average of the last few epochs to determine if the training \n'
                   'accuracy rises or not. If not: the training will be stopped.'
        ],
    })

    if not torch.cuda.is_available():
        parameters['device'][1] = 'Not available! CUDA not supported!'
        parameters['gpu'][1] = 'Not available! CUDA not supported!'
    elif device_count == 1:
        parameters['gpu'][1] = 'Not available! Only one graphics card was detected!'
    elif device_count > 1:
        parameters['gpu'][1] = parameters['gpu'][1] + f' ({", ".join([str(i) for i in range(device_count)])}'

    parameter_names = [para for para in parameters.keys()]
    parameter_values = [val[0] for k, val in parameters.items()]
    parameter_help = [val[1] for k, val in parameters.items()]

    parameters = Parameters(parameter_names, parameter_values, parameter_help)

    start_training(parameters, continue_training=True)

    print('Finished Training.')
