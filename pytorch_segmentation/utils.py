import cv2
import math
import psutil
import subprocess as sp
import sys
import time
import torch

from datetime import date
from datetime import datetime

from PIL import Image

from pytorch_segmentation import constants


def get_human_time_values_from_seconds(seconds):
    hours = int(seconds / 3600)
    minutes = int(seconds / 60 - 60 * hours)
    seconds = int(seconds - 3600 * hours - 60 * minutes)
    return hours, minutes, seconds


def get_human_time_string_from_h_m_s(hours, minutes, seconds):
    if hours == 0:
        hours = f'00'
    elif hours < 10:
        hours = f'0{hours}'
    else:
        hours = f'{hours}'
    if minutes == 0:
        minutes = f'00'
    elif minutes < 10:
        minutes = f'0{minutes}'
    else:
        minutes = f'{minutes}'
    if seconds == 0:
        seconds = f'00'
    elif seconds < 10:
        seconds = f'0{seconds}'
    else:
        seconds = f'{seconds}'

    return f'{hours}h:{minutes}m:{seconds}s'


def calculate_human_spent_time_values(start_time):
    current_time = time.time()
    total_time = current_time - start_time
    return get_human_time_values_from_seconds(total_time)


def get_time_spent_string(start_time):
    hours, minutes, seconds = calculate_human_spent_time_values(start_time)
    return get_human_time_string_from_h_m_s(hours, minutes, seconds)


def get_time_remaining_string(start_time, total_count, current_idx):
    current_time = time.time()
    total_seconds_spent = current_time - start_time
    remaining_seconds = (total_seconds_spent * total_count) / (current_idx + 1) - total_seconds_spent
    hours, minutes, seconds = get_human_time_values_from_seconds(remaining_seconds)
    return get_human_time_string_from_h_m_s(hours, minutes, seconds)


def get_human_date(date_format='%d.%m.%Y'):
    """
    Args:
        date_format: build up from:
            %a	Abbreviated weekday name.	Sun, Mon, ...
            %A	Full weekday name.	Sunday, Monday, ...
            %w	Weekday as a decimal number.	0, 1, ..., 6
            %d	Day of the month as a zero-padded decimal.	01, 02, ..., 31
            %-d	Day of the month as a decimal number.	1, 2, ..., 30
            %b	Abbreviated month name.	Jan, Feb, ..., Dec
            %B	Full month name.	January, February, ...
            %m	Month as a zero-padded decimal number.	01, 02, ..., 12
            %-m	Month as a decimal number.	1, 2, ..., 12
            %y	Year without century as a zero-padded decimal number.	00, 01, ..., 99
            %-y	Year without century as a decimal number.	0, 1, ..., 99
            %Y	Year with century as a decimal number.	2013, 2019 etc.
            %Z	Time zone name.
            %j	Day of the year as a zero-padded decimal number.	001, 002, ..., 366
            %-j	Day of the year as a decimal number.	1, 2, ..., 366
            %U	Week number of the year (Sunday as the first day of the week).
                All days in a new year preceding the first Sunday are considered to be in week 0.	00, 01, ..., 53
            %W	Week number of the year (Monday as the first day of the week).
                All days in a new year preceding the first Monday are considered to be in week 0.	00, 01, ..., 53
            %x	Locale’s appropriate date representation.	09/30/13
            %X	Locale’s appropriate time representation.	07:06:05
            %%	A literal '%' character.	%

    Returns: string of current date

    """
    return date.today().strftime(date_format)


def get_human_datetime(datetime_format='%d.%m.%Y, %H:%M:%S', time_ending=' Uhr'):
    """

    Args:
        datetime_format: build up from:
            %a	Abbreviated weekday name.	Sun, Mon, ...
            %A	Full weekday name.	Sunday, Monday, ...
            %w	Weekday as a decimal number.	0, 1, ..., 6
            %d	Day of the month as a zero-padded decimal.	01, 02, ..., 31
            %-d	Day of the month as a decimal number.	1, 2, ..., 30
            %b	Abbreviated month name.	Jan, Feb, ..., Dec
            %B	Full month name.	January, February, ...
            %m	Month as a zero-padded decimal number.	01, 02, ..., 12
            %-m	Month as a decimal number.	1, 2, ..., 12
            %y	Year without century as a zero-padded decimal number.	00, 01, ..., 99
            %-y	Year without century as a decimal number.	0, 1, ..., 99
            %Y	Year with century as a decimal number.	2013, 2019 etc.
            %H	Hour (24-hour clock) as a zero-padded decimal number.	00, 01, ..., 23
            %-H	Hour (24-hour clock) as a decimal number.	0, 1, ..., 23
            %I	Hour (12-hour clock) as a zero-padded decimal number.	01, 02, ..., 12
            %-I	Hour (12-hour clock) as a decimal number.	1, 2, ... 12
            %p	Locale’s AM or PM.	AM, PM
            %M	Minute as a zero-padded decimal number.	00, 01, ..., 59
            %-M	Minute as a decimal number.	0, 1, ..., 59
            %S	Second as a zero-padded decimal number.	00, 01, ..., 59
            %-S	Second as a decimal number.	0, 1, ..., 59
            %f	Microsecond as a decimal number, zero-padded on the left.	000000 - 999999
            %z	UTC offset in the form +HHMM or -HHMM.
            %Z	Time zone name.
            %j	Day of the year as a zero-padded decimal number.	001, 002, ..., 366
            %-j	Day of the year as a decimal number.	1, 2, ..., 366
            %U	Week number of the year (Sunday as the first day of the week).
                All days in a new year preceding the first Sunday are considered to be in week 0.	00, 01, ..., 53
            %W	Week number of the year (Monday as the first day of the week).
                All days in a new year preceding the first Monday are considered to be in week 0.	00, 01, ..., 53
            %c	Locale’s appropriate date and time representation.	Mon Sep 30 07:06:05 2013
            %x	Locale’s appropriate date representation.	09/30/13
            %X	Locale’s appropriate time representation.	07:06:05
            %%	A literal '%' character.	%
        time_ending: Defaults to german: " Uhr"  <- note the space before Uhr

    Returns: string of current date and time by default like: 01.06.2021, 10:15:27 Uhr

    """
    return datetime.now().strftime(datetime_format) + time_ending


def resize_cv2_or_pil_image(image, max_width_or_height, use_pil=False, interpolation=None, invert=False):
    """
    Resize an OpenCV or PIL image based on the aspect ratio

    Args:
        image: image to resize
        max_width_or_height: integer, maximum dimension (width or height) in pixels
        use_pil: True to use PIL image as input
        interpolation: choose an interpolation method, default will be nearest
        invert: If invert is true, the max_width_or_height relates to the min_width_or_height

    Returns: resized image

    """
    h_equals_w = False
    if use_pil:
        w = image.size[0]
        h = image.size[1]
        if interpolation is None:
            interpolation = Image.NEAREST
    else:
        try:
            h, w, c = image.shape
        except ValueError:
            h, w = image.shape

        if interpolation is None:
            interpolation = cv2.INTER_NEAREST

    if h == w:
        h_equals_w = True

    height_larger_width = True
    if h > w:
        percentage = max_width_or_height / w if invert else max_width_or_height / h
    else:
        percentage = max_width_or_height / h if invert else max_width_or_height / w
        height_larger_width = False

    if height_larger_width:
        h = int(round(h * percentage, 1)) if invert else max_width_or_height
        w = max_width_or_height if invert else int(round(w * percentage, 1))
    else:
        h = max_width_or_height if invert else int(round(h * percentage, 1))
        w = int(round(w * percentage, 1)) if invert else max_width_or_height

    if invert:
        assert w >= max_width_or_height
        assert h >= max_width_or_height
    else:
        assert w <= max_width_or_height
        assert h <= max_width_or_height

    if h_equals_w:
        assert h == w

    if use_pil:
        image = image.resize((w, h), interpolation)
    else:
        image = cv2.resize(image, (w, h), interpolation=interpolation)
    return image


def get_gpu_memory():
    """
    Returns: in MiB
    """
    def _output_to_list(x):
        return x.decode('ascii').split('\n')[:-1]

    used = "nvidia-smi --query-gpu=memory.used --format=csv"
    total = "nvidia-smi --query-gpu=memory.total --format=csv"
    memory_used_info = _output_to_list(sp.check_output(used.split()))[1:]
    memory_total_info = _output_to_list(sp.check_output(total.split()))[1:]
    memory_used_values = [int(x.split()[0]) for i, x in enumerate(memory_used_info)]
    memory_total_values = [int(x.split()[0]) for i, x in enumerate(memory_total_info)]
    return memory_used_values, memory_total_values


def get_cpu_memory(in_gb=False):
    # **30 is GiB, **20 MiB
    if in_gb:
        ram_used = psutil.virtual_memory().used / (2 ** 30)
        ram_total = psutil.virtual_memory().total / (2 ** 30)
    else:
        ram_used = psutil.virtual_memory().used / (2 ** 20)
        ram_total = psutil.virtual_memory().total / (2 ** 20)
    return ram_used, ram_total


def show_gpu_memory(device=None):
    if device is None:
        used, total = get_gpu_memory()
        gpu_s = len(used)
        combined = zip(used, total)
        text = []
        for i, (used, total) in enumerate(combined):
            text.append(f'GPU {str(i).rjust(len(str(gpu_s)))}: {used:.0f}/{total:.0f} MiB')
    else:
        used, total = get_gpu_memory()
        if isinstance(device, torch.device):
            device = device.type

        try:
            device_nr = int(device.split(':')[1])

            if device_nr > len(used) - 1:
                print(f'GPU {device_nr} does not exist! Your system has {len(used)} GPUs. Defaulting to GPU 0.')
            used = used[device_nr]
            total = total[device_nr]
        except IndexError:
            device_nr = 0
            used = used[0]
            total = total[0]

        text = [f'GPU {device_nr}: {used:.0f}/{total:.0f} MiB']

    print(f'VRAM used: {"; ".join(text)}')


def show_cpu_memory():
    ram_used, ram_total = get_cpu_memory()
    print(f'Ram usage system: {ram_used:.2f}/{ram_total:.2f} MiB')


def show_memory(device):
    if device == 'cpu':
        show_cpu_memory()
    else:
        show_gpu_memory(device)


def get_memory(device):
    if device == 'cpu':
        used, total = get_cpu_memory()
    else:
        used, total = get_gpu_memory()
        try:
            device_nr = int(device.split(':')[1])

            if device_nr > len(used) - 1:
                print(f'GPU {device_nr} does not exist! Your system has {len(used)} GPUs. Defaulting to GPU 0.')
            used = used[device_nr]
            total = total[device_nr]
        except IndexError:
            used = used[0]
            total = total[0]

    unit = 'MiB'
    return used, total, unit


def get_max_threads(logical=False, max_ram_in_gb=None):
    """
    Project specific maximum number of threads:
    one thread will use around constants.MULTICORE_RAM_RESERVATION GB of RAM (1.5GB)
    spawning too many will fail obviously --> This function prevents that
    """
    ram_used, ram_total = get_cpu_memory(in_gb=True)
    available = ram_total - ram_used
    if max_ram_in_gb is not None:
        threads = int(available / max_ram_in_gb)
    else:
        threads = int(available / constants.MULTICORE_RAM_RESERVATION)

    if threads == 0:
        # Do not use multiprocessing! Stay in same process at your function!
        threads = 1

    # logical are physical threads, otherwise hyperthreading will be used
    max_thread_count = psutil.cpu_count(logical)

    if sys.platform == 'win32' and max_thread_count > 60:
        # python bug: max allowed processes in multithreading is 61!: https://bugs.python.org/issue26903
        # In short: multiprocessing is using a thread per process to track the processes, which is quite wasteful on
        # both unix and windows where nonblocking primitives exist, but nevertheless, thats what it is doing. I
        # need to limit this to 60 processes to avoid the limit in question as a result.
        max_thread_count = 60

    if threads > max_thread_count:
        threads = max_thread_count

    return threads


def ceil(number, decimals):
    """
    rounds up to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1030

    Returns: rounded up number

    """

    return math.ceil((10.0 ** decimals) * number) / (10.0 ** decimals)


def floor(number, decimals):
    """
    rounds down to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1020

    Returns: rounded up number

    """

    return math.floor((10.0 ** decimals) * number) / (10.0 ** decimals)


def round_to_base_number(x, base=5):
    """Example: round_to_number(12, 5) = 10; round_to_number(13, 5) = 15"""
    return base * round(x/base)
