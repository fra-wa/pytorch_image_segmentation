import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(current_dir)
sys.path.extend([module_dir])

from pytorch_segmentation.data_augmentation.image_augmenter import ImageAugmenter
from pytorch_segmentation.user_interaction.parameters import Parameters


if __name__ == '__main__':
    # These are only initial parameters. You are able to change them by runtime
    # i did spaces for better reading at execution
    parameters = [
        # Name              Value   Help
        ('images_folder',   r'',    'Pass the data_folder or Pass the path to the images and masks'),
        ('masks_folder',    r'',    'Pass the path to the masks'),
        ('out_folder',      r'',
         'Optional. Otherwise new images are stored at initial images and masks folder.'),
        ('channels',        3,      '1 for gray, 3 for rgb.'),
        ('classes',         0,      '0 for "dont know" or "want to skip the sanity check after manipulated masks", at '
                                    'least 2 (background, class 1, ...)'),
        ('use_multicore',   True,  'True is faster but no details will be showed except for progress of process 1'),
        ('show_details',    False,  'True shows performed operations on each image, False shows progress.'),
        ('colored_masks',   False,   'If True, the mask is loaded to rgb, else to gray'),
        ('min_dimension',   256,    '0 for no min limit, else every calculated img with height or width smaller than '
                                    'this is not saved.'),
        ('max_augmentations',   30, 'The augmenter is able to create >2k (gray) or >6k (rgb) images out of one '
                                     'image!\nPass 0 to keep all images, or pass an upper boundary.'),
    ]

    parameter_names = [para[0] for para in parameters[:]]
    parameter_values = [para[1] for para in parameters[:]]
    parameter_help = [para[2] for para in parameters[:]]

    parameters = Parameters(parameter_names, parameter_values, parameter_help)
    params = parameters.start_interaction_guide()

    images_folder = params.images_folder
    masks_folder = params.masks_folder
    channels = params.channels
    use_multicore = params.use_multicore
    show_details = params.show_details
    colored_masks = params.colored_masks
    out_folder = params.out_folder if params.out_folder else None

    label_count = params.classes
    if label_count <= 0:
        label_count = None

    min_dimension = params.min_dimension
    if min_dimension <= 0:
        min_dimension = None

    augmentations_per_img = params.max_augmentations
    if augmentations_per_img <= 0:
        augmentations_per_img = None

    augmenter = ImageAugmenter(
        images_folder=images_folder,
        masks_folder=masks_folder,
        channels=channels,
        colored_masks=colored_masks,
        label_count=label_count,
        min_dimension=min_dimension,
        max_augmentations_per_image=augmentations_per_img,
        out_folder=out_folder,
    )
    augmenter.augment(show_details, use_multicore)
