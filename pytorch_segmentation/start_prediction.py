import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(current_dir)
sys.path.extend([module_dir])

from pytorch_segmentation.inference.predict import start_prediction
from pytorch_segmentation.user_interaction.parameters import Parameters


if __name__ == '__main__':
    # These are only initial parameters. You are able to change them by runtime
    # i did spaces for better reading at execution
    parameters = [
        # Name              Value       Help
        ('model_path',
         r'C:\Users\Franz Wagner\projects\model_zoo\segmentation\volume_test_model.pt',
         'pass the path to your model'),
        ('data_folder',
         r'C:\Users\Franz Wagner\projects\segmentation_datasets\test_3d\images\vol_1',
         'Pass the path to the folder containing the reconstruction images'),
        ('input_size', 128, 'You can decide the input size of an image passed to the model, keep an eye on the ram!\n'
         'Be careful: it should be smaller or equal than the images to predict!'),
        ('batch_size', 1, 'In case of a 2D or recurrent model: this is the number of images predicted simultaneously.\n'
         'In case of a 3D model, this is the numbers of volumes predicted simultaneously.'),
        ('device', 'cuda', 'I will try to use cuda if possible, else cpu is forced.'),
        ('save_color', True, 'The model outputs a mask where each class relates to a number starting '
         'from 0 (background) to n classes.\n'
         'save_color=True decodes the mask to human readable colors'),
        ('nice_color', True, 'If this is activated, the colormap is beautiful but not well suited for post-processing'),
        ('overlap', 25, 'Set the percentage of overlap. Represents overlapping % in width, height and depth.'),
    ]

    parameter_names = [para[0] for para in parameters[:]]
    parameter_values = [para[1] for para in parameters[:]]
    parameter_help = [para[2] for para in parameters[:]]

    parameters = Parameters(parameter_names, parameter_values, parameter_help)

    start_prediction(parameters)
