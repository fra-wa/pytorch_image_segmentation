import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation.data_augmentation.pixel_transformer import PixelTransformer
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_file


class PixelTransformerTests(TestCase):
    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')
        self.images_folder = os.path.join(augmentation_folder, 'images')
        self.masks_folder = os.path.join(augmentation_folder, 'masks')
        self.gray_image_path = os.path.join(self.images_folder, 'gray.png')
        self.color_image_path = os.path.join(self.images_folder, 'rgb.jpg')

        self.gray_mask_path = os.path.join(self.masks_folder, 'gray.png')
        self.color_mask_path = os.path.join(self.masks_folder, 'rgb.png')

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        images = get_file_paths_in_folder(self.images_folder)
        for path in images:
            if path != self.gray_image_path and path != self.color_image_path:
                remove_file(path)

        masks = get_file_paths_in_folder(self.masks_folder)
        for path in masks:
            if path != self.gray_mask_path and path != self.color_mask_path:
                remove_file(path)

        sys.stdout = self.previous_out

    def test_color(self):
        pix_transformer = PixelTransformer(
            img_path=self.color_image_path,
            mask_path=self.color_mask_path,
            channels=3,
            colored_masks=True,
            suppress_print=True,
        )
        pix_transformer.transform()
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        # 1 (original) + 21 manipulations
        self.assertEqual(len(new_images), 22)

    def test_gray(self):
        pix_transformer = PixelTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        pix_transformer.transform()
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 10)
        
    def test_less_operations(self):
        pix_transformer = PixelTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        pix_transformer.transform(max_augmentations_per_image=5)
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 6)  # 5 plus original
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

        pix_transformer = PixelTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        pix_transformer.transform(max_augmentations_per_image=15)
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 10)  # only 9 operations for gray
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

        pix_transformer = PixelTransformer(
            img_path=self.color_image_path,
            mask_path=self.color_mask_path,
            channels=3,
            colored_masks=True,
            suppress_print=True,
        )
        pix_transformer.transform(max_augmentations_per_image=15)
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 16)  # only 9 operations for gray
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

        pix_transformer = PixelTransformer(
            img_path=self.color_image_path,
            mask_path=self.color_mask_path,
            channels=3,
            colored_masks=True,
            suppress_print=True,
        )
        max_operations = pix_transformer.max_created_images
        pix_transformer.transform(max_augmentations_per_image=100)
        new_images, new_masks = pix_transformer.all_out_images, pix_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), max_operations)  # only 9 operations for gray
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))
