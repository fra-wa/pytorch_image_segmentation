import cv2
import os
import torch

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.models import UNet3Plus, model_constants
from pytorch_segmentation.models import UNet3PlusBackboned


class UNet3PlusTests(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')

    def test_resnet_backbones(self):
        with torch.no_grad():
            image_paths = get_file_paths_in_folder(self.train_images)
            images = []
            for i in range(5):
                images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

            image_batch = torch.stack(images)
            image_batch = torch.unsqueeze(image_batch, dim=1)
            image_batch.to('cpu')

            backbones = constants.ARCHITECTURES_AND_BACKBONES[UNet3PlusBackboned.__name__]
            for backbone in backbones:
                unet = UNet3PlusBackboned(
                    image_channels=1,
                    num_classes=6,
                    backbone=backbone,
                )
                try:
                    out = unet(image_batch.float())
                except RuntimeError as e:
                    print(f'UNet Resnet backbone failed! Backbone: {backbone}')
                    raise RuntimeError(e)
                self.assertEqual(out.shape[0], 5)
                self.assertEqual(out.shape[1], 6)
                self.assertEqual(out.shape[2], 256)
                self.assertEqual(out.shape[3], 256)

    def test_correct_normalizations(self):
        for norm in model_constants.NORMALIZATIONS:
            if norm == 'gn':
                excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
            elif norm == 'bn':
                excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm3d]
            else:
                raise NotImplementedError()

            unet = UNet3PlusBackboned(
                image_channels=1,
                num_classes=6,
                backbone=constants.ARCHITECTURES_AND_BACKBONES[UNet3PlusBackboned.__name__][0],
                normalization=norm,
            )
            for name, module in unet.named_modules():
                for excluded_norm in excluded_norms:
                    self.assertFalse(isinstance(module, excluded_norm))

    def test_no_backbone(self):
        with torch.no_grad():
            image_paths = get_file_paths_in_folder(self.train_images)
            images = []
            for i in range(5):
                images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

            image_batch = torch.stack(images)
            image_batch = torch.unsqueeze(image_batch, dim=1)
            image_batch.to('cpu')

            unet = UNet3Plus(
                image_channels=1,
                num_classes=6,
            )
            out = unet(image_batch.float())
            self.assertEqual(out.shape[0], 5)
            self.assertEqual(out.shape[1], 6)
            self.assertEqual(out.shape[2], 256)
            self.assertEqual(out.shape[3], 256)
