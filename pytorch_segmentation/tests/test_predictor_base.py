import cv2
import io
import os
import sys

from unittest import TestCase

import numpy as np

from pytorch_segmentation.file_handling.utils import copy_or_move_file_or_files
from pytorch_segmentation.file_handling.utils import create_folder
from pytorch_segmentation.file_handling.utils import remove_dir
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference.predictor_base import multiprocess_images
from pytorch_segmentation.inference.predictor_base import SlicedImage
from pytorch_segmentation.inference.simple_predictors.preditor_base_simple import SimplePredictorBase
from pytorch_segmentation.models import UNet


class MockedPredictor(SimplePredictorBase):
    def __init__(self, folder, input_size, model, channels=1, decode_color=True):
        super(MockedPredictor, self).__init__(
            folder, input_size, model, channels=channels, testcase=True, decode_to_color=decode_color,
        )

    def predict(self, device='cuda'):
        return None


class PredictorBaseTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_input_folder = os.path.join(self.volume_folder, 'temp', 'in')
        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        self.segmented_folder = os.path.join(self.volume_folder, 'segmented')

        self.test_model = UNet(1, 6)

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_input_folder):
            remove_dir(self.temporary_input_folder)

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        if os.path.isdir(self.segmented_folder):
            remove_dir(self.segmented_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

    def test_get_out_path_of_in_path(self):
        predictor = MockedPredictor(self.volume_folder, 256, self.test_model)

        image_path = get_file_paths_in_folder(self.volume_folder)[0]
        out_path = predictor.get_out_path_of_in_path(image_path)
        expected_out = os.path.join(predictor.temp_out_folder, os.path.basename(image_path))
        self.assertEqual(out_path, expected_out)

    def test_decode_segmentation_map(self):
        predictor = MockedPredictor(self.volume_folder, 256, model=self.test_model, decode_color=True)

        class_count = 2
        segmentation_map = np.zeros((4, 5), dtype=np.uint8)
        segmentation_map[2:4, 2:4] = 1
        colored_stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        h, w, c = colored_stack.shape
        self.assertEqual(h, 4)
        self.assertEqual(w, 5)
        self.assertEqual(c, 3)
        unique = np.unique(cv2.cvtColor(colored_stack, cv2.COLOR_BGR2GRAY))
        self.assertEqual(len(unique), 2)

        class_count = 3
        segmentation_map = np.zeros((4, 5), dtype=np.uint8)
        segmentation_map[2:4, 2:4] = 1
        segmentation_map[0:2, 0:2] = 2
        colored_stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        h, w, c = colored_stack.shape
        self.assertEqual(h, 4)
        self.assertEqual(w, 5)
        self.assertEqual(c, 3)
        unique = np.unique(cv2.cvtColor(colored_stack, cv2.COLOR_BGR2GRAY))
        self.assertEqual(len(unique), 3)  # decoding has 128 on two channels --> unique is 2

        class_count = 9
        segmentation_map = np.zeros((4, 5), dtype=np.uint8)
        segmentation_map[2:4, 2:4] = 1
        segmentation_map[0:2, 0:2] = 8
        colored_stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        h, w, c = colored_stack.shape
        self.assertEqual(h, 4)
        self.assertEqual(w, 5)
        self.assertEqual(c, 3)
        unique = np.unique(cv2.cvtColor(colored_stack, cv2.COLOR_BGR2GRAY))
        self.assertEqual(len(unique), 3)  # decoding now also contains a 64 on r channel

        predictor = MockedPredictor(self.volume_folder, 256, model=self.test_model, decode_color=False)
        class_count = 9
        segmentation_map = np.zeros((4, 5), dtype=np.uint8)
        segmentation_map[2:4, 2:4] = 1
        segmentation_map[0:2, 0:2] = 8
        stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        self.assertEqual(len(stack.shape), 2)
        h, w = stack.shape
        for x in range(w):
            for y in range(h):
                self.assertEqual(segmentation_map[y, x], stack[y, x])

        predictor = MockedPredictor(self.volume_folder, 256, model=self.test_model, decode_color=True)
        class_count = 2
        segmentation_map = np.zeros((2, 10, 10), dtype=np.uint8)
        colored_stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        self.assertEqual(colored_stack.shape[0], 2)
        self.assertEqual(colored_stack.shape[1], 10)
        self.assertEqual(colored_stack.shape[2], 10)
        self.assertEqual(colored_stack.shape[3], 3)

        predictor = MockedPredictor(self.volume_folder, 256, model=self.test_model, decode_color=True)
        class_count = 2
        segmentation_map = np.zeros((2, 11, 10, 10), dtype=np.uint8)
        colored_stack = predictor.decode_segmentation_map(segmentation_map, class_count)
        self.assertEqual(colored_stack.shape[0], 2)
        self.assertEqual(colored_stack.shape[1], 11)
        self.assertEqual(colored_stack.shape[2], 10)
        self.assertEqual(colored_stack.shape[3], 10)
        self.assertEqual(colored_stack.shape[4], 3)


class MultiprocessImagesTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        create_folder(os.path.join(self.volume_folder, 'temp'))
        create_folder(self.temporary_output_folder)

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

    def reset_test_environment(self):
        self.tearDown()
        self.setUp()

    def check_new_paths(self, image_paths, new_image_paths, input_size):
        """Test shapes, naming and correct sorting after multiprocessing"""
        for init_path, sequence in zip(image_paths, new_image_paths):
            base_name = os.path.basename(init_path).split('.')[0]
            for idx, path in enumerate(sequence):
                img = cv2.imread(path)
                self.assertEqual(img.shape[0], input_size)
                self.assertEqual(img.shape[1], input_size)
                expected_name_part = f'{base_name}_sub_slice_{str(idx).zfill(3)}_width'
                self.assertIn(expected_name_part, path)
                expected_name_part = 'height'
                self.assertIn(expected_name_part, path)
                self.assertTrue(path.endswith('.png'))

    def test_multiprocess_images_simple(self):
        """Sliding window tests below"""
        image_paths = self.volume_files[:10]
        input_size = 128
        process_nr = 2
        new_image_paths = multiprocess_images(image_paths, self.temporary_output_folder, input_size, process_nr)

        h, w = cv2.imread(image_paths[0]).shape[:2]
        self.assertEqual(h, w)
        self.assertEqual(h, 256)

        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 4 * len(image_paths))

        # test shapes and correct sorting after multiprocessing
        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 256
        new_image_paths = multiprocess_images(image_paths, self.temporary_output_folder, input_size, process_nr)
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 300
        new_image_paths = multiprocess_images(image_paths, self.temporary_output_folder, input_size, process_nr)
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), len(image_paths))

        files = get_file_paths_in_folder(os.path.join(self.volume_folder, 'original'))
        copy_or_move_file_or_files(file_or_file_list=files, target_folder=self.volume_folder)
        remove_dir(os.path.join(self.volume_folder, 'original'))
        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

    def test_multiprocess_images_sliding_window(self):
        image_paths = self.volume_files[:10]
        input_size = 128
        process_nr = 2
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_w=1, overlap_h=1
        )

        h, w = cv2.imread(image_paths[0]).shape[:2]
        self.assertEqual(h, w)
        self.assertEqual(h, 256)

        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 9 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 256
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=1, overlap_w=1
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 300
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=1, overlap_w=1
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), len(image_paths))

        self.assertTrue(os.path.isdir(os.path.join(self.volume_folder, 'original')))
        files = get_file_paths_in_folder(os.path.join(self.volume_folder, 'original'))
        self.assertEqual(len(files), 10)
        copy_or_move_file_or_files(file_or_file_list=files, target_folder=self.volume_folder)
        remove_dir(os.path.join(self.volume_folder, 'original'))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 128
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=0, overlap_w=1
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 6 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 128
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=1, overlap_w=0
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 6 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 80
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=0, overlap_w=40
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 24 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 80
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=40, overlap_w=0
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 24 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()

        input_size = 80
        new_image_paths = multiprocess_images(
            image_paths, self.temporary_output_folder, input_size, process_nr, overlap_h=40, overlap_w=40
        )
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 36 * len(image_paths))

        self.check_new_paths(image_paths, new_image_paths, input_size)
        self.reset_test_environment()


class SlicedImageTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        create_folder(os.path.join(self.volume_folder, 'temp'))
        create_folder(self.temporary_output_folder)

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

    def test_slice_into_parts(self):
        stack = SlicedImage()
        img = get_file_paths_in_folder(self.volume_folder)[0]

        new_images = multiprocess_images([img], self.temporary_output_folder, 16, 2, testcase=True)
        stack.images = new_images[0]
        stack.slice_into_parts(16)

        self.assertEqual(len(stack.parts), 16)

        stack = SlicedImage()
        stack.images = new_images[0]
        stack.slice_into_parts(20)

        self.assertEqual(len(stack.parts), 13)
        self.assertEqual(len(stack.parts[-1]), 16)
