import io
import os
import sys
import torch

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import remove_dir, copy_or_move_file_or_files
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference.simple_predictors.image_predictor import SimpleImagePredictor
from pytorch_segmentation.models import UNetResNet


class SimplePredictorTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_input_folder = os.path.join(self.volume_folder, 'temp', 'in')
        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        self.segmented_folder = os.path.join(self.volume_folder, 'segmented')

        self.test_model = UNetResNet(1, 6, backbone=constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__][0])

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_input_folder):
            remove_dir(self.temporary_input_folder)

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        if os.path.isdir(self.segmented_folder):
            remove_dir(self.segmented_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

        if os.path.isdir(os.path.join(self.volume_folder, 'original')):
            copy_or_move_file_or_files(
                file_or_file_list=get_file_paths_in_folder(os.path.join(self.volume_folder, 'original')),
                target_folder=self.volume_folder
            )
            remove_dir(os.path.join(self.volume_folder, 'original'))

    def test_load_batch_to_tensor(self):
        predictor = SimpleImagePredictor(self.volume_folder, 256, self.test_model, batch_size=2, testcase=True)
        batch = predictor.load_images_to_tensor(self.volume_files)
        self.assertEqual(len(batch.shape), 4)
        self.assertEqual(batch.shape[0], 11)
        self.assertEqual(batch.shape[1], 1)
        self.assertEqual(batch.shape[2], 256)
        self.assertEqual(batch.shape[3], 256)

    def test_create_batched_input_and_output_paths(self):
        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        model = UNetResNet(1, 6, backbone=constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__][0]).to(device)
        predictor = SimpleImagePredictor(self.volume_folder, 256, model, batch_size=6, testcase=True)

        in_batches, out_batches = predictor.create_batched_input_and_output_paths()
        self.assertEqual(len(in_batches), 2)
        self.assertEqual(len(in_batches), len(out_batches))
        self.assertEqual(len(in_batches[0]), 6)
        self.assertEqual(len(in_batches[0]), len(out_batches[0]))
        self.assertEqual(len(in_batches[1]), 5)
        self.assertEqual(len(in_batches[1]), len(out_batches[1]))

        for in_batch_paths, out_batch_paths in zip(in_batches, out_batches):
            for in_path, out_path in zip(in_batch_paths, out_batch_paths):
                self.assertEqual(os.path.basename(in_path), os.path.basename(out_path))

        predictor.batch_size = 1
        in_batches, out_batches = predictor.create_batched_input_and_output_paths()
        self.assertEqual(len(in_batches), 11)
        self.assertEqual(len(in_batches), len(out_batches))
        self.assertEqual(len(in_batches[0]), 1)
        self.assertEqual(len(in_batches[0]), len(out_batches[0]))
        self.assertEqual(len(in_batches[1]), 1)
        self.assertEqual(len(in_batches[1]), len(out_batches[1]))

        for in_batch_paths, out_batch_paths in zip(in_batches, out_batches):
            for in_path, out_path in zip(in_batch_paths, out_batch_paths):
                self.assertEqual(os.path.basename(in_path), os.path.basename(out_path))

    def test_predict(self):
        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

        model = UNetResNet(1, 6, backbone=constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__][0]).to(device)
        predictor = SimpleImagePredictor(self.volume_folder, 256, model, batch_size=6, testcase=True)
        predictor.predict(device=device)

        self.assertTrue(os.path.isdir(self.segmented_folder))

        files = get_file_paths_in_folder(self.temporary_output_folder)
        self.assertEqual(len(self.volume_files), len(files))

        files = get_file_paths_in_folder(self.segmented_folder)
        self.assertEqual(len(self.volume_files), len(files))

        device = torch.device('cpu')
        # now for binary classification
        model = UNetResNet(1, 2, backbone=constants.ARCHITECTURES_AND_BACKBONES[UNetResNet.__name__][0])
        predictor = SimpleImagePredictor(self.volume_folder, 256, model, batch_size=2, testcase=True)
        predictor.predict(device=device)

        self.assertTrue(os.path.isdir(self.segmented_folder))
        files = get_file_paths_in_folder(self.segmented_folder)  # files created above should be overwritten
        self.assertEqual(len(self.volume_files), len(files))
