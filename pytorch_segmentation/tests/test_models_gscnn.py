import warnings

from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import GSCNN


class GSCNNTests(ModelBaseTests2D):
    def test_forward(self):
        with warnings.catch_warnings(record=True):
            self.start_test('GSCNN', GSCNN)
