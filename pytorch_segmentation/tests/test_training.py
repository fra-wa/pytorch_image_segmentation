import io
import os
import sys
import torch

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.models import UNetResNet
from pytorch_segmentation.training.training_execution import get_targets_and_outputs, get_targets_outputs_and_loss, \
    compute_accuracy, train_loop
from pytorch_segmentation.training.training_init import get_loss, valid_loop


class FakeParams:
    def __init__(self,
                 architecture=None,
                 bs=1,
                 in_size=256,
                 classes=6,
                 channels=1,
                 drop_last=True,
                 depth_or_rnn=12,
                 device='cpu',
                 show_ram=False,
                 ):
        self.architecture = architecture
        self.batch_size = bs
        self.input_size = in_size
        self.classes = classes
        self.channels = channels
        self.drop_last = drop_last
        self.depth_3d = depth_or_rnn
        self.rnn_sequence_size = depth_or_rnn
        self.device = device
        self.show_ram = show_ram


class FakeLoader:
    def __init__(self, inputs_targets):
        self.inputs_targets = [inputs_targets]

        class FakeDataset:
            def __init__(self, length):
                self.length = length

            def __len__(self):
                return self.length

        self.dataset = FakeDataset(len(self.inputs_targets))

    def __getitem__(self, item):
        return self.inputs_targets[item]

    def __len__(self):
        return len(self.inputs_targets)


class TrainingTests(TestCase):
    def setUp(self) -> None:
        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.train_images = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))]
        self.train_masks = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'masks', 'volume_1'))]

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

    def test_get_loss(self):
        self.assertTrue(isinstance(get_loss(FakeParams(classes=2)), torch.nn.BCEWithLogitsLoss))
        self.assertTrue(isinstance(get_loss(FakeParams(classes=3)), torch.nn.CrossEntropyLoss))

    def test_get_targets_and_outputs(self):
        model = UNetResNet(1, 2, backbone='resnet18')
        batch_data = (torch.zeros((1, 1, 128, 128)), torch.zeros((1, 128, 128)))

        targets, outputs = get_targets_and_outputs(batch_data[0], batch_data[1], FakeParams(), model)
        self.assertEqual(targets.device, torch.device('cpu'))

        # batch_data = (torch.zeros((1, 1, 128, 128)), torch.zeros((3, 128, 128)))
        # targets, outputs = get_targets_and_outputs(
        #     batch_data,
        #     FakeParams(architecture=no supported model, depth_or_rnn=2),
        #     model,
        # )
        # self.assertEqual(targets.shape[0], 1)

        model = UNetResNet(1, 2, backbone='resnet18', device='cpu')
        batch_data = (torch.zeros((1, 1, 1, 128, 128)), torch.zeros((1, 1, 128, 128)))

        targets, outputs = get_targets_and_outputs(
            batch_data[0], batch_data[1], FakeParams(architecture=constants.UNetResNet_NAME), model
        )
        self.assertEqual(targets.device, torch.device('cpu'))

    def test_get_targets_outputs_and_loss(self):
        model = UNetResNet(1, 2, backbone='resnet18')
        inputs_targets = (torch.zeros((1, 1, 128, 128)), torch.zeros((1, 128, 128)))

        targets, outputs, loss = get_targets_outputs_and_loss(
            inputs_targets[0],
            inputs_targets[1],
            FakeParams(classes=2),
            model,
            get_loss(FakeParams(classes=2)),
        )
        self.assertEqual(targets.device, torch.device('cpu'))

        # model = AStatelessModel(1, 3, backbone='resnet18')
        # inputs_targets = (torch.zeros((1, 1, 128, 128)), torch.zeros((3, 128, 128)))
        # targets, outputs, loss = get_targets_outputs_and_loss(
        #     inputs_targets,
        #     FakeParams(classes=3, architecture=no supported model, depth_or_rnn=2),
        #     model,
        #     get_loss(FakeParams(classes=3)),
        # )
        # self.assertEqual(targets.shape[0], 1)

        model = UNetResNet(1, 2, backbone='resnet18', device='cpu')
        inputs_targets = (torch.zeros((1, 1, 1, 128, 128)), torch.zeros((1, 1, 128, 128)))

        targets, outputs, loss = get_targets_outputs_and_loss(
            inputs_targets[0],
            inputs_targets[1],
            FakeParams(classes=2, architecture=constants.UNetResNet_NAME),
            model,
            get_loss(FakeParams(classes=2)),
        )
        self.assertEqual(targets.device, torch.device('cpu'))

    def test_compute_accuracy(self):
        model = UNetResNet(1, 2, backbone='resnet18')
        inputs, targets = torch.zeros((1, 1, 128, 128)), torch.zeros((1, 128, 128))
        accuracy = compute_accuracy(model(inputs), targets)
        self.assertTrue(0 <= accuracy <= 1)

    def test_train_and_loss_loop(self):
        # actually this only checks that the loops running
        params = FakeParams(classes=2, device='cpu', architecture=constants.UNetResNet_NAME)
        model = UNetResNet(1, 2, backbone='resnet18')
        inputs_targets = (torch.zeros((1, 1, 128, 128)), torch.zeros((1, 128, 128)))
        loss_func = get_loss(params)
        optimizer = torch.optim.Adam(model.parameters())
        loader = FakeLoader(inputs_targets)
        loss, acc, model = train_loop(params, model, loader, loss_func, optimizer)
        self.assertTrue(0 <= acc <= 1)
        valid_loop(params, model, loader, loss_func)
        self.assertTrue(0 <= acc <= 1)
