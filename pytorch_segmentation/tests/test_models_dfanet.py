from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import DFANet


class DFANetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DFANet', DFANet)
