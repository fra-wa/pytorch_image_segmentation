import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation.data_augmentation.geometric_transformer import GeometricTransformer
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_file


class GeometricTransformerTests(TestCase):
    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')
        self.images_folder = os.path.join(augmentation_folder, 'images')
        self.masks_folder = os.path.join(augmentation_folder, 'masks')
        self.gray_image_path = os.path.join(self.images_folder, 'gray.png')
        self.color_image_path = os.path.join(self.images_folder, 'rgb.jpg')

        self.gray_mask_path = os.path.join(self.masks_folder, 'gray.png')
        self.color_mask_path = os.path.join(self.masks_folder, 'rgb.png')

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        images = get_file_paths_in_folder(self.images_folder)
        for path in images:
            if path != self.gray_image_path and path != self.color_image_path:
                remove_file(path)

        masks = get_file_paths_in_folder(self.masks_folder)
        for path in masks:
            if path != self.gray_mask_path and path != self.color_mask_path:
                remove_file(path)

        sys.stdout = self.previous_out

    def test_color(self):
        geo_transformer = GeometricTransformer(
            img_path=self.color_image_path,
            mask_path=self.color_mask_path,
            channels=3,
            colored_masks=True,
            suppress_print=True,
        )
        self.assertEqual(geo_transformer.max_created_images, 360)
        geo_transformer.transform()
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        # 1. rotate --> 12 images, 2. resize --> 24 images
        # now 11 operations on each img -- > 24 + 12 * 24 images = 312
        self.assertEqual(len(new_images), 360)

    def test_gray(self):
        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        self.assertEqual(geo_transformer.max_created_images, 360)
        geo_transformer.transform()
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 360)

    def test_get_operation_dict(self):
        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        base_operation_keys = geo_transformer.base_operations
        additional_operation_keys = geo_transformer.additional_operations

        base_operation_dict, additional_operation_dict = geo_transformer.get_operation_dicts(None)
        for key, value in base_operation_dict.items():
            self.assertIn(key, base_operation_keys)

        for key, value in additional_operation_dict.items():
            self.assertIn(key, additional_operation_keys)

        base_operation_dict, additional_operation_dict = geo_transformer.get_operation_dicts(1000)
        for key, value in base_operation_dict.items():
            self.assertIn(key, base_operation_keys)
        for key, value in additional_operation_dict.items():
            self.assertIn(key, additional_operation_keys)

        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        base_operation_dict, additional_operation_dict = geo_transformer.get_operation_dicts(2)
        combined_keys = list(base_operation_dict.keys()) + list(additional_operation_dict.keys())
        self.assertEqual(len(base_operation_dict.keys()), 1)
        self.assertEqual(len(additional_operation_dict.keys()), 3)
        self.assertEqual(len(combined_keys), 4)

        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        base_operation_dict, additional_operation_dict = geo_transformer.get_operation_dicts(20)
        # formula: 3 rotations + 1 original = 4 base images + 4 additional operations * 4 base images --> 20 out images
        combined_keys = list(base_operation_dict.keys()) + list(additional_operation_dict.keys())
        self.assertEqual(len(base_operation_dict.keys()), 1)
        self.assertEqual(len(additional_operation_dict.keys()), 4)
        self.assertEqual(len(combined_keys), 5)
        self.assertEqual(geo_transformer.max_rotations, 3)

        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        base_operation_dict, additional_operation_dict = geo_transformer.get_operation_dicts(150)
        combined_keys = list(base_operation_dict.keys()) + list(additional_operation_dict.keys())
        self.assertEqual(len(base_operation_dict.keys()), 2)
        self.assertEqual(len(additional_operation_dict.keys()), 9)
        self.assertEqual(len(combined_keys), 11)
        self.assertEqual(geo_transformer.max_rotations, 7)

    def test_less_operations(self):
        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        geo_transformer.transform(max_augmentations_per_image=10)
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 11)  # 10 plus original
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        geo_transformer.transform(max_augmentations_per_image=100)
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 101)  # -> all modified plus original
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

        geo_transformer = GeometricTransformer(
            img_path=self.gray_image_path,
            mask_path=self.gray_mask_path,
            channels=1,
            colored_masks=False,
            suppress_print=True,
        )
        geo_transformer.transform(max_augmentations_per_image=500)
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks
        self.assertEqual(len(self.printed_output.getvalue()), 0)
        self.assertEqual(len(new_images), len(new_masks))
        self.assertEqual(len(new_images), 360)
        for img, mask in zip(new_images, new_masks):
            self.assertTrue(os.path.isfile(img))
            self.assertTrue(os.path.isfile(mask))

