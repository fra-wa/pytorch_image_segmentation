from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import SINet


class SINetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('SINet', SINet)
