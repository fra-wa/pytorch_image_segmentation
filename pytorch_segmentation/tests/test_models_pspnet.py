from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import PSPNet
from pytorch_segmentation.models import PSPDenseNet


class PSPNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('PSPNet', PSPNet)
        self.start_test('PSPDenseNet', PSPDenseNet)
