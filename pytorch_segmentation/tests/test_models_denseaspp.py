from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import DenseASPP121
from pytorch_segmentation.models import DenseASPP161
from pytorch_segmentation.models import DenseASPP169
from pytorch_segmentation.models import DenseASPP201


class DenseASPPTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DenseASPP121', DenseASPP121)
        self.start_test('DenseASPP161', DenseASPP161)
        self.start_test('DenseASPP169', DenseASPP169)
        self.start_test('DenseASPP201', DenseASPP201)
