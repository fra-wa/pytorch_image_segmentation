import io
import numpy as np
import os
import sys
import torch

from unittest import TestCase

from pytorch_segmentation.file_handling.utils import copy_or_move_file_or_files
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_dir
from pytorch_segmentation.inference.sliding_predictors.sliding_image_predictor import SlidingImageCalculator
from pytorch_segmentation.inference.sliding_predictors.sliding_image_predictor import SlidingImagePredictor
from pytorch_segmentation.models import UNet


class SlidingImageCalculatorTests(TestCase):
    def test_add_current_logits(self):
        classes = 6
        height = 10
        width = 10
        logits = np.ones((6, 6, 6), dtype=np.float32)
        calculator = SlidingImageCalculator(classes, height, width)

        calculator.add_current_logits(
            logits=logits,
            h_start=0,
            w_start=0,
            padding=1,
        )

        expected_array = np.zeros((6, 10, 10), dtype=np.float32)
        expected_array[:, :5, :5] = 1
        self.assertTrue(np.array_equal(expected_array, calculator.logits_image))

        calculator.add_current_logits(
            logits=logits,
            h_start=0,
            w_start=0,
            padding=1,
        )
        expected_array[expected_array == 1] = 2
        self.assertTrue(np.array_equal(expected_array, calculator.logits_image))

        logits = np.ones_like(calculator.logits_image)
        calculator.add_current_logits(
            logits=logits,
            h_start=0,
            w_start=0,
            padding=1,
        )
        expected_array += 1
        self.assertTrue(np.array_equal(expected_array, calculator.logits_image))

        logits = np.ones((6, 6, 6), dtype=np.float32)
        calculator.add_current_logits(
            logits=logits,
            h_start=4,
            w_start=0,
            padding=1,
        )
        expected_array[:, 5:, :5] += 1
        self.assertTrue(np.array_equal(expected_array, calculator.logits_image))

        logits = np.ones((6, 6, 6), dtype=np.float32)
        calculator.add_current_logits(
            logits=logits,
            h_start=0,
            w_start=4,
            padding=1,
        )
        expected_array[:, :5, 5:] += 1
        self.assertTrue(np.array_equal(expected_array, calculator.logits_image))

        count_volume = calculator.counting_image
        logits = calculator.logits_image.astype(count_volume.dtype)
        self.assertTrue(np.array_equal(count_volume, logits))

        expected_average = np.ones_like(calculator.logits_image)
        averaged_volume = calculator.get_avg_logits_volume()
        self.assertTrue(np.array_equal(expected_average, averaged_volume))


class SlidingImagePredictorTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_input_folder = os.path.join(self.volume_folder, 'temp', 'in')
        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        self.segmented_folder = os.path.join(self.volume_folder, 'segmented')

        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        self.test_model = UNet(1, 6).to(self.device)

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_input_folder):
            remove_dir(self.temporary_input_folder)

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        if os.path.isdir(self.segmented_folder):
            remove_dir(self.segmented_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

        if os.path.isdir(os.path.join(self.volume_folder, 'original')):
            copy_or_move_file_or_files(
                file_or_file_list=get_file_paths_in_folder(os.path.join(self.volume_folder, 'original')),
                target_folder=self.volume_folder
            )
            remove_dir(os.path.join(self.volume_folder, 'original'))

    def test_load_batch_to_tensor(self):
        predictor = SlidingImagePredictor(self.volume_folder, 256, self.test_model, batch_size=2, testcase=True)
        batch = predictor.load_images_to_tensor(self.volume_files)
        self.assertEqual(len(batch.shape), 4)
        self.assertEqual(batch.shape[0], 11)
        self.assertEqual(batch.shape[1], 1)
        self.assertEqual(batch.shape[2], 256)
        self.assertEqual(batch.shape[3], 256)

    def test_create_batched_input_and_output_paths(self):
        predictor = SlidingImagePredictor(self.volume_folder, 256, self.test_model, batch_size=6, testcase=True)

        in_batches, out_batches = predictor.create_batched_input_and_output_paths()
        self.assertEqual(len(in_batches), 2)
        self.assertEqual(len(in_batches), len(out_batches))
        self.assertEqual(len(in_batches[0]), 6)
        self.assertEqual(len(in_batches[0]), len(out_batches[0]))
        self.assertEqual(len(in_batches[1]), 5)
        self.assertEqual(len(in_batches[1]), len(out_batches[1]))

        for in_batch_paths, out_batch_paths in zip(in_batches, out_batches):
            for in_path, out_path in zip(in_batch_paths, out_batch_paths):
                in_file_name = os.path.basename(in_path).split('.')[0]
                out_file_name = os.path.basename(out_path).split('.')[0]
                self.assertEqual(in_file_name, out_file_name)
                self.assertEqual(os.path.basename(out_path).split('.')[1], 'bin')

        predictor.batch_size = 1
        in_batches, out_batches = predictor.create_batched_input_and_output_paths()
        self.assertEqual(len(in_batches), 11)
        self.assertEqual(len(in_batches), len(out_batches))
        self.assertEqual(len(in_batches[0]), 1)
        self.assertEqual(len(in_batches[0]), len(out_batches[0]))
        self.assertEqual(len(in_batches[1]), 1)
        self.assertEqual(len(in_batches[1]), len(out_batches[1]))

        for in_batch_paths, out_batch_paths in zip(in_batches, out_batches):
            for in_path, out_path in zip(in_batch_paths, out_batch_paths):
                in_file_name = os.path.basename(in_path).split('.')[0]
                out_file_name = os.path.basename(out_path).split('.')[0]
                self.assertEqual(in_file_name, out_file_name)
                self.assertEqual(os.path.basename(out_path).split('.')[1], 'bin')

    def test_predict(self):
        predictor = SlidingImagePredictor(self.volume_folder, 256, self.test_model, batch_size=6, testcase=True)
        predictor.predict(device=self.device)

        self.assertTrue(os.path.isdir(self.segmented_folder))

        files = get_file_paths_in_folder(self.temporary_output_folder)
        self.assertEqual(len(self.volume_files), len(files))

        files = get_file_paths_in_folder(self.segmented_folder)
        self.assertEqual(len(self.volume_files), len(files))

        device = torch.device('cpu')
        # now for binary classification
        predictor = SlidingImagePredictor(self.volume_folder, 129, self.test_model, batch_size=2, testcase=True)
        predictor.predict(device=device)

        self.assertTrue(os.path.isdir(self.temporary_output_folder))
        self.assertEqual(
            len(get_file_paths_in_folder(self.temporary_output_folder)),  # 11 images
            4 * len(get_file_paths_in_folder(self.volume_folder))  # sliced into 4 images per base image
        )

        self.assertTrue(os.path.isdir(self.segmented_folder))
        files = get_file_paths_in_folder(self.segmented_folder)  # files created above should be overwritten
        self.assertEqual(len(self.volume_files), len(files))
