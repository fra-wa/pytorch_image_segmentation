import cv2
import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation.file_handling.utils import copy_or_move_file_or_files
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_dir
from pytorch_segmentation.inference.predictor_base import SlicedVolume
from pytorch_segmentation.inference.sliding_predictors.predictor_base_sliding_window import SlidingPredictorBase
from pytorch_segmentation.models import UNet, UNet3D


class MockedPredictor(SlidingPredictorBase):
    def __init__(self, folder, input_size, model, channels=1, decode_color=True):
        super(MockedPredictor, self).__init__(
            folder, input_size, model, channels=channels, testcase=True, decode_to_color=decode_color,
        )

    def predict(self, device='cuda'):
        return None


class SlidingPredictorTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'predictor_tests')
        self.volume_files = get_file_paths_in_folder(self.volume_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        self.temporary_input_folder = os.path.join(self.volume_folder, 'temp', 'in')
        self.temporary_output_folder = os.path.join(self.volume_folder, 'temp', 'out')
        self.segmented_folder = os.path.join(self.volume_folder, 'segmented')

        self.test_model_3d = UNet3D(1, 6)
        self.test_model_2d = UNet(1, 6)

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

        if os.path.isdir(self.temporary_input_folder):
            remove_dir(self.temporary_input_folder)

        if os.path.isdir(self.temporary_output_folder):
            remove_dir(self.temporary_output_folder)

        if os.path.isdir(self.segmented_folder):
            remove_dir(self.segmented_folder)

        temp_folder = os.path.join(self.volume_folder, 'temp')
        if os.path.isdir(temp_folder):
            remove_dir(temp_folder)

        if os.path.isdir(os.path.join(self.volume_folder, 'original')):
            copy_or_move_file_or_files(
                file_or_file_list=get_file_paths_in_folder(os.path.join(self.volume_folder, 'original')),
                target_folder=self.volume_folder
            )
            remove_dir(os.path.join(self.volume_folder, 'original'))

    def check_new_paths(self, image_paths, new_image_paths, input_size):
        """Test shapes, naming and correct sorting after multiprocessing"""
        for init_path, sequence in zip(image_paths, new_image_paths):
            base_name = os.path.basename(init_path).split('.')[0]
            for idx, path in enumerate(sequence):
                img = cv2.imread(path)
                self.assertEqual(img.shape[0], input_size)
                self.assertEqual(img.shape[1], input_size)
                expected_name_part = f'{base_name}_sub_slice_{str(idx).zfill(3)}_width'
                self.assertIn(expected_name_part, path)
                expected_name_part = 'height'
                self.assertIn(expected_name_part, path)
                self.assertTrue(path.endswith('.png'))

    def test_prepare_input_images(self):
        image_paths = self.volume_files[:11]
        input_size = 150
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        self.assertTrue(os.path.isdir(self.temporary_input_folder))
        self.assertTrue(os.path.isdir(self.temporary_output_folder))
        self.assertTrue(os.path.isdir(self.segmented_folder))

        new_image_paths = predictor.prepare_input_images(threads=1)
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 4 * len(image_paths))

        self.check_new_paths(get_file_paths_in_folder(self.volume_folder), new_image_paths, input_size)

        new_image_paths = predictor.prepare_input_images()
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), 4 * len(image_paths))

        self.check_new_paths(get_file_paths_in_folder(self.volume_folder), new_image_paths, input_size)

        self.tearDown()
        self.setUp()
        input_size = 256
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        self.assertTrue(os.path.isdir(self.temporary_input_folder))
        self.assertTrue(os.path.isdir(self.temporary_output_folder))
        self.assertTrue(os.path.isdir(self.segmented_folder))

        new_image_paths = predictor.prepare_input_images(threads=1)
        self.assertEqual(len(new_image_paths), len(image_paths))
        self.assertEqual(sum(map(len, new_image_paths)), len(image_paths))

        self.tearDown()
        self.setUp()
        input_size = 128
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)

        new_image_paths = predictor.prepare_input_images(threads=1)
        self.assertEqual(len(new_image_paths[0]), 9)

    def test_prepare_sub_stacks_3d(self):
        input_size = 128
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        predictor.prepare_sub_stacks_3d()
        self.assertEqual(len(predictor.image_stacks), 9)

        for vol in predictor.image_stacks:
            self.assertTrue(isinstance(vol, SlicedVolume))

        input_size = 200
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        self.assertEqual(len(predictor.image_stacks), 4)

        input_size = 256
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        self.assertEqual(len(predictor.image_stacks), 1)

        input_size = 300
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_3d)
        self.assertEqual(len(predictor.image_stacks), 1)

        for vol in predictor.image_stacks:
            self.assertTrue(isinstance(vol, SlicedVolume))
            self.assertEqual(len(vol), len(vol.predicted_images))
            self.assertIn('out', vol.predicted_images[0])

        self.assertTrue(os.path.isdir(os.path.join(self.volume_folder, 'original')))
        self.assertEqual(len(get_file_paths_in_folder(os.path.join(self.volume_folder, 'original'))), 11)

    def test_prepare_sub_stacks_2d(self):
        input_size = 256
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_2d)
        self.assertTrue(os.path.isdir(self.temporary_input_folder))
        self.assertTrue(os.path.isdir(self.temporary_output_folder))
        self.assertTrue(os.path.isdir(self.segmented_folder))

        predictor.prepare_sub_stacks_2d()
        self.assertEqual(len(predictor.image_stacks), 11)
        for stack in predictor.image_stacks:
            self.assertEqual(len(stack.images), 1)
            self.assertEqual(len(stack.predicted_images), 1)

        input_size = 129
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_2d)
        predictor.prepare_sub_stacks_2d()
        self.assertEqual(len(predictor.image_stacks), 11)
        for stack in predictor.image_stacks:
            self.assertEqual(len(stack.images), 4)
            self.assertEqual(len(stack.predicted_images), 4)

        input_size = 128
        predictor = MockedPredictor(self.volume_folder, input_size, model=self.test_model_2d)
        predictor.prepare_sub_stacks_2d()
        self.assertEqual(len(predictor.image_stacks), 11)
        for stack in predictor.image_stacks:
            self.assertEqual(len(stack.images), 9)
            self.assertEqual(len(stack.predicted_images), 9)
