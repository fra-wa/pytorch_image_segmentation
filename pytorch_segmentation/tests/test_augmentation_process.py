import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation.data_augmentation.image_augmenter import ImageAugmenter
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_file


class AugmentationTests(TestCase):
    @staticmethod
    def removing_helper(files, files_to_keep):
        indexes_to_pop = []
        for file_to_keep in files_to_keep:
            indexes_to_pop.append(files.index(file_to_keep))
        indexes_to_pop.sort(reverse=True)

        if len(files) > len(indexes_to_pop):
            for idx_to_pop in indexes_to_pop:
                files.pop(idx_to_pop)

            for file in files:
                remove_file(file)

    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')

        self.color_images_folder = os.path.join(augmentation_folder, 'color_img')
        self.color_masks_folder = os.path.join(augmentation_folder, 'color_mask')
        self.gray_images_folder = os.path.join(augmentation_folder, 'gray_img')
        self.gray_masks_folder = os.path.join(augmentation_folder, 'gray_mask')

        self.gray_image_path = os.path.join(self.gray_images_folder, 'gray.png')
        self.gray_mask_path = os.path.join(self.gray_masks_folder, 'gray.png')
        self.color_image_path = os.path.join(self.color_images_folder, 'rgb.jpg')
        self.color_mask_path = os.path.join(self.color_masks_folder, 'rgb.png')

        # if test was stopped by hand, teardown is not called.
        self.removing_helper(get_file_paths_in_folder(self.color_images_folder), [self.color_image_path])
        self.removing_helper(get_file_paths_in_folder(self.color_masks_folder), [self.color_mask_path])
        self.removing_helper(get_file_paths_in_folder(self.gray_images_folder), [self.gray_image_path])
        self.removing_helper(get_file_paths_in_folder(self.gray_masks_folder), [self.gray_mask_path])

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        self.removing_helper(get_file_paths_in_folder(self.color_images_folder), [self.color_image_path])
        self.removing_helper(get_file_paths_in_folder(self.color_masks_folder), [self.color_mask_path])
        self.removing_helper(get_file_paths_in_folder(self.gray_images_folder), [self.gray_image_path])
        self.removing_helper(get_file_paths_in_folder(self.gray_masks_folder), [self.gray_mask_path])

        sys.stdout = self.previous_out

    def test_color(self):
        augmenter = ImageAugmenter(
            images_folder=self.color_images_folder,
            masks_folder=self.color_masks_folder,
            channels=3,
            colored_masks=True,
        )
        augmenter.augment(use_multiprocessing=False, show_details=False, test_case=True)
        created_images = get_file_paths_in_folder(self.color_images_folder)
        created_masks = get_file_paths_in_folder(self.color_masks_folder)
        self.assertEqual(len(created_masks), len(created_images))

    def test_gray(self):
        augmenter = ImageAugmenter(
            images_folder=self.gray_images_folder,
            masks_folder=self.gray_masks_folder,
            channels=1,
            colored_masks=False,
        )
        augmenter.augment(use_multiprocessing=False, show_details=True, test_case=True)
        created_images = get_file_paths_in_folder(self.gray_images_folder)
        created_masks = get_file_paths_in_folder(self.gray_masks_folder)
        self.assertEqual(len(created_masks), len(created_images))

    def test_multiprocessing(self):
        augmenter = ImageAugmenter(
            images_folder=self.gray_images_folder,
            masks_folder=self.gray_masks_folder,
            channels=1,
            colored_masks=False,
            max_augmentations_per_image=100,
        )
        augmenter.augment(use_multiprocessing=True, show_details=True, test_case=True)
        created_images = get_file_paths_in_folder(self.gray_images_folder)
        created_masks = get_file_paths_in_folder(self.gray_masks_folder)
        self.assertEqual(len(created_masks), len(created_images))

    def test_min_dimension(self):
        augmenter = ImageAugmenter(
            images_folder=self.gray_images_folder,
            masks_folder=self.gray_masks_folder,
            channels=1,
            colored_masks=False,
            min_dimension=250,
            max_augmentations_per_image=100,
        )
        augmenter.augment(use_multiprocessing=True, show_details=True, test_case=True)
        created_images = get_file_paths_in_folder(self.gray_images_folder)
        created_masks = get_file_paths_in_folder(self.gray_masks_folder)
        self.assertEqual(len(created_masks), len(created_images))
        try:
            self.assertTrue(len(created_masks) < 2880)
        except AssertionError:
            self.fail('Min dimension did not trigger a reduction of images. This is really unlikely. Check that')

    def test_max_augmentations_per_image(self):
        augmenter = ImageAugmenter(
            images_folder=self.gray_images_folder,
            masks_folder=self.gray_masks_folder,
            channels=1,
            colored_masks=False,
            max_augmentations_per_image=10
        )
        augmenter.augment(use_multiprocessing=False, show_details=True, test_case=True)
        created_images = get_file_paths_in_folder(self.gray_images_folder)
        created_masks = get_file_paths_in_folder(self.gray_masks_folder)

        for img, mask in zip(created_images, created_masks):
            img_name = os.path.basename(img).split('.')[0]
            mask_name = os.path.basename(mask).split('.')[0]
            self.assertEqual(img_name, mask_name)

        self.assertEqual(len(created_masks), len(created_images))
        self.assertEqual(len(created_masks), 11)  # original plus deleted
