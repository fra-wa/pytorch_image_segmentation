from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.models import ESPNet


class ESPNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('ESPNet', ESPNet)
