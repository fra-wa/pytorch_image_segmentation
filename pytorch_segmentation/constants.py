import os

from pathlib import Path


def create_folder(folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)


MULTICORE_RAM_RESERVATION = 4  # GB; minimum ram reservation to load all dependencies and process data
# the ram reservation is needed to determines the number of processes being spawned
SUPPORTED_MASK_TYPES = ['.png', '.bmp', '.tif']
SUPPORTED_INPUT_IMAGE_TYPES = ['.jpg', '.png', '.bmp', '.tif']

current_path = os.path.dirname(__file__)
project_directory = Path(current_path).parent.parent

# Folder Structures
DL_MODELS = os.path.join(project_directory, 'model_zoo')
TRAINED_MODELS_FOLDER = os.path.join(DL_MODELS, 'segmentation', 'trained_model')
TRAIN_MODELS_BACKUP_FOLDER = os.path.join(DL_MODELS, 'segmentation', 'train_backups')

create_folder(DL_MODELS)
create_folder(TRAINED_MODELS_FOLDER)
create_folder(TRAIN_MODELS_BACKUP_FOLDER)

# Data Loading
DATASET_FOLDER = os.path.join(project_directory, 'segmentation_datasets')
create_folder(DATASET_FOLDER)

# Online augmentation configs
ONLINE_AUG_CONFIGS_FOLDER = os.path.join(project_directory, 'pytorch_segmentation', 'configs', 'online_aug')

# Model Names 2D
AttentionUNet_NAME = 'AttentionUNet'
BiSeNet_NAME = 'BiSeNet'
BiSeNetV2_NAME = 'BiSeNetV2'
DANet_NAME = 'DANet'
Dran_NAME = 'Dran'
DeepLabV3Plus_NAME = 'DeepLabV3Plus'
DenseASPP121_NAME = 'DenseASPP121'
DenseASPP161_NAME = 'DenseASPP161'
DenseASPP169_NAME = 'DenseASPP169'
DenseASPP201_NAME = 'DenseASPP201'
DenseNet57_NAME = 'DenseNet57'
DenseNet67_NAME = 'DenseNet67'
DenseNet103_NAME = 'DenseNet103'
DFANet_NAME = 'DFANet'
DFN_NAME = 'DFN'
DeepLabDUCHDC_NAME = 'DeepLabDUCHDC'
ENet_NAME = 'ENet'
ERFNet_NAME = 'ERFNet'
ESPNet_NAME = 'ESPNet'
ExtremeC3Net_NAME = 'ExtremeC3Net'
FastSCNN_NAME = 'FastSCNN'
FCN8_NAME = 'FCN8'
FCN16_NAME = 'FCN16'
FCN32_NAME = 'FCN32'
GCN_NAME = 'GCN'
GSCNN_NAME = 'GSCNN'
HRNet_NAME = 'HRNet'
ICNet_NAME = 'ICNet'
LadderNet_NAME = 'LadderNet'
LEDNet_NAME = 'LEDNet'
OCNet_NAME = 'OCNet'
PSANet_NAME = 'PSANet'
PSPNet_NAME = 'PSPNet'
PSPDenseNet_NAME = 'PSPDenseNet'
R2AttentionUNet_NAME = 'R2AttentionUNet'
R2UNet_NAME = 'R2UNet'
SegNet_NAME = 'SegNet'
SegResNet_NAME = 'SegResNet'
SINet_NAME = 'SINet'
UNet_NAME = 'UNet'
UNetResNet_NAME = 'UNetResNet'
UNet2Plus_NAME = 'UNet2Plus'
UNet3Plus_NAME = 'UNet3Plus'
UNet3PlusBackboned_NAME = 'UNet3PlusBackboned'
UperNet_NAME = 'UperNet'


TWO_D_NETWORKS = [
    AttentionUNet_NAME,
    BiSeNet_NAME,
    BiSeNetV2_NAME,
    DANet_NAME,
    Dran_NAME,
    DeepLabV3Plus_NAME,
    DenseASPP121_NAME,
    DenseASPP161_NAME,
    DenseASPP169_NAME,
    DenseASPP201_NAME,
    DenseNet57_NAME,
    DenseNet67_NAME,
    DenseNet103_NAME,
    DFANet_NAME,
    DFN_NAME,
    DeepLabDUCHDC_NAME,
    ENet_NAME,
    ERFNet_NAME,
    ESPNet_NAME,
    ExtremeC3Net_NAME,
    FastSCNN_NAME,
    FCN8_NAME,
    FCN16_NAME,
    FCN32_NAME,
    GCN_NAME,
    GSCNN_NAME,
    HRNet_NAME,
    ICNet_NAME,
    LadderNet_NAME,
    LEDNet_NAME,
    OCNet_NAME,
    PSANet_NAME,
    PSPNet_NAME,
    PSPDenseNet_NAME,
    R2AttentionUNet_NAME,
    R2UNet_NAME,
    SegNet_NAME,
    SegResNet_NAME,
    SINet_NAME,
    UNet_NAME,
    UNetResNet_NAME,
    UNet2Plus_NAME,
    UNet3Plus_NAME,
    UNet3PlusBackboned_NAME,
    UperNet_NAME,
]

ARCHITECTURES_AND_BACKBONES = {
    AttentionUNet_NAME: [],
    BiSeNet_NAME: [],
    BiSeNetV2_NAME: [],
    DANet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    Dran_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    DeepLabV3Plus_NAME: ['drn', 'xception', 'resnet101'],
    DenseASPP121_NAME: [],
    DenseASPP161_NAME: [],
    DenseASPP169_NAME: [],
    DenseASPP201_NAME: [],
    DenseNet57_NAME: [],
    DenseNet67_NAME: [],
    DenseNet103_NAME: [],
    DFANet_NAME: [],
    DFN_NAME: [],
    DeepLabDUCHDC_NAME: [],
    ENet_NAME: [],
    ERFNet_NAME: [],
    ESPNet_NAME: [],
    ExtremeC3Net_NAME: [],
    FastSCNN_NAME: [],
    FCN8_NAME: [],
    FCN16_NAME: [],
    FCN32_NAME: [],
    GCN_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    GSCNN_NAME: [],
    HRNet_NAME: [],
    ICNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    LadderNet_NAME: [],
    LEDNet_NAME: [],
    OCNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSANet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSPNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSPDenseNet_NAME: ['densenet121', 'densenet161', 'densenet169', 'densenet201'],
    R2AttentionUNet_NAME: [],
    R2UNet_NAME: [],
    SegNet_NAME: [],
    SegResNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    SINet_NAME: [],
    UNet_NAME: [],
    UNetResNet_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    UNet2Plus_NAME: [],
    UNet3Plus_NAME: [],
    UNet3PlusBackboned_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    UperNet_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
}

SUPPORTED_ARCHITECTURES = ARCHITECTURES_AND_BACKBONES.keys()

DEFAULT_RGB_MEAN = [0.485, 0.456, 0.406]
DEFAULT_RGB_STD = [0.229, 0.224, 0.225]

DEFAULT_GRAY_MEAN = [0.5]
DEFAULT_GRAY_STD = [0.5]

# GLOBAL_SEED used if reproduce is true. Needed to get different augmentations for each worker but also
# keep reproducibility.
GLOBAL_SEED = 100
