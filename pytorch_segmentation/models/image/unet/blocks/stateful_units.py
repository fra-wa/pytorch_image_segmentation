"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch
from torch.nn import functional

from .rnn_cells import ConvGRUCell
from .rnn_cells import ConvLSTMCell


class StatefulRnnUnitBase(torch.nn.Module):
    """
    This unit processes an continuous input data stream.
    It remembers information as long as a reset was not explicitly called.

    The unit is able to set up multiple lstm or gru cells to go even deeper.
    Rule of thumb: use 2 cells.
    """
    def __init__(self,
                 rnn_cell,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True,
                 device='cuda',
                 dropout=0.2):
        """

        Args:
            rnn_cell: cell to be used
            in_channels: Number of channels in input
            hidden_channels: List of out channels for each hidden layer.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden channel
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
            dropout: Used for multilayer rnns --> adds dropout to output of cell 0 if cell 1 is existent
        """
        super(StatefulRnnUnitBase, self).__init__()

        if not isinstance(kernel_size, tuple) and not isinstance(kernel_size, list):
            raise ValueError('`kernel_size` must be tuple or list of tuples')

        if isinstance(kernel_size, list):
            # all kernels should be tuples
            if not all([isinstance(elem, tuple) for elem in kernel_size]):
                raise ValueError('`kernel_size` must be tuple or list of tuples')
        else:
            # make list of tuples
            kernel_size = self.extend_for_multilayer(kernel_size, len(hidden_channels))

        self.input_dim = in_channels
        self.hidden_dim = hidden_channels
        self.kernel_size = kernel_size
        self.num_layers = len(hidden_channels)
        self.bias = bias
        self.hidden_state = None
        self.dropout = dropout

        cell_list = []
        for i in range(self.num_layers):
            cur_input_dim = self.input_dim if i == 0 else self.hidden_dim[i - 1]

            cell_list.append(rnn_cell(
                in_channels=cur_input_dim,
                hidden_channels=self.hidden_dim[i],
                kernel_size=self.kernel_size[i],
                bias=self.bias,
                device=device,
            ))

        self.cell_list = torch.nn.ModuleList(cell_list)

    @staticmethod
    def extend_for_multilayer(param, num_layers):
        if not isinstance(param, list):
            param = [param] * num_layers
        return param

    def init_cells_hidden(self, batch_size, height, width):
        init_states = []
        for i in range(self.num_layers):
            init_states.append(self.cell_list[i].init_hidden(batch_size, height, width))
        return init_states

    def forward(self, input_tensor, reset_state):
        raise NotImplementedError('implement at your class')


class StatefulConvLSTM(StatefulRnnUnitBase):
    """
    1 frame at a time can be predicted where the frame represents an image out of a loop iterating over a video or
    image sequence
    """

    def __init__(self,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True,
                 device='cuda',
                 dropout=0.2):
        """

        Args:
            in_channels: Number of channels in input
            hidden_channels: List of channels for each hidden layer. Length of this sets the number of lstm cells used.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
            return_all_layers: Return the list of computations for all layers for debugging or whatever you need
        """
        super(StatefulConvLSTM, self).__init__(
            ConvLSTMCell,
            in_channels,
            hidden_channels,
            kernel_size,
            bias=bias,
            device=device,
            dropout=dropout,
        )

    def forward(self, input_tensor, reset_state):
        """
        Args:
            input_tensor: Tensor of shape (b, c, h, w).
                b ... batch size
                c ... in channels
                h ... height
                w ... width
            reset_state (bool): When video/image sequence was completed, you need to reset the hidden state

        Returns: last layer output

        """

        batch_size, channels, height, width = input_tensor.shape

        if reset_state or self.hidden_state is None:
            self.hidden_state = self.init_cells_hidden(batch_size, height, width)

        # if instantiated with 0 cells, simply return tensor
        h = input_tensor

        for layer_idx in range(self.num_layers):

            previous_state = self.hidden_state[layer_idx]  # load previous
            if layer_idx != self.num_layers - 1:
                # do not use dropout in last layer
                previous_state = functional.dropout2d(previous_state, p=self.dropout)

            h, c = self.cell_list[layer_idx](input_tensor=input_tensor, previous_state=previous_state)

            self.hidden_state[layer_idx] = [h.detach(), c.detach()]  # save current

        return h


class StatefulConvGRU(StatefulRnnUnitBase):
    """
    Not stateful convolutional gated recurrent unit.

    """

    def __init__(self,
                 in_channels,
                 hidden_channels,
                 kernel_size,
                 bias=True,
                 device='cuda',
                 dropout=0.2):
        """

        Args:
            in_channels: Number of channels in input
            hidden_channels: List of channels for each hidden layer. Length of this sets the number of lstm cells used.
                Rule of thumb: 2 layers. More can be better, must be tested.
                Most likely, more layers will not be better but harder to train.
                Output will have shape of last hidden
            kernel_size (tuple/list of tuples): Overall kernel size of the filter or list of kernel sizes like channels
            bias: CNN Layer bias -> adds learnable bias to the output.
                See also:
                    https://stackoverflow.com/questions/51959507/does-bias-in-the-convolutional-layer-really-make-a-difference-to-the-test-accura
        """
        super(StatefulConvGRU, self).__init__(
            ConvGRUCell,
            in_channels,
            hidden_channels,
            kernel_size,
            bias=bias,
            device=device,
            dropout=dropout,
        )

    def forward(self, input_tensor, reset_state):
        """
        Args:
            input_tensor: 4-D Tensor of shape (b, c, h, w).
                b ... batch size
                c ... in channels
                h ... height
                w ... width
            reset_state (bool): When video/image sequence was completed, you need to reset the hidden state

        Returns: last layer output

        """

        batch_size, channels, height, width = input_tensor.shape

        if reset_state or self.hidden_state is None:
            self.hidden_state = self.init_cells_hidden(batch_size, height, width)

        # if instantiated with 0 cells, simply return tensor
        h = input_tensor

        for layer_idx in range(self.num_layers):

            h = self.hidden_state[layer_idx]  # load previous
            if layer_idx != self.num_layers - 1:
                # do not use dropout in last layer
                h = functional.dropout2d(h, p=self.dropout)

            h = self.cell_list[layer_idx](input_tensor, h)
            input_tensor = h

            self.hidden_state[layer_idx] = h.detach()  # save current

        return h
