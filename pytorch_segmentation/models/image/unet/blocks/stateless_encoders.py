"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from .stateless_units import ConvGRU
from .stateless_units import ConvLSTM

from .conv_relu_blocks import DoubleConvReluBlock


class StatelessRNNEncoderBase(torch.nn.Module):
    def __init__(self, in_channels, hidden_channels, out_channels, rnn_class):
        super(StatelessRNNEncoderBase, self).__init__()

        if hidden_channels[0] != in_channels:
            raise ValueError('Out channels and first channel of hidden channels must be equal.')

        self.conv_block = DoubleConvReluBlock(in_channels, out_channels)
        self.pool = torch.nn.MaxPool2d(kernel_size=2, stride=2)
        self.rnn_layer = rnn_class(in_channels, hidden_channels=hidden_channels, kernel_size=(3, 3))

    def forward(self, x):
        """
        Args:
            x: previous encoded sequence conv block or initial sequence tensor
                [F, C, H, W] --> F .. sequence of frames

        Returns: down sampled tensor, skip connection [1, C, H, W]

        """
        skip_connection = self.rnn_layer(x)
        skip_connection = torch.cat((x[-1, :, :, :].unsqueeze(0), skip_connection), dim=1)
        x = self.conv_block(x)
        # -1 for possible image sequences: we want to investigate last img of sequence
        x = self.pool(x)
        return x, skip_connection


class StatelessEncoderLSTM(StatelessRNNEncoderBase):
    def __init__(self, in_channels, hidden_channels, out_channels):
        super(StatelessEncoderLSTM, self).__init__(in_channels, hidden_channels, out_channels, ConvLSTM)


class StatelessEncoderGRU(StatelessRNNEncoderBase):
    def __init__(self, in_channels, hidden_channels, out_channels):
        super(StatelessEncoderGRU, self).__init__(in_channels, hidden_channels, out_channels, ConvGRU)


class StatelessRUNetCenter(torch.nn.Module):
    def __init__(self, in_channels, hidden_channels, use_gru=True):
        super(StatelessRUNetCenter, self).__init__()

        if hidden_channels[-1] != in_channels:
            raise ValueError('Center in channels and last channel of hidden channels must be equal.')

        rnn_class = ConvGRU if use_gru else ConvLSTM
        self.rnn_layer = rnn_class(in_channels, hidden_channels, kernel_size=(3, 3))

    def forward(self, x):
        skip_connection = self.rnn_layer(x)
        # -1 for possible image sequences: we want to investigate last img of sequence
        return torch.cat((x[-1, :, :, :].unsqueeze(0), skip_connection), dim=1)
