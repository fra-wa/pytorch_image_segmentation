import inspect
import torch

from . import SUPPORTED_MODEL_CLASSES

from pytorch_segmentation import constants
from ..user_interaction.parameters import FixedParameters


def instantiate_model_class(model_class,
                            image_channels,
                            num_classes,
                            normalization=None,
                            backbone=None,
                            pretrained=None,
                            device=None,
                            in_size=None,
                            print_information=True):

    model_name = model_class.__name__

    if print_information:
        print(f'Instantiating model: {model_name} with')
        print(f'      image_channels={image_channels}')
        print(f'      num_classes=   {num_classes}')
        print(f'      normalization= {normalization}')
        if model_name in constants.TWO_D_NETWORKS:
            print(f'      backbone=      {backbone}')
        print(f'      pretrained=    {pretrained}')
        print(f'      device=        {device}')
    if model_name == constants.PSANet_NAME and print_information:
        print(f'      in_size=        {in_size}')

    try:
        model = model_class(
            image_channels=image_channels,
            num_classes=num_classes,
            normalization=normalization,
            backbone=backbone,
            pretrained=pretrained,
            device=device,
            in_size=in_size,
        )
    except TypeError:
        raise NotImplementedError(
            f'Trying to load: {model_name} but got an unexpected argument. '
            f'If you added a new model with specific needs, please ensure compatibility!'
            f'\n'
            f'Expected arguments are: {inspect.signature(model_class)}'
        )
    return model


def get_model(model_name,
              channels,
              classes,
              device,
              backbone=None,
              pretrained=None,
              normalization=None,
              in_size=None,
              print_information=True):
    """

    Args:
        model_name: name is the same like class.__name__
        channels: input channels of the image
        classes: number of classes to segment into
        device: cuda or cpu
        backbone: if a backbone is supported, you can pass one. See at readme for supported ones
        pretrained: Ture or False. if no backbone is supported, this is redundant
        normalization: 'bn' or 'gn' (batch norm or group norm)
        in_size: only for psa net.
        print_information: True -> shows the parameters for initialization, otherwise nothing will be printed

    Returns: the loaded model on the passed device

    """

    if len(SUPPORTED_MODEL_CLASSES) != len(constants.SUPPORTED_ARCHITECTURES):
        raise NotImplementedError(
            f'Length of supported model classes: {len(SUPPORTED_MODEL_CLASSES)} '
            f'is not equal to length of supported models: {len(constants.SUPPORTED_ARCHITECTURES)}!\n'
            f'Please check the SUPPORTED_ARCHITECTURES at:\npytorch_segmentation/constants.py\n'
            f'and the SUPPORTED_MODEL_CLASSES at:\npytorch_segmentation/models/loading.py!'
        )

    for model_class in SUPPORTED_MODEL_CLASSES:
        try:
            assert model_class.__name__ in constants.SUPPORTED_ARCHITECTURES
        except AssertionError:
            raise ValueError(f'The model_name {model_class.__name__} is not in constants.SUPPORTED_ARCHITECTURES')

    model_class = None
    for model_class in SUPPORTED_MODEL_CLASSES:
        if model_class.__name__ == model_name:
            model_class = model_class
            break

    if model_class is None:
        raise NotImplementedError(f'The model {model_name} was not found.\n'
                                  f'Please check pytorch_segmentation/models/loading.py')

    model = instantiate_model_class(
        model_class=model_class,
        image_channels=channels,
        num_classes=classes,
        normalization=normalization,
        backbone=backbone,
        pretrained=pretrained,
        device=device,
        in_size=in_size,
        print_information=print_information,
    )

    return model.to(device)


def load_checkpoint_to_continue_training(params):
    """
    Returns: params with loaded attributes from checkpoint, model, optimizer state dict, current history.
    """
    if not isinstance(params, FixedParameters):
        raise ValueError('Expected a FixedParameters instance as input')
    if not params.model_path:
        raise RuntimeError('Please pass the path to the saved checkpoint.')

    device = torch.device(params.device)
    checkpoint = torch.load(params.model_path, map_location=device)
    checkpoint_keys = checkpoint.keys()

    try:
        params.input_size = checkpoint['image_input_size']
        params.classes = checkpoint['classes']
        params.architecture = checkpoint['architecture']
        params.channels = checkpoint['image_channels']
    except KeyError:
        raise ValueError("This model can't be continued. Missing some keys at checkpoint")

    backbone = None
    norm = 'bn'

    if 'backbone' in checkpoint_keys:
        params.backbone = checkpoint['backbone']
        backbone = checkpoint['backbone']
    if 'normalization' in checkpoint_keys:
        params.norm = checkpoint['normalization']
        norm = checkpoint['normalization']
    if 'drop_last' in checkpoint_keys:
        params.drop_last = checkpoint['drop_last']
    if 'aug_strength' in checkpoint_keys:
        params.aug_strength = checkpoint['aug_strength']
    if 'aug_start_epoch' in checkpoint_keys:
        params.aug_start_epoch = checkpoint['aug_start_epoch']
    if 'aug_ema_range' in checkpoint_keys:
        params.aug_ema_range = checkpoint['aug_ema_range']
    # if 'adaptive_aug' in checkpoint_keys:  # not supported currently
    #     params.adaptive_aug = checkpoint['adaptive_aug']
    if 'online_aug' in checkpoint_keys:
        params.online_aug = checkpoint['online_aug']

    if params.drop_last is None:
        print('Could not find: drop_last in checkpoint. Using default: False')
        params.drop_last = False

    if params.aug_strength is None:
        print('Could not find: aug_strength in checkpoint. Using default: 0')
        params.aug_strength = 0

    if params.aug_start_epoch is None:
        print('Could not find: aug_start_epoch in checkpoint. Using default: 0')
        params.aug_start_epoch = 0

    if params.aug_ema_range is None:
        print('Could not find: aug_ema_range in checkpoint. Using default: 15')
        params.aug_ema_range = 15

    if params.online_aug is None:
        print('Could not find: online_aug in checkpoint. Using default: False')
        params.online_aug = False

    params.pretrained = False

    model = get_model(
        model_name=params.architecture,
        channels=params.channels,
        classes=params.classes,
        device=params.device,
        backbone=backbone,
        pretrained=False,
        normalization=norm,
        in_size=params.input_size,
    )
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()

    return params, model, checkpoint['optimizer_state_dict'], checkpoint['history']


def load_model_from_checkpoint(model_path, device='cuda', print_model_instantiation_info=False):
    """
    Loads a model ready to use for some segmentation task.

    Args:
        model_path: path to the model
        device: cuda or cpu, you decide
        print_model_instantiation_info: during training, this can be useful --> prints some parameters for log files

    Returns: model in eval mode

    """
    checkpoint = torch.load(model_path, map_location=device)
    checkpoint_keys = checkpoint.keys()

    backbone = None
    use_gru = True
    normalization = None
    in_size = None
    dataset_mean = None
    dataset_std = None

    if 'backbone' in checkpoint_keys:
        backbone = checkpoint['backbone']

    if 'use_gru' in checkpoint_keys:
        use_gru = checkpoint['use_gru']

    if 'normalization' in checkpoint_keys:
        normalization = checkpoint['normalization']

    if 'input_size' in checkpoint_keys:
        in_size = checkpoint['input_size']

    if 'dataset_mean' in checkpoint_keys:
        dataset_mean = checkpoint['dataset_mean']

    if 'dataset_std' in checkpoint_keys:
        dataset_std = checkpoint['dataset_std']

    model = get_model(
        model_name=checkpoint['architecture'],
        channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        device=device,
        backbone=backbone,
        pretrained=False,
        use_gru=use_gru,
        normalization=normalization,
        in_size=in_size,
        print_information=print_model_instantiation_info,
    )
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()

    return model, checkpoint['image_channels'], dataset_mean, dataset_std
