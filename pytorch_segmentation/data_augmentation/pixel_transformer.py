import os
import random

from copy import deepcopy

from .base_transformer import BaseTransformer
from .pixel_manipulation import to_gray_and_adaptive_histogram
from .pixel_manipulation import brightness
from .pixel_manipulation import contrast
from .pixel_manipulation import gaussian_filter
from .pixel_manipulation import gaussian_noise
from .pixel_manipulation import to_gray_and_histogram_normalization
from .pixel_manipulation import to_gray_and_non_local_means_filter
from .pixel_manipulation import sharpen
from .pixel_manipulation import random_erasing
from .rgb_manipulation import channel_shuffle
from .rgb_manipulation import color_to_hsv
from .rgb_manipulation import iso_noise
from .rgb_manipulation import random_fog
from .rgb_manipulation import random_rain
from .rgb_manipulation import random_shadow
from .rgb_manipulation import random_snow
from .rgb_manipulation import random_sun_flair
from .rgb_manipulation import rgb_shift
from .rgb_manipulation import solarize
from .rgb_manipulation import to_gray
from .rgb_manipulation import to_sephia
from ..file_handling.utils import create_folder


class PixelTransformer(BaseTransformer):
    def __init__(self, img_path, mask_path, channels, colored_masks, suppress_print=False, out_folder=None):
        """
        Examples:
            transformer = PixelTransformer(img_path, mask_path, 1)
            transformer.transform()
            new_image_paths, new_mask_paths = transformer.out_images, transformer.out_masks

        Args:
            img_path:
            mask_path:
            channels: 1 for gray, 3 rgb, rest is not supported
            colored_masks: True if masks are colored or False if only gray
            suppress_print: If true, nothing will be printed
        """
        super(PixelTransformer, self).__init__(img_path, mask_path, channels, colored_masks, out_folder=out_folder)

        self.name = os.path.basename(img_path).split('.')[0]
        if out_folder is not None:
            images_folder = os.path.join(out_folder, 'images')
            masks_folder = os.path.join(out_folder, 'masks')
            create_folder(images_folder)
            create_folder(masks_folder)
            self.img_base_path = os.path.join(images_folder, 'pix_' + self.name)
            self.mask_base_path = os.path.join(masks_folder, 'pix_' + self.name)
        else:
            self.img_base_path = os.path.join(os.path.dirname(img_path), 'pix_' + self.name)
            self.mask_base_path = os.path.join(os.path.dirname(mask_path), 'pix_' + self.name)

        self.suppress_print = suppress_print

        self.base_operations = [
            'to_gray_and_adaptive_histogram',
            'brightness',
            'contrast',
            'gaussian_filter',
            'gaussian_noise',
            'to_gray_and_histogram_normalization',
            'to_gray_and_non_local_means_filter',
            'sharpen',
            'random_erasing',
        ]

        self.rgb_operations = [
            'channel_shuffle',
            'color_to_hsv',
            'iso_noise',
            'random_fog',
            'random_rain',
            'random_shadow',
            'random_snow',
            'random_sun_flair',
            'rgb_shift',
            'solarize',
            'to_gray',
            'to_sephia',
        ]
        self.operations = self.base_operations if channels == 1 else self.base_operations + self.rgb_operations

    @property
    def max_created_images(self):
        # original + manipulated
        if self.channels == 1:
            return len(self.base_operations) + 1
        elif self.channels == 3:
            return len(self.operations) + 1
        return None

    def transform(self, max_augmentations_per_image=None, *args, **kwargs):
        """
        Executes all operations

        Args:
            max_augmentations_per_image: How many transformations shall be applied to one image
            *args:
            **kwargs:

        Returns:

        """
        if self.channels == 1:
            operation_dict = {
                'to_gray_and_adaptive_histogram': self.to_gray_and_adaptive_histogram,
                'contrast': self.contrast,
                'brightness': self.brightness,
                'gaussian_filter': self.gaussian_filter,
                'gaussian_noise': self.gaussian_noise,
                'to_gray_and_histogram_normalization': self.to_gray_and_histogram_normalization,
                'to_gray_and_non_local_means_filter': self.to_gray_and_non_local_means_filter,
                'sharpen': self.sharpen,
                'random_erasing': self.random_erasing,
            }
        else:
            operation_dict = {
                'to_gray_and_adaptive_histogram': self.to_gray_and_adaptive_histogram,
                'contrast': self.contrast,
                'brightness': self.brightness,
                'gaussian_filter': self.gaussian_filter,
                'gaussian_noise': self.gaussian_noise,
                'to_gray_and_histogram_normalization': self.to_gray_and_histogram_normalization,
                'to_gray_and_non_local_means_filter': self.to_gray_and_non_local_means_filter,
                'sharpen': self.sharpen,
                'random_erasing': self.random_erasing,
                'channel_shuffle': self.channel_shuffle,
                'color_to_hsv': self.color_to_hsv,
                'iso_noise': self.iso_noise,
                'random_fog': self.random_fog,
                'random_rain': self.random_rain,
                'random_shadow': self.random_shadow,
                'random_snow': self.random_snow,
                'random_sun_flair': self.random_sun_flair,
                'rgb_shift': self.rgb_shift,
                'solarize': self.solarize,
                'to_gray': self.to_gray,
                'to_sephia': self.to_sephia,
            }

        if max_augmentations_per_image is not None:
            operations_to_execute = deepcopy(self.operations)
            while len(operations_to_execute) > max_augmentations_per_image:
                index_to_pop = random.randint(0, len(operations_to_execute) - 1)
                operations_to_execute.pop(index_to_pop)

            for operation in self.operations:
                if operation not in operations_to_execute:
                    operation_dict.pop(operation)

        if self.suppress_print:
            for operation in self.operations:
                if operation not in operation_dict.keys():
                    continue
                operation_dict[operation]()

        else:
            print(f'Augmenting image: {self.name}.')
            for operation in self.operations:
                if operation not in operation_dict.keys():
                    continue
                print(f'Starting: {operation}', end='\r')
                operation_dict[operation]()
                print(f'Starting: {operation} - Finished.')

        self.reduce_created_images(max_augmentations_per_image)  # should not be necessary
        if not self.suppress_print:
            print(f'Created {len(self.all_out_images) - 1} new images and masks.')

    def to_gray_and_adaptive_histogram(self):
        self.execute_augmentation_function(to_gray_and_adaptive_histogram)

    def brightness(self):
        self.execute_augmentation_function(brightness)

    def contrast(self):
        self.execute_augmentation_function(contrast)

    def gaussian_filter(self):
        self.execute_augmentation_function(gaussian_filter)

    def gaussian_noise(self):
        self.execute_augmentation_function(gaussian_noise)

    def to_gray_and_histogram_normalization(self):
        self.execute_augmentation_function(to_gray_and_histogram_normalization)

    def to_gray_and_non_local_means_filter(self):
        self.execute_augmentation_function(to_gray_and_non_local_means_filter)

    def sharpen(self):
        self.execute_augmentation_function(sharpen)

    def random_erasing(self):
        self.execute_augmentation_function(random_erasing)

    def channel_shuffle(self):
        self.execute_augmentation_function(channel_shuffle)

    def color_to_hsv(self):
        self.execute_augmentation_function(color_to_hsv)

    def iso_noise(self):
        self.execute_augmentation_function(iso_noise)

    def random_fog(self):
        self.execute_augmentation_function(random_fog)

    def random_rain(self):
        self.execute_augmentation_function(random_rain)

    def random_shadow(self):
        self.execute_augmentation_function(random_shadow)

    def random_snow(self):
        self.execute_augmentation_function(random_snow)

    def random_sun_flair(self):
        self.execute_augmentation_function(random_sun_flair)

    def rgb_shift(self):
        self.execute_augmentation_function(rgb_shift)

    def solarize(self):
        self.execute_augmentation_function(solarize)

    def to_gray(self):
        self.execute_augmentation_function(to_gray)

    def to_sephia(self):
        self.execute_augmentation_function(to_sephia)
