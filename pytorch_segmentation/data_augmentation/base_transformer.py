import cv2
import os
import random

from copy import deepcopy

from ..file_handling.utils import create_folder
from ..file_handling.utils import remove_file


class BaseTransformer:
    def __init__(self, img_path, mask_path, channels, colored_masks, min_dimension=None, out_folder=None):
        """
        Args:
            img_path:
            mask_path:
            channels: 1 for gray, 3 rgb, rest is not supported
            colored_masks: True if masks are colored or False if only gray
            min_dimension: when passing this, a sanity check will be performed after you call transformer()
                where each image with a shape < min_dimension will be removed and deleted from disk
        """
        if channels not in [1, 3]:
            raise ValueError('Only gray and rgb are supported')

        self.name = os.path.basename(img_path).split('.')[0]
        self.current_file_nr = 0

        if out_folder is not None:
            images_folder = os.path.join(out_folder, 'images')
            masks_folder = os.path.join(out_folder, 'masks')
            create_folder(images_folder)
            create_folder(masks_folder)
            self.img_base_path = os.path.join(images_folder, 'aug_' + self.name)
            self.mask_base_path = os.path.join(masks_folder, 'aug_' + self.name)
        else:
            self.img_base_path = os.path.join(os.path.dirname(img_path), 'aug_' + self.name)
            self.mask_base_path = os.path.join(os.path.dirname(mask_path), 'aug_' + self.name)

        self.channels = channels
        self.min_dimension = min_dimension
        self.colored_masks = colored_masks

        # storing all out images and masks
        self.all_out_images = [img_path]
        self.all_out_masks = [mask_path]

        # list containing all base operations like rotating and resizing and the base images
        self.base_out_images = [img_path]
        self.base_out_masks = [mask_path]

    def transform(self, *args, **kwargs):
        raise NotImplementedError('Implement at your class')

    @staticmethod
    def get_manipulated_file_nr(img_path, manipulation_index):
        try:
            file_nr = int(os.path.basename(img_path).split('.')[0].split('_')[-1]) + 1 + manipulation_index
        except ValueError:
            # file is most likely original
            file_nr = 0
        return file_nr

    def get_file_paths(self, file_nr):
        """
        Args:
            file_nr: expected file number
        """
        img_path = f'{self.img_base_path}_{str(file_nr).zfill(4)}.png'
        mask_path = f'{self.mask_base_path}_{str(file_nr).zfill(4)}.png'
        return img_path, mask_path

    def load_img_mask(self, img_path, mask_path):
        img = cv2.imread(img_path) if self.channels == 3 else cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        mask = cv2.imread(mask_path) if self.colored_masks else cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
        return img, mask

    def save_manipulated(self, img, mask, file_nr, is_base_operation=False):
        """
        Args:
            img:
            mask:
            file_nr: the current file number to save
            is_base_operation: all additional operations are applied on top of the base operations
        """
        img_path, mask_path = self.get_file_paths(file_nr)

        if self.min_dimension is not None:
            if img.shape[0] < self.min_dimension or img.shape[1] < self.min_dimension:
                return

        cv2.imwrite(img_path, img)
        cv2.imwrite(mask_path, mask)
        self.all_out_images.append(img_path)
        self.all_out_masks.append(mask_path)
        if is_base_operation:
            self.base_out_images.append(img_path)
            self.base_out_masks.append(mask_path)

    def execute_augmentation_function(self, function):
        if not callable(function):
            raise ValueError('function must be a callable!')

        # only perform on base images
        images_to_process = deepcopy(self.base_out_images)
        masks_to_process = deepcopy(self.base_out_masks)

        for img_path, mask_path in zip(images_to_process, masks_to_process):
            base_img, base_mask = self.load_img_mask(img_path, mask_path)
            img, mask = function(base_img, base_mask)

            self.save_manipulated(img, mask, self.current_file_nr)
            self.current_file_nr += 1

    def reduce_created_images(self, max_augmentations_per_image):
        """
        Correct amount of created images is slightly higher than max count due to multiplication
        (First base then additional).
        This removes the additional ones.
        """
        if max_augmentations_per_image is None or max_augmentations_per_image <= 0:
            return

        original_img = self.all_out_images[0]
        original_mask = self.all_out_masks[0]

        self.all_out_images.pop(0)
        self.all_out_masks.pop(0)

        while len(self.all_out_images) > max_augmentations_per_image:
            random_index = random.randint(0, len(self.all_out_images) - 1)
            img_path = self.all_out_images[random_index]
            mask_path = self.all_out_masks[random_index]
            self.all_out_images.pop(random_index)
            self.all_out_masks.pop(random_index)

            remove_file(img_path)
            remove_file(mask_path)

        self.all_out_images.insert(0, original_img)
        self.all_out_masks.insert(0, original_mask)
