import cv2
import numpy as np
import os
import random

from .geometric_transformer import GeometricTransformer
from .pixel_transformer import PixelTransformer

from .. import constants
from ..file_handling.utils import get_file_paths_in_folder, remove_file, create_folder
from ..multicore_utils import multiprocess_function
from ..multicore_utils import slice_list_equally
from ..multicore_utils import slice_multicore_parts
from ..utils import get_max_threads


def get_geometric_and_pix_image_count(augmentations_per_image, image_channels):
    """

    Args:
        augmentations_per_image:
        image_channels:

    Returns: number of max_images for GeometricTransformer, number of max_images for PixelTransformer

    """
    if augmentations_per_image is None:
        return None, None

    if augmentations_per_image < 10:
        # at least 9 images (10 in total with original) are created. Less is not supported
        return 3, 3

    geo_count = GeometricTransformer('', '', image_channels, False).max_created_images
    pix_count = PixelTransformer('', '', image_channels, False).max_created_images
    max_augmentations = geo_count * pix_count

    difference = max_augmentations - augmentations_per_image
    if difference <= 0:
        return None, None

    found_ratio = False
    geo_count -= - 1
    while not found_ratio:
        total_augmentations = geo_count * pix_count
        if total_augmentations <= augmentations_per_image:
            found_ratio = True
        geo_count -= 1

    # calculate new ratio: almost 1:1
    if geo_count < pix_count:
        geo_count = int(np.sqrt(augmentations_per_image))
        pix_count = geo_count + 1
        return geo_count, pix_count
    else:
        return geo_count, pix_count


def multicore_augmentation(image_paths,
                           mask_paths,
                           channels,
                           colored_masks,
                           process_nr,
                           label_count=None,
                           min_dimension=None,
                           augmentations_per_image=None,
                           out_folder=None):
    total_images = len(image_paths)
    all_out_images = []
    all_out_masks = []

    geo_count, pix_count = get_geometric_and_pix_image_count(augmentations_per_image, channels)

    for idx, (image, mask) in enumerate(zip(image_paths, mask_paths)):
        if process_nr == 1:
            print(f'Progress: {(idx +1 )/total_images * 100:.1f}% ({idx+1}/{total_images})              ', end='\r')
        geo_transformer = GeometricTransformer(
            image,
            mask,
            channels,
            colored_masks,
            suppress_print=True,
            min_dimension=min_dimension,
            out_folder=out_folder,
        )
        geo_transformer.transform(geo_count)
        new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks

        for geo_img, geo_mask in zip(new_images, new_masks):
            pixel_transformer = PixelTransformer(
                geo_img, geo_mask, channels, colored_masks, suppress_print=True, out_folder=out_folder
            )
            pixel_transformer.transform(pix_count)
            all_out_masks += pixel_transformer.all_out_masks
            all_out_images += pixel_transformer.all_out_images

    if label_count is not None:
        if process_nr == 1:
            print('Checking augmented masks.')
        for idx, mask in enumerate(all_out_masks):
            if process_nr == 1:
                print(f'Progress: {(idx + 1) / total_images * 100:.1f}%   ', end='\r')
            mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)
            labels = np.unique(mask).shape[0]
            assert labels <= label_count

    return all_out_images, all_out_masks


class ImageAugmenter:
    def __init__(self,
                 images_folder,
                 masks_folder,
                 channels,
                 colored_masks,
                 label_count=None,
                 min_dimension=None,
                 max_augmentations_per_image=None,
                 out_folder=None):
        """

        Args:
            images_folder:
            masks_folder:
            channels: 1 or 3 for gray or rgb images
            colored_masks: True if the mask contains color or False if only rgb
            label_count: max labels per image
            min_dimension: if an image was geometrically manipulated so that either h or w is < min_dimension, the file
                will not be saved
            max_augmentations_per_image: upper border for one augmented image.
                The augmenter is able to create 2880 (gray) or 6048 (rgb) images out of one image.
        """
        if channels != 1 and channels != 3:
            raise NotImplementedError('Multichannel is not supported at the moment.')

        if label_count is None:
            print('label_count is None. Skipping post sanity check for masks')

        if not os.path.isdir(images_folder):
            raise FileNotFoundError(f'There is no folder named {images_folder}!')

        self.images = get_file_paths_in_folder(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

        if not self.images:
            raise FileNotFoundError(f'The folder {images_folder} does not contain images')

        self.masks = get_file_paths_in_folder(masks_folder, extension=constants.SUPPORTED_MASK_TYPES)

        if len(self.images) != len(self.masks):
            raise RuntimeError('There must be an equal amount of images and masks')

        if label_count is not None:
            print('Checking if label count fits to masks')
            for idx, mask_file in enumerate(self.masks):
                print(f'{(idx + 1) / len(self.masks) * 100:.1f}%', end='\r')
                mask = cv2.imread(mask_file, cv2.IMREAD_GRAYSCALE)
                labels = list(np.unique(mask))
                if len(labels) > label_count:
                    raise ValueError(f'Found more labels ({len(labels)}) than your input label count of {label_count}.\n'
                                     f'Labels found:\n{labels}')
            print('')
            print('Masks are valid.')

        self.images_folder = images_folder
        self.masks_folder = masks_folder
        self.out_folder = out_folder

        if self.out_folder is not None:
            images_folder = os.path.join(out_folder, 'images')
            masks_folder = os.path.join(out_folder, 'masks')
            create_folder(images_folder)
            create_folder(masks_folder)

        self.initial_images_count = len(
            get_file_paths_in_folder(self.images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        )

        self.channels = channels
        self.label_count = label_count
        self.colored_masks = colored_masks
        self.min_dimension = min_dimension
        self.max_augmentations_per_image = max_augmentations_per_image

    def get_multiprocessing_maps(self, test_case):
        threads = get_max_threads(logical=True)
        images_map = slice_multicore_parts(self.images, logical=True, total_maps=threads)
        masks_map = slice_list_equally(self.masks, len(images_map))
        assert len(images_map) == len(masks_map)

        for sub_images, sub_masks in zip(images_map, masks_map):
            for img, mask in zip(sub_images, sub_masks):
                img_name = os.path.basename(img).split('.')[0]
                mask_name = os.path.basename(mask).split('.')[0]
                assert img_name == mask_name

        channels_map = len(images_map) * [self.channels]
        colored_masks_map = len(images_map) * [self.colored_masks]
        process_nr_map = len(images_map) * [0]

        if not test_case:
            # trigger printing
            process_nr_map[0] = 1

        label_count_map = len(images_map) * [self.label_count]
        min_dimensions_map = len(images_map) * [self.min_dimension]
        out_folder_map = len(images_map) * [self.out_folder]
        max_augmentations_per_image_map = len(images_map) * [self.max_augmentations_per_image]

        maps = [
            images_map,
            masks_map,
            channels_map,
            colored_masks_map,
            process_nr_map,
            label_count_map,
            min_dimensions_map,
            max_augmentations_per_image_map,
            out_folder_map,
        ]
        return maps

    def reduce_created_images(self):
        """
        Correct amount of created images is slightly higher than max count due to multiplication (First geo, then pix).
        This removes the additional ones.
        """
        if self.max_augmentations_per_image is None or self.max_augmentations_per_image <= 0:
            return

        if self.out_folder is not None:
            out_images_folder = os.path.join(self.out_folder, 'images')
            out_masks_folder = os.path.join(self.out_folder, 'masks')
        else:
            out_images_folder = self.images_folder
            out_masks_folder = self.masks_folder

        created_images = get_file_paths_in_folder(out_images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        created_masks = get_file_paths_in_folder(out_masks_folder, extension=constants.SUPPORTED_MASK_TYPES)

        for img, mask in zip(self.images, self.masks):
            img_name = os.path.basename(img).split('.')[0]
            mask_name = os.path.basename(mask).split('.')[0]

            related_images = sorted([path for path in created_images if img_name in path])
            related_masks = sorted([path for path in created_masks if mask_name in path])

            if img in related_images:
                idx = related_images.index(img)
                related_images.pop(idx)
                idx = related_masks.index(mask)
                related_masks.pop(idx)

            while len(related_images) > self.max_augmentations_per_image:
                random_index = random.randint(0, len(related_images) - 1)
                img_path = related_images[random_index]
                mask_path = related_masks[random_index]
                remove_file(img_path)
                remove_file(mask_path)
                related_images.pop(random_index)
                related_masks.pop(random_index)

    def augment(self, show_details=True, use_multiprocessing=True, test_case=False):
        """
        This supports multicore processing

        Args:
            show_details: if True, you will get details to every single image which is augmented.
                Otherwise you only get a percentage.
                However: when using multiprocessing, you only get percentage of one process!
            use_multiprocessing: obvious
            test_case: to suppress printing while testing multiprocessing

        Returns:

        """
        initial_images_count = len(self.images)

        if use_multiprocessing:
            threads = get_max_threads()
            if threads == 1:
                use_multiprocessing = False

        print(f'Augmenting {initial_images_count} images.')
        print(f'Some images might not be saved due to too small dimensions originated by geometric calculations.')
        if self.min_dimension is not None:
            print(f'Smaller dimensions than {self.min_dimension}x{self.min_dimension} are not valid.')

        maps = self.get_multiprocessing_maps(test_case)

        if use_multiprocessing and not len(maps[0]) == 1:
            results = multiprocess_function(multicore_augmentation, maps)
            all_out_images = []
            all_out_masks = []
            for result in results:
                all_out_images += result[0]
                all_out_masks += result[1]

        else:
            if show_details:
                suppress_transformer_print = False
            else:
                suppress_transformer_print = True

            total_images = len(self.images)
            all_out_images = []
            all_out_masks = []

            geo_count, pix_count = get_geometric_and_pix_image_count(self.max_augmentations_per_image, self.channels)

            for idx, (image, mask) in enumerate(zip(self.images, self.masks)):
                if suppress_transformer_print:
                    # if suppressed, then we can print the global process
                    print(f'Processing img {idx + 1}/{total_images}   ', end='\r')

                # first geometric transformation
                geo_transformer = GeometricTransformer(
                    image,
                    mask,
                    self.channels,
                    self.colored_masks,
                    suppress_print=suppress_transformer_print,
                    min_dimension=self.min_dimension,
                    out_folder=self.out_folder,
                )
                geo_transformer.transform(geo_count)
                new_images, new_masks = geo_transformer.all_out_images, geo_transformer.all_out_masks

                # now pixel manipulation
                for geo_img, geo_mask in zip(new_images, new_masks):
                    pixel_transformer = PixelTransformer(
                        geo_img,
                        geo_mask,
                        self.channels,
                        self.colored_masks,
                        suppress_print=suppress_transformer_print,
                        out_folder=self.out_folder,
                    )
                    pixel_transformer.transform(pix_count)
                    all_out_images += pixel_transformer.all_out_images
                    all_out_masks += pixel_transformer.all_out_masks

            if suppress_transformer_print:
                print('')

            if self.label_count is not None:
                print('Checking augmented masks.')
                print(f'Progress: 0%   ', end='\r')
                for idx, mask in enumerate(all_out_masks):
                    mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)
                    labels = np.unique(mask).shape[0]
                    assert labels <= self.label_count
                    print(f'Progress: {(idx + 1) / total_images * 100:.1f}%   ', end='\r')
                print('')

        if self.max_augmentations_per_image is not None and self.max_augmentations_per_image != 0:
            self.reduce_created_images()

        all_out_count = len(all_out_images) - self.initial_images_count
        print(f'Finished augmentation. Created: {all_out_count} out of {initial_images_count} images.')
