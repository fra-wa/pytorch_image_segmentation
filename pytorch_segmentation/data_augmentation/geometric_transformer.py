import os
import random

from copy import deepcopy

from .base_transformer import BaseTransformer
from .geometric_transformations import elastic_distortion
from .geometric_transformations import flip_x
from .geometric_transformations import flip_xy
from .geometric_transformations import flip_y
from .geometric_transformations import grid_distortion
from .geometric_transformations import optical_distortion
from .geometric_transformations import random_crop
from .geometric_transformations import random_grid_shuffle
from .geometric_transformations import resize
from .geometric_transformations import rotate_by_angle
from .geometric_transformations import squeeze_height
from .geometric_transformations import squeeze_width
from .geometric_transformations import tilt_backwards
from .geometric_transformations import tilt_forward
from .geometric_transformations import tilt_left
from .geometric_transformations import tilt_right
from ..file_handling.utils import create_folder


class GeometricTransformer(BaseTransformer):
    def __init__(self,
                 img_path,
                 mask_path,
                 channels,
                 colored_masks,
                 min_dimension=None,
                 suppress_print=False,
                 out_folder=None):
        """
        Examples:
            transformer = GeometricTransformer(img_path, mask_path, 1)
            transformer.transform()
            new_image_paths, new_mask_paths = transformer.out_images, transformer.out_masks

        Args:
            img_path:
            mask_path:
            channels: 1 for gray, 3 rgb, rest is not supported
            colored_masks: True if the masks are colored else False
            min_dimension: when passing this, a sanity check will be performed after you call transformer()
                where each image with a shape < min_dimension will be removed and deleted from disk.
            suppress_print: whether or not to show some details
        """
        super(GeometricTransformer, self).__init__(
            img_path, mask_path, channels, colored_masks, min_dimension, out_folder
        )

        self.name = os.path.basename(img_path).split('.')[0]

        if out_folder is not None:
            images_folder = os.path.join(out_folder, 'images')
            masks_folder = os.path.join(out_folder, 'masks')
            create_folder(images_folder)
            create_folder(masks_folder)
            self.img_base_path = os.path.join(images_folder, 'geo_' + self.name)
            self.mask_base_path = os.path.join(masks_folder, 'geo_' + self.name)
        else:
            self.img_base_path = os.path.join(os.path.dirname(img_path), 'geo_' + self.name)
            self.mask_base_path = os.path.join(os.path.dirname(mask_path), 'geo_' + self.name)

        self.suppress_print = suppress_print
        self.rotation_degrees = 30
        # if 2, then -30; 0; +30 is performed, 4: -60; -30; 0; 30; 60;, .... you got it. Will be calculated by runtime
        # if unequal, the above formula is used plus one random step either in negative or positive direction
        self.max_rotations = 360 // self.rotation_degrees - 1  # 11 triggers whole circle

        # these will be performed like: first rotating the base image
        # then keep all rotated images and create a random resized version of each
        self.base_operations = [
            'rotate',
            'resize',
        ]

        # afterwards all additional operations will performed once on top of each already created image
        self.additional_operations = [
            'tilt_backwards',
            'tilt_forward',
            'tilt_left',
            'tilt_right',
            'elastic_distortion',
            'grid_distortion',
            'optical_distortion',
            'random_crop',
            'random_grid_shuffle',
            'squeeze_height',
            'squeeze_width',
            'flip_horizontal',
            'flip_vertical',
            'flip_h_w',
        ]

    @property
    def max_created_images(self):
        additional_operations = len(self.additional_operations)
        total_possible_images = (additional_operations + 1) * 360 // self.rotation_degrees * 2
        return total_possible_images

    def get_operation_dicts(self, max_augmentations_per_image):
        """
        Based on the max_augmentations_per_image, this reduces the rotations and the additional operations
        performed by the transformer

        Args:
            max_augmentations_per_image:

        Returns:

        """
        base_operation_dict = {}
        for base_operation in self.base_operations:
            base_operation_dict[base_operation] = self.__getattribute__(base_operation)

        additional_operation_dict = {}
        for additional_operation in self.additional_operations:
            additional_operation_dict[additional_operation] = self.__getattribute__(additional_operation)

        for key, value in base_operation_dict.items():
            assert key in self.base_operations

        for key, value in additional_operation_dict.items():
            assert key in self.additional_operations

        if max_augmentations_per_image is None or max_augmentations_per_image > self.max_created_images:
            return base_operation_dict, additional_operation_dict

        if max_augmentations_per_image < self.max_created_images:
            # recalculate the amount of created images by adjusting max rotations and additional operations
            resize_multiplier = 2
            self.max_rotations = 1
            additional_operations = 3

            if max_augmentations_per_image < 21:
                # triggers at least 2 * 3 = 6 images (original, one rotation, both 3 times manipulated)
                # if 20: 3 rotations, 1 original, 5 additional --> 20 images (4 * 5)
                base_operation_dict.pop('resize')
                resize_multiplier = 1

            add_rotation = True
            max_rotations_reached = False
            current_base_images = (self.max_rotations + 1) * resize_multiplier
            current_max_created_images = current_base_images + additional_operations * current_base_images

            # first iter: add a rotation, second: add a additional operation, third: add a rotation, ..... until limit
            while current_max_created_images < max_augmentations_per_image:
                if add_rotation and not max_rotations_reached:
                    self.max_rotations += 1
                    if self.max_rotations >= 360 // self.rotation_degrees - 1:
                        max_rotations_reached = True
                    add_rotation = False
                else:
                    additional_operations += 1
                    add_rotation = True

                current_base_images = (self.max_rotations + 1) * resize_multiplier
                current_max_created_images = current_base_images + additional_operations * current_base_images

            operations_to_execute = deepcopy(self.additional_operations)
            while len(operations_to_execute) > additional_operations:
                index_to_pop = random.randint(0, len(operations_to_execute) - 1)
                operations_to_execute.pop(index_to_pop)
        else:
            operations_to_execute = deepcopy(self.additional_operations)

        for operation in self.additional_operations:
            if operation not in operations_to_execute:
                additional_operation_dict.pop(operation)

        return base_operation_dict, additional_operation_dict

    def transform(self, max_augmentations_per_image=None, *args, **kwargs):
        """

        Args:
            max_augmentations_per_image: How many transformations shall be applied to one image
            *args:
            **kwargs:

        Returns:

        """
        base_operation_dict, additional_operation_dict = self.get_operation_dicts(max_augmentations_per_image)
        if not isinstance(base_operation_dict, dict) or not isinstance(additional_operation_dict, dict):
            raise RuntimeError

        if self.suppress_print:
            for operation in self.base_operations:
                if operation not in base_operation_dict.keys():
                    continue
                base_operation_dict[operation]()

            for operation in self.additional_operations:
                if operation not in additional_operation_dict.keys():
                    continue
                additional_operation_dict[operation]()
        else:
            print(f'Augmenting image: {self.name}.')
            for operation in self.base_operations:
                if operation not in base_operation_dict.keys():
                    continue
                print(f'Starting: {operation}', end='\r')
                base_operation_dict[operation]()
                print(f'Starting: {operation} - Finished.')

            for operation in self.additional_operations:
                if operation not in additional_operation_dict.keys():
                    continue
                print(f'Starting: {operation}', end='\r')
                additional_operation_dict[operation]()
                print(f'Starting: {operation} - Finished.')

        self.reduce_created_images(max_augmentations_per_image)
        if not self.suppress_print:
            print(f'Created {len(self.all_out_images) - 1} new images and masks.')

    def rotate(self):
        images_to_process = deepcopy(self.all_out_images)
        masks_to_process = deepcopy(self.all_out_masks)

        for img_path, mask_path in zip(images_to_process, masks_to_process):
            base_img, base_mask = self.load_img_mask(img_path, mask_path)

            if self.max_rotations % 2 != 0:
                if random.randint(0, 1):
                    start_rotation = -self.rotation_degrees * (self.max_rotations // 2) - self.rotation_degrees
                    end_rotation = self.rotation_degrees * (self.max_rotations // 2)
                else:
                    start_rotation = -self.rotation_degrees * (self.max_rotations // 2)
                    end_rotation = self.rotation_degrees * (self.max_rotations // 2) + self.rotation_degrees
            else:
                start_rotation = -self.rotation_degrees * (self.max_rotations // 2)
                end_rotation = self.rotation_degrees * (self.max_rotations // 2)

            # plus 1 to trigger last rotation
            for degrees in range(start_rotation, end_rotation + 1, self.rotation_degrees):
                if degrees == 0:
                    continue
                img, mask = rotate_by_angle(base_img, base_mask, degrees)
                self.save_manipulated(img, mask, self.current_file_nr, is_base_operation=True)
                self.current_file_nr += 1

    def resize(self):
        images_to_process = deepcopy(self.all_out_images)
        masks_to_process = deepcopy(self.all_out_masks)

        for img_path, mask_path in zip(images_to_process, masks_to_process):
            base_img, base_mask = self.load_img_mask(img_path, mask_path)
            scale = random.randint(3, 20) / 10
            img, mask = resize(base_img, base_mask, scale)

            self.save_manipulated(img, mask, self.current_file_nr, is_base_operation=True)
            self.current_file_nr += 1

    @staticmethod
    def get_tilt_factor():
        if random.randint(0, 1):
            start = None
            end = None
        else:
            if random.randint(0, 1):
                start = 0
                end = None
            else:
                start = None
                end = 1
        return start, end

    def execute_random_tilt(self, function):
        if not callable(function) or function not in [tilt_backwards, tilt_forward, tilt_left, tilt_right]:
            raise ValueError('pass one of the tilt functions')

        images_to_process = deepcopy(self.base_out_images)
        masks_to_process = deepcopy(self.base_out_masks)

        for img_path, mask_path in zip(images_to_process, masks_to_process):
            base_img, base_mask = self.load_img_mask(img_path, mask_path)
            start, end = self.get_tilt_factor()
            img, mask = function(base_img, base_mask, start, end)

            self.save_manipulated(img, mask, self.current_file_nr)
            self.current_file_nr += 1

    def tilt_backwards(self):
        self.execute_random_tilt(tilt_backwards)

    def tilt_forward(self):
        self.execute_random_tilt(tilt_forward)

    def tilt_left(self):
        self.execute_random_tilt(tilt_left)

    def tilt_right(self):
        self.execute_random_tilt(tilt_right)

    def elastic_distortion(self):
        self.execute_augmentation_function(elastic_distortion)

    def grid_distortion(self):
        self.execute_augmentation_function(grid_distortion)

    def optical_distortion(self):
        self.execute_augmentation_function(optical_distortion)

    def random_crop(self):
        self.execute_augmentation_function(random_crop)

    def random_grid_shuffle(self):
        self.execute_augmentation_function(random_grid_shuffle)

    def squeeze_height(self):
        self.execute_augmentation_function(squeeze_height)

    def squeeze_width(self):
        self.execute_augmentation_function(squeeze_width)

    def flip_horizontal(self):
        self.execute_augmentation_function(flip_x)

    def flip_vertical(self):
        self.execute_augmentation_function(flip_y)

    def flip_h_w(self):
        self.execute_augmentation_function(flip_xy)
