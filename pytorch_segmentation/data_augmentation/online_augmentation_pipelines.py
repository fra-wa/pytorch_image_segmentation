import cv2
import random

import numpy as np

# image
from .geometric_transformations import elastic_distortion
from .geometric_transformations import execute_random_flip
from .geometric_transformations import execute_random_tilt
from .geometric_transformations import grid_distortion
from .geometric_transformations import optical_distortion
from .geometric_transformations import random_crop
from .geometric_transformations import resize
from .geometric_transformations import rotate_by_angle
from .pixel_manipulation import brightness
from .pixel_manipulation import contrast
from .pixel_manipulation import gaussian_filter
from .pixel_manipulation import gaussian_noise
from .pixel_manipulation import random_erasing
from .rgb_manipulation import channel_shuffle
from .rgb_manipulation import color_to_hsv
from .rgb_manipulation import iso_noise
from .rgb_manipulation import random_fog
from .rgb_manipulation import random_rain
from .rgb_manipulation import random_shadow
from .rgb_manipulation import random_snow
from .rgb_manipulation import random_sun_flair
from .rgb_manipulation import rgb_shift
from .rgb_manipulation import solarize
from .rgb_manipulation import to_gray
from .rgb_manipulation import to_sephia

from ..utils import resize_cv2_or_pil_image


class BaseAugmentFunc:
    def __init__(self, func, channels, probability, *args, **kwargs):
        self.func = func
        self.channels = channels if isinstance(channels, list) else [channels]
        self.probability = probability

        self.expected_input_size = None
        for key, value in kwargs.items():
            self.__setattr__(key, value)

    def __str__(self):
        return f'AugmentFunc_{self.func.__name__}'

    def __call__(self, inputs, targets, global_probability=1.0, *args, **kwargs):
        raise NotImplementedError


class ImageAugmentFunc(BaseAugmentFunc):
    def __init__(self, func, channels, probability, *args, **kwargs):
        super(ImageAugmentFunc, self).__init__(func, channels, probability, *args, **kwargs)

    def _adjust_resized(self, inputs, targets):
        """
        Helper if func is a resizing.
        Needed during augmentation --> then the pipeline will resize the result back to the original if not caught here
        """
        if self.expected_input_size is None:
            raise ValueError(
                'The function ImageAugmentFunc._adjust_resized is only callable if self has the following attribute:\n'
                'expected_input_size'
            )

        in_size = self.expected_input_size  # just for programming/reading --> needs less space

        # crop or fill directly to model_input_size so that the resizing at the end of the
        # pipeline does not undo the resizing operation
        try:
            h, w, c = inputs.shape[:]
        except ValueError:
            h, w = inputs.shape[:]
            c = 1
        # if created volume is too small: fill with mirrored input.
        if h < in_size or w < in_size:
            shape = (in_size, in_size, c) if c != 1 else (in_size, in_size)
            new_inputs = np.zeros(shape, dtype=inputs.dtype)
            new_targets = np.zeros((in_size, in_size), dtype=inputs.dtype)
            h_diff = in_size - h
            w_diff = in_size - w
            h_start = h_diff // 2
            h_end = h_diff - h_start
            w_start = w_diff // 2
            w_end = w_diff - w_start
            new_inputs[h_start:-h_end, w_start:-w_end] = inputs
            new_targets[h_start:-h_end, w_start:-w_end] = targets
            # mirror horizontal and fill like (diff = 5): [0, 1, 2, 3, ...] -> [5, 4, 3, 2, 1, 0, 1, 2, 3, ...]
            mirror_inputs = np.flip(inputs, axis=1)
            mirror_targets = np.flip(targets, axis=1)
            new_inputs[h_start:-h_end, :w_start] = mirror_inputs[:, -1 - w_start:-1]
            new_inputs[h_start:-h_end, -w_end:] = mirror_inputs[:, 1:w_end + 1]
            new_targets[h_start:-h_end, :w_start] = mirror_targets[:, -1 - w_start:-1]
            new_targets[h_start:-h_end, -w_end:] = mirror_targets[:, 1:w_end + 1]
            # mirror vertically
            mirror_inputs = np.flip(new_inputs[h_start:-h_end, :], axis=0)
            mirror_targets = np.flip(new_targets[h_start:-h_end, :], axis=0)
            new_inputs[:h_start, :] = mirror_inputs[-1 - h_start: -1, :]
            new_inputs[-h_end:, :] = mirror_inputs[1:h_end + 1, :]
            new_targets[:h_start, :] = mirror_targets[-1 - h_start: -1, :]
            new_targets[-h_end:, :] = mirror_targets[1:h_end + 1, :]

            inputs = new_inputs
            targets = new_targets

        h, w = inputs.shape[:2]
        if h > in_size or w > in_size:
            h_start = (h - in_size) // 2
            w_start = (w - in_size) // 2
            inputs = inputs[h_start:in_size + h_start, w_start:in_size + w_start]
            targets = targets[h_start:in_size + h_start, w_start:in_size + w_start]

        return inputs, targets

    def __call__(self, inputs, targets, global_probability=1.0, *args, **kwargs):
        """
        Executes function to inputs and targets with a given probability

        If the function accepts further arguments, you can pass those in the keyword arguments.
        See at if elif loop for options
        """
        if global_probability == 0:
            return inputs, targets

        if random.randint(1, 100) / 100 > self.probability * global_probability:
            return inputs, targets

        arguments = []
        # RESIZING
        if 'resize' in str(self) and 'resizing_scale' in kwargs.keys():
            factor = 10 ** 2  # percent exact
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['resizing_scale'])), int(factor * max(kwargs['resizing_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # ROTATION
        elif 'rotate' in str(self) and 'max_rot_angle' in kwargs.keys():
            angle = random.randint(0, kwargs['max_rot_angle'])
            angle = -1 * angle if random.randint(0, 1) else angle
            arguments.append(angle)
        # SQUEEZING
        elif 'squeeze' in str(self) and 'squeeze_scale' in kwargs.keys():
            factor = 10 ** 2
            scale_factor = random.randint(
                # min max to prevent wrong usage
                int(factor * min(kwargs['squeeze_scale'])), int(factor * max(kwargs['squeeze_scale']))
            )
            scale_factor = scale_factor / factor
            arguments.append(scale_factor)
        # TILTING
        elif 'tilt' in str(self) and 'tilting_start_end_factor' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(min(kwargs['tilting_start_end_factor']))
            arguments.append(max(kwargs['tilting_start_end_factor']))

        # Flipping
        elif 'flip' in str(self) and 'vertical_flip_allowed' in kwargs.keys():
            # min max to prevent wrong usage
            arguments.append(kwargs['vertical_flip_allowed'])

        # AUGMENT EXECUTION
        inputs, targets = self.func(inputs, targets, *arguments)

        # In case of resizing: adjust to expected input size
        if self.func == resize:
            inputs, targets = self._adjust_resized(inputs, targets)

        return inputs, targets


class ImageAugmentationPipeline:
    def __init__(self,
                 aug_strength=0.0,
                 expected_input_size=256,

                 # pixel manipulation probabilities
                 brightness_prob=0.15,
                 contrast_prob=0.15,
                 gaussian_filter_prob=0.03,
                 gaussian_noise_prob=0.05,
                 random_erasing_prob=0.05,
                 channel_shuffle_prob=0.05,
                 color_to_hsv_prob=0.05,
                 iso_noise_prob=0.05,
                 random_fog_prob=0.02,
                 random_rain_prob=0.02,
                 random_shadow_prob=0.02,
                 random_snow_prob=0.02,
                 random_sun_flair_prob=0.02,
                 rgb_shift_prob=0.05,
                 solarize_prob=0.05,
                 to_gray_prob=0.05,
                 to_sephia_prob=0.05,

                 # geometric probabilities
                 elastic_distortion_prob=0.05,
                 flip_prob=0.1,
                 grid_distortion_prob=0.05,
                 optical_distortion_prob=0.05,
                 random_crop_prob=0.05,
                 resize_prob=0.15,
                 rotate_by_angle_prob=0.15,
                 tilt_prob=0.05,

                 # further properties:
                 grid_shuffle_active=False,
                 max_rot_angle=30,
                 resizing_scale=(0.8, 1.2),
                 squeeze_scale=(0.8, 1.2),
                 vertical_flip_allowed=False,
                 tilting_start_end_factor=(1/5, 4/5),
                 ):
        """

        Args:
            aug_strength: global augmentation strength. Calculate using the training profiler for adaptive change.
            expected_input_size: to ensure the image will be resized at the end to the network expected size

            ### pixel manipulation probabilities ###
            brightness_prob:
            contrast_prob:
            gaussian_filter_prob:
            gaussian_noise_prob:
            random_erasing_prob:
            channel_shuffle_prob:
            color_to_hsv_prob:
            iso_noise_prob:
            random_fog_prob:
            random_rain_prob:
            random_shadow_prob:
            random_snow_prob:
            random_sun_flair_prob:
            rgb_shift_prob:
            solarize_prob:
            to_gray_prob:
            to_sephia_prob:

            ### geometric probabilities ###
            elastic_distortion_prob:
            flip_prob:
            grid_distortion_prob:
            optical_distortion_prob:
            random_crop_prob:
            resize_prob:
            rotate_by_angle_prob:
            tilt_prob:

            ### MORE OPTIONS for some arguments ###
            grid_shuffle_active:
            max_rot_angle: degrees
            resizing_scale: (min, max)
            squeeze_scale: (min, max)
            vertical_flip_allowed:
            tilting_start_end_factor: (start multiplier, end multiplier), see tilt functions for more explanation
        """

        if aug_strength < 0 or aug_strength > 1:
            raise ValueError('global aug_strength must be between 0 and 1')

        self.global_strength = aug_strength
        self.expected_input_size = expected_input_size
        self.options_dict = {
            'max_rot_angle': max_rot_angle,
            'resizing_scale': resizing_scale,
            'squeeze_scale': squeeze_scale,
            'tilting_start_end_factor': tilting_start_end_factor,
            'vertical_flip_allowed': vertical_flip_allowed,
        }
        if not grid_shuffle_active:
            grid_distortion_prob = 0.0

        self.pixel_manipulating_functions = [
            ImageAugmentFunc(func=brightness, channels=[1, 3], probability=brightness_prob),
            ImageAugmentFunc(func=contrast, channels=[1, 3], probability=contrast_prob),
            ImageAugmentFunc(func=gaussian_filter, channels=[1, 3], probability=gaussian_filter_prob),
            ImageAugmentFunc(func=gaussian_noise, channels=[1, 3], probability=gaussian_noise_prob),
            ImageAugmentFunc(func=random_erasing, channels=[1, 3], probability=random_erasing_prob),
            ImageAugmentFunc(func=channel_shuffle, channels=[3], probability=channel_shuffle_prob),
            ImageAugmentFunc(func=color_to_hsv, channels=[3], probability=color_to_hsv_prob),
            ImageAugmentFunc(func=iso_noise, channels=[3], probability=iso_noise_prob),
            ImageAugmentFunc(func=random_fog, channels=[3], probability=random_fog_prob),
            ImageAugmentFunc(func=random_rain, channels=[3], probability=random_rain_prob),
            ImageAugmentFunc(func=random_shadow, channels=[3], probability=random_shadow_prob),
            ImageAugmentFunc(func=random_snow, channels=[3], probability=random_snow_prob),
            ImageAugmentFunc(func=random_sun_flair, channels=[3], probability=random_sun_flair_prob),
            ImageAugmentFunc(func=rgb_shift, channels=[3], probability=rgb_shift_prob),
            ImageAugmentFunc(func=solarize, channels=[3], probability=solarize_prob),
            ImageAugmentFunc(func=to_gray, channels=[3], probability=to_gray_prob),
            ImageAugmentFunc(func=to_sephia, channels=[3], probability=to_sephia_prob),
        ]
        self.geometric_functions = [
            ImageAugmentFunc(func=elastic_distortion, channels=[1, 3], probability=elastic_distortion_prob),
            ImageAugmentFunc(func=execute_random_flip, channels=[1, 3], probability=flip_prob),
            ImageAugmentFunc(func=grid_distortion, channels=[1, 3], probability=grid_distortion_prob),
            ImageAugmentFunc(func=optical_distortion, channels=[1, 3], probability=optical_distortion_prob),
            ImageAugmentFunc(func=random_crop, channels=[1, 3], probability=random_crop_prob),
            ImageAugmentFunc(
                func=resize,
                channels=[1, 3],
                probability=resize_prob,
                # cropping at end of operation to ensure pipeline does not undo this operation if it was the only
                # executed one
                **{'expected_input_size': expected_input_size},
            ),
            ImageAugmentFunc(func=rotate_by_angle, channels=[1, 3], probability=rotate_by_angle_prob),
            ImageAugmentFunc(func=execute_random_tilt, channels=[1, 3], probability=tilt_prob),
        ]

    def __call__(self, inputs, targets, *args, **kwargs):
        """inputs and targets must be either one! image or one! volume and the related mask"""
        functions = self.geometric_functions + self.pixel_manipulating_functions
        random.shuffle(functions)

        if len(inputs.shape) == 3:
            channels = 3
        else:
            channels = 1

        for augmentation_func in functions:
            if channels not in augmentation_func.channels:
                continue
            inputs, targets = augmentation_func(inputs, targets, self.global_strength, **self.options_dict)

        h, w = inputs.shape[:2]
        if h != self.expected_input_size or w != self.expected_input_size:
            # invert true --> max_width_or_height = min_width_or_height
            inputs = resize_cv2_or_pil_image(
                inputs, max_width_or_height=self.expected_input_size, interpolation=cv2.INTER_NEAREST, invert=True
            )
            targets = resize_cv2_or_pil_image(
                targets, max_width_or_height=self.expected_input_size, interpolation=cv2.INTER_NEAREST, invert=True
            )

            # eventual cropping if image dimensions changed --> using center of image
            h, w = inputs.shape[:2]
            start_h = (h - self.expected_input_size) // 2
            end_h = start_h + self.expected_input_size
            start_w = (w - self.expected_input_size) // 2
            end_w = start_w + self.expected_input_size

            inputs = inputs[start_h:end_h, start_w:end_w]
            targets = targets[start_h:end_h, start_w:end_w]

        return inputs, targets
