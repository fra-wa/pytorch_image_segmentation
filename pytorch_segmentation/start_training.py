import os
import sys
import torch

from collections import OrderedDict

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(current_dir)
sys.path.extend([module_dir])

from pytorch_segmentation import constants
from pytorch_segmentation.training.training_init import start_training
from pytorch_segmentation.user_interaction.parameters import Parameters


def create_supported_architectures_string():
    architecture_names = constants.SUPPORTED_ARCHITECTURES
    text_lengths = [len(name) for name in architecture_names]
    max_length = max(text_lengths) + 3  # for space at end and leading "|" plus space --> "| name "

    max_symbols_per_line = 125
    max_names_per_line = int(max_symbols_per_line / max_length)

    separated_names = []
    sub_list = []

    for idx, name in enumerate(architecture_names):
        name_nr = idx + 1
        if name_nr % max_names_per_line == 0:
            separated_names.append(sub_list)
            sub_list = []
        else:
            sub_list.append(name)

    text = ''
    for line_list in separated_names:
        for name in line_list:
            spaces = max_length - len(name) - 2
            current_element = f'| {name}{spaces * " "}'
            text += current_element
        text += '\n'
    return text


if __name__ == '__main__':
    arch = constants.UNetResNet_NAME

    all_architectures_text = create_supported_architectures_string()

    device_count = torch.cuda.device_count()

    # These are only initial parameters. You are able to change them by runtime
    # i did spaces for better reading at execution
    parameters = OrderedDict({
        # Name: [Value, Help]
        'architecture': [
            arch, f'Select which network you want. Options are:\n{all_architectures_text}'
        ],
        'backbone': [
            'resnet18', 'Read readme to get supported backbones.'
        ],
        'pretrained': [
            True, 'Resnet backbone pretrained or not. If images are grayscale:\n'
                  'pretrained is always false since resnet was trained on color images.'
        ],
        'dataset_name': [
            '', 'Read Readme for more help! Pass the folder name of the training dataset.\n'
                'The folder must contain two sub folders named: "masks" and "images"\n'
                'VALIDATION: you can also put a folder called "validation" at the same\n'
                'level of masks and images. Inside, structure your data as before. Then this\n'
                'folder will be used for validation.'
        ],
        'norm_on_dataset': [
            True, 'If True, the mean and std of the dataset will be used for normalization (recommended).\n'
                  'Otherwise default values are used: \n'
                  f'rgb:  mean = {constants.DEFAULT_RGB_MEAN}; std = {constants.DEFAULT_RGB_STD}\n'
                  f'gray: mean = {constants.DEFAULT_GRAY_MEAN}; std = {constants.DEFAULT_GRAY_STD}'
        ],
        'batch_size': [
            6, 'Images/Volumes/Frame sequences per forward pass while training.'
        ],
        'channels': [
            1, 'input channels 1 for gray, 3 for rgb, others are currently not supported'
        ],
        'classes': [
            2, 'Classes to be segmented -> for binary classification pass 2, '
               'multiclass: int > 2'
        ],
        'epochs': [
            100, 'Number of maximal epochs to run. If auto_train_stop is True, training might stop earlier.'
        ],
        'save_every': [
            1, "Save a model checkpoint every x epochs."
        ],
        'input_size': [
            256, 'Input size in pixels (squared images)'
        ],
        'reproduce': [
            False, 'If True, the random seed will be set to 0. ATTENTION: using deterministic cuDNN backend\n'
                   'is significantly SLOWER!'
        ],
        'norm': [
            'gn', 'Recommended usage: batch size >= 16: use BatchNorm (bn), batch size < 16: use GroupNorm (gn)'
        ],
        'drop_last': [
            False,          'If True, the last loaded batch, which is in most cases smaller, is not used.'
        ],
        'lr': [
            0.001,          'Initial learning rate, will be adapted by Adam optimizer'
        ],
        'show_ram': [
            False,          'Whether to show the used ram (device based) or not'
        ],
        'device': [
            'cuda', 'Device to process on (cpu or cuda)'
        ],
        'gpu': [
            0,              'Which GPU should be used'
        ],
        'online_aug': [
            False, 'If True, the online augmentation will be used. Recommended for large datasets. For small sets, use\n'
                   'offline augmentation'
        ],
        'aug_strength': [
            1.0, 'Initial augmentation strength (0 to 1) if online_aug is True'
        ],
        'aug_start_epoch': [
            0, 'Epoch after which the augmentation is activated. Every previous Epoch is not augmented.'
        ],
        'aug_ema_range': [
            15, 'Range over which the Exponential Moving Average (EMA) of\n'
                'the training/validation accuracy will be calculated.'
        ],
        'auto_train_stop': [
            False, 'If True, the TrainingProfiler will be asked to calculate whether or not to stop the training.\n'
                   'Then, either the epochs or the TrainingProfiler result will stop the training.\n'
                   'It calculates the moving average of the last few epochs to determine if the training \n'
                   'accuracy rises or not. If not (plus aug_strength is maxed out at 1): the training will be stopped.'
        ],
    })

    if not torch.cuda.is_available():
        parameters['device'][1] = 'Not available! CUDA not supported!'
        parameters['device'][0] = 'cpu'
        parameters['gpu'][1] = 'Not available! CUDA not supported!'
    elif device_count == 1:
        parameters['gpu'][1] = 'Not available! Only one graphics card was detected!'
    elif device_count > 1:
        parameters['gpu'][1] = parameters['gpu'][1] + f' ({", ".join([str(i) for i in range(device_count)])})'

    parameter_names = [para for para in parameters.keys()]
    parameter_values = [val[0] for k, val in parameters.items()]
    parameter_help = [val[1] for k, val in parameters.items()]

    parameters = Parameters(parameter_names, parameter_values, parameter_help)

    start_training(parameters)

    print('Finished Training.')
