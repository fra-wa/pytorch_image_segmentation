import cv2
import numpy as np
import os
from pathlib import Path

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import create_folder, copy_or_move_file_or_files, natural_sort_key, \
    remove_dir, folder_contains_files_or_files_in_sub_folders
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference import inference_constants
from pytorch_segmentation.training.transformations import get_gray_image_transformation
from pytorch_segmentation.training.transformations import get_rgb_image_transformation
from pytorch_segmentation.utils import resize_cv2_or_pil_image


def multiprocess_images(image_paths,
                        target_folder,
                        input_size,
                        process_nr,
                        testcase=False,
                        original_folder=None,
                        overlap_w=0,
                        overlap_h=0,
                        ):
    """

    Args:
        image_paths: image paths to be checked
        target_folder: where to store computed images
        input_size: model input size -> images will be sliced if too big or reshaped if too small
        process_nr: if 1, this will output the status, other processes are muted
        testcase: only to mute output while testing: simply redirecting stdout does not work at multiprocessing
        original_folder: some images might be enlarged. the original image will be stored at this folder.
            defaults to: image_path_folder/original
        overlap_w: number of pixels of overlap between subimages in the x direction.
        overlap_h: number of pixels of overlap between subimages in the y direction.

    Returns: list like:
        if img shape = input size or img shape < input size
            [[preprocessed_img_1], [2], [...], ...] or
        if img shape > input size
            [[sliced_sub_img_1_1, sliced_sub_img_1_2, sliced_sub_img_1_n], [...], ...]

    """
    new_image_paths = []

    for idx, img_path in enumerate(image_paths):

        if process_nr == 1 and not testcase:
            print(f'Process {process_nr}: Processed {idx + 1}/{len(image_paths)}', end='\r')

        img = cv2.imread(img_path)

        original_h, original_w = img.shape[:2]

        if original_w < input_size or original_h < input_size:
            if original_folder is None:
                original_folder = os.path.join(os.path.dirname(img_path), 'original')
                create_folder(original_folder)
            copy_or_move_file_or_files(img_path, original_folder, move=True)

            img = resize_cv2_or_pil_image(img, input_size, invert=True)

        img_w = img.shape[1]
        img_h = img.shape[0]

        increment_w = input_size - overlap_w
        increment_h = input_size - overlap_h

        assert increment_w > 0, f'Can not width-overlap {overlap_w} pixels with an input size of {input_size}'
        assert increment_h > 0, f'Can not height-overlap {overlap_h} pixels with an input size of {input_size}'

        w_subindexes = [w for w in range(0, img_w - input_size, increment_w)]
        w_subindexes.append(img_w - input_size)
        h_subindexes = [h for h in range(0, img_h - input_size, increment_h)]
        h_subindexes.append(img_h - input_size)

        new_image_paths_aux = []
        img_name = os.path.basename(img_path).split('.')[0]
        img_extension = os.path.basename(img_path).split('.')[1]

        for w_idx, w in enumerate(w_subindexes):
            for h_idx, h in enumerate(h_subindexes):
                stack_nr = str(w_idx * len(h_subindexes) + h_idx).zfill(3)
                new_img_name = f'{img_name}_sub_slice_{stack_nr}_width{w}height{h}.{img_extension}'
                new_img_path = str(Path(target_folder, new_img_name))
                cv2.imwrite(new_img_path, img[h:h + input_size, w:w + input_size])
                new_image_paths_aux.append(new_img_path)
        new_image_paths.append(sorted(new_image_paths_aux, key=natural_sort_key))

    return new_image_paths


class SlicedImage:
    """
    Use this instead of the SlicedVolume for 2D images
    This holds the subimages of one image.
    heights and widths of all images are = network input_size
    """
    def __init__(self):
        self.images = []
        self.parts = []
        self.predicted_images = []  # in case of a sliding window, these are possibly stored logits (.bin)
        # Sliding window attributes:
        self.h_start_list = []
        self.w_start_list = []
        self.img_base_name = None

    def slice_into_parts(self, batch_size):
        """
        Slices itself into n batches. Last batch eventually has less images.
        """
        self.parts = []
        for img in self.images:
            w_start, h_start = os.path.basename(img).split('width')[1].split('height')
            self.w_start_list.append(int(w_start))
            self.h_start_list.append(int(h_start.split('.')[0]))

        self.img_base_name = os.path.basename(self.images[0]).split('_sub_slice_')[0]

        part_start_indexes = [b for b in range(0, len(self.images), batch_size)]
        for i, part_idx in enumerate(part_start_indexes):
            try:
                self.parts.append(self.images[part_idx:part_start_indexes[i + 1]])
            except IndexError:
                self.parts.append(self.images[part_idx:])

    def __len__(self):
        return len(self.images)


class SlicedVolume:
    """
    Use this instead of the SlicedImage for 3D data.
    One image stack represents a subvolume of the 3d data.
        sliced into h = w = network input_size
        and d = len(3D data)
    """
    def __init__(self, h_start=None, w_start=None):
        self.images = []
        self.parts = []
        self.predicted_images = []
        self.predicted_parts = []  # in case of a sliding window: list of are stored binaries (.bin) representing a part
        # Sliding window attributes:
        self.part_start_d = []  # list containing the z coordinate where a part begins
        self.h_start = h_start
        self.w_start = w_start

    def slice_into_parts(self, depth, overlap=None, slice_predicted_images=True):
        """
        Slices itself into n sub image stacks, each of depth: depth_or_bs

        The last part will be: [-depth:]

        Args:
            depth:
            overlap: Use this for 3D networks using a sliding window approach (smoother transitions at
                prediction boundaries).
            slice_predicted_images: The
        """

        if len(self.images) // depth < 1:
            raise RuntimeError(f'Too less slices ({len(self.images)}) for count: {depth}')

        if slice_predicted_images and len(self.images) != len(self.predicted_images):
            raise ValueError('Please add the expected self.predicted_images.')

        self.parts = []
        if overlap is None:
            self.parts = [self.images[i:i + depth] for i in range(0, self.__len__() - depth, depth)]
            self.parts.append(self.images[-depth:])
            if slice_predicted_images:
                self.predicted_parts = [
                    self.predicted_images[i:i + depth] for i in range(0, self.__len__() - depth, depth)
                ]
                self.predicted_parts.append(self.predicted_images[-depth:])
        else:
            if depth - overlap <= 0:
                raise ValueError(f'depth - overlap ({depth} - {overlap}) must be > 0!')

            self.part_start_d = [d for d in range(0, len(self.images) - depth, depth - overlap)]
            self.part_start_d.append(len(self.images) - depth)
            for d in self.part_start_d:
                self.parts.append(self.images[d:d + depth])

            if slice_predicted_images:
                for d in self.part_start_d:
                    basename = self.predicted_images[d].split('.')[0]
                    file = basename + '.bin'
                    self.predicted_parts.append(file)

    def __len__(self):
        return len(self.images)


class PredictorBase:
    def __init__(self,
                 folder,
                 input_size,
                 model,
                 channels=1,
                 testcase=False,
                 decode_to_color=True,
                 nice_color=True,
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """

        Args:
            folder: folder containing images of the volume or 2D images
            input_size: input size of images to pass in (the model should take any squared input size)
            model:
            channels: image channels of the data to predict
            testcase: only to mute output while testing: simply redirecting stdout does not work at multiprocessing
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            nice_color: False results in a simpler decoded map
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """
        self.testcase = testcase
        self.decode_to_color = decode_to_color
        self.use_nice_color = nice_color

        if not os.path.isdir(folder):
            raise FileNotFoundError(f'Could not find folder: "{folder}"')

        images = get_file_paths_in_folder(folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        if not images:
            raise FileNotFoundError(f'Could not find a file at: {folder}')

        h, w = cv2.imread(images[0]).shape[:2]
        self.image_stack_height = h
        self.image_stack_width = w
        self.image_stack_length = len(images)

        self.channels = channels
        if channels == 1:
            self.img_transform = get_gray_image_transformation(dataset_mean, dataset_std)
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation(dataset_mean, dataset_std)
        else:
            raise NotImplementedError(f'{channels} channels are not supported.')

        self.model = model
        _, self.first_layer_conv = self.model.get_first_layer_information()
        self.model.eval()
        self.input_size = input_size

        self.folder = folder

        self.original_folder = os.path.join(folder, 'original')
        create_folder(self.original_folder)

        self.temp_folder = os.path.join(folder, 'temp')
        create_folder(self.temp_folder)

        self.temp_in_folder = os.path.join(self.temp_folder, 'in')
        self.temp_out_folder = os.path.join(self.temp_folder, 'out')
        self.out_folder = os.path.join(folder, 'segmented')
        create_folder(self.temp_in_folder)
        create_folder(self.temp_out_folder)
        create_folder(self.out_folder)

    def get_out_path_of_in_path(self, in_path):
        # temp in path is: .../volume_folder/temp/in/img.png
        # temp out path is: .../volume_folder/temp/out/img.png
        file_name = os.path.basename(in_path)
        out_path = os.path.join(self.temp_out_folder, file_name)
        return out_path

    def remove_temp_folder_if_not_testcase(self):
        if os.path.isdir(self.temp_folder) and not self.testcase:
            remove_dir(self.temp_folder)

    def remove_original_folder_if_empty(self):
        if os.path.isdir(self.original_folder) and \
                not folder_contains_files_or_files_in_sub_folders(self.original_folder):
            remove_dir(self.original_folder)

    def prepare_input_images(self, threads=None):
        raise NotImplementedError('implement at your class')

    def prepare_sub_stacks_3d(self):
        raise NotImplementedError('implement at your class')

    def prepare_sub_stacks_2d(self):
        raise NotImplementedError('implement at your class')

    def map_to_color(self, class_map, class_count):
        """Helper for decode_segmentation_map"""
        decoding_map = inference_constants.BEAUTIFUL_COLORS_RGB if self.use_nice_color else \
            inference_constants.DECODING_COLORS_RGB

        r = np.zeros_like(class_map).astype(np.uint8)
        g = np.zeros_like(class_map).astype(np.uint8)
        b = np.zeros_like(class_map).astype(np.uint8)

        for class_number in range(class_count):
            idx = class_map == class_number
            r[idx] = decoding_map[class_number, 0]
            g[idx] = decoding_map[class_number, 1]
            b[idx] = decoding_map[class_number, 2]

        if len(class_map.shape) == 4:
            colored_map = np.stack([b, g, r], axis=4)
        elif len(class_map.shape) == 3:
            colored_map = np.stack([b, g, r], axis=3)
        elif len(class_map.shape) == 2:
            colored_map = np.stack([b, g, r], axis=2)
        else:
            raise RuntimeError(f'Invalid shape of class_map: {class_map.shape}')

        return colored_map

    def decode_segmentation_map(self, segmentation_map, class_count):
        """
        0 in segmentation map relates to background! If not, you need to change.

        Args:
            segmentation_map: either a batch of volumes or a single volume or a stack of 2D images
                shape for volumes:
                    (b, d, h, w) or (d, h, w)
                shape for images:
                    (b, h, w) or (h, w)
            class_count: max classes, which the model can predict (at least 2 because true or false)

        Returns: decoded image(s) or volume(s)
            shapes if not decoded to color: like input. It is simply returned.
            shapes if decoded to color:
                shape for volumes:
                    (b, d, h, w, 3) or (d, h, w, 3)
                shape for images:
                    (b, h, w, 3) or (h, w, 3)

        """
        if not isinstance(segmentation_map, np.ndarray):
            raise ValueError('Segmentation map must be a numpy array')

        if not self.decode_to_color:
            return segmentation_map

        if class_count > inference_constants.DECODING_COLORS_RGB.shape[0]:
            raise RuntimeError('Cant detect that many classes, increase decoding labels')

        colored_stack = self.map_to_color(segmentation_map, class_count)

        return colored_stack

    def predict(self, device='cuda'):
        raise NotImplementedError('implement at your class')
