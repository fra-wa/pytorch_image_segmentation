import cv2
import numpy as np
import os

import torch.nn

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference.predictor_base import PredictorBase, multiprocess_images, SlicedVolume, SlicedImage
from pytorch_segmentation.multicore_utils import multiprocess_function
from pytorch_segmentation.multicore_utils import slice_multicore_parts
from pytorch_segmentation.utils import get_max_threads


class SimplePredictorBase(PredictorBase):
    def __init__(self,
                 folder,
                 input_size,
                 model,
                 channels=1,
                 testcase=False,
                 decode_to_color=True,
                 nice_color=True,
                 batch_size=None,
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """

        Args:
            folder: folder containing images of the volume or 2D images
            input_size: input size of images to pass in (the model should take any squared input size)
            model:
            channels: image channels of the data to predict
            testcase: only to mute output while testing: simply redirecting stdout does not work at multiprocessing
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            nice_color: False results in a simpler decoded map
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """
        super(SimplePredictorBase, self).__init__(
            folder,
            input_size,
            model,
            channels,
            testcase,
            decode_to_color,
            nice_color,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )

        self.batch_size = batch_size

        self.image_stacks = []
        if isinstance(self.first_layer_conv, torch.nn.Conv3d):
            self.prepare_sub_stacks_3d()
        else:
            self.prepare_sub_stacks_2d()

    def prepare_input_images(self, threads=None):
        """
        Args:
            threads (int): you can set the used threads by yourself

        Returns: prepared image paths

        """
        if threads is None:
            threads = get_max_threads()

        image_paths = get_file_paths_in_folder(self.folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

        image_paths_map = slice_multicore_parts(image_paths, total_maps=threads)
        target_folder_map = len(image_paths_map) * [self.temp_in_folder]
        in_size_map = len(image_paths_map) * [self.input_size]
        process_nr_map = [i + 1 for i in range(len(image_paths_map))]
        testcase_map = [self.testcase for i in range(len(image_paths_map))]

        maps = [
            image_paths_map,
            target_folder_map,
            in_size_map,
            process_nr_map,
            testcase_map,
        ]

        print('Preprocessing images for model..')
        print(f'Some images might be enlarged to fit the model input size.'
              f'\nThe original data is are stored at: {self.original_folder}')
        results = multiprocess_function(multiprocess_images, maps)

        new_image_paths = []
        for image_paths in results:
            new_image_paths += image_paths

        return sorted(new_image_paths)

    def prepare_sub_stacks_3d(self):
        """
        This can be used for volume or sequence prediction.
        Assumption: each image has the same height and width

        Prepares the image stack of self.folder for the self.model.
        This function will create temporary folders for in and out

        Returns: longitudinal split sub stacks of the real img slices (ImageStack instances for each sub stack)

        """
        new_image_paths = self.prepare_input_images()

        image_stacks = [SlicedVolume() for i in range(len(new_image_paths[0]))]

        for split_images in new_image_paths:
            for idx, img in enumerate(sorted(split_images)):
                image_stacks[idx].images.append(img)
                image_stacks[idx].predicted_images.append(self.get_out_path_of_in_path(img))

        self.image_stacks = image_stacks

    def prepare_sub_stacks_2d(self):
        """
        This is used for standard prediction. For 3D, you can use the optimized version for 3D.
        Assumption: The images in the folder are not of same size (but can)
        When using this, you need a custom image recreation. Example at: SimplePredictor

        Prepares the image stack of self.folder for the self.model.
        This function will create temporary folders for in and out

        Returns: longitudinal split sub stacks of the real img slices (ImageStack instances for each sub stack)

        """
        new_image_paths = self.prepare_input_images()

        self.image_stacks = []

        for idx, split_images in enumerate(new_image_paths):
            sliced_image = SlicedImage()
            sliced_image.images = split_images
            sliced_image.predicted_images = [self.get_out_path_of_in_path(path) for path in split_images]
            self.image_stacks.append(sliced_image)

    def predict(self, device='cuda'):
        raise NotImplementedError('implement at your class')

    def combine_sub_images_to_mask(self, image_paths):
        height_list = []
        width_list = []

        for img in image_paths:
            w_start, h_start = os.path.basename(img).split('width')[1].split('height')
            height_list.append(int(h_start.split('.')[0]))
            width_list.append(int(w_start))

        heights = sorted(list(set(height_list)))
        widths = sorted(list(set(width_list)))

        rows = len(heights)
        columns = len(widths)

        # create a list holding the images for every row and column. Example with 9 images:
        # aligned_images = [
        #       [img_top_left,      img_top_middle,     img_top_right],
        #       [img_mid_left,      img_middle,         img_mid_right],
        #       [img_bottom_left,   img_bottom_middle,  img_bottom_right],
        #   ]
        # initialize here since loaded images are not sorted in correct order due to naming
        aligned_images = [columns * [None] for i in range(rows)]
        for img in image_paths:
            w_start, h_start = os.path.basename(img).split('width')[1].split('height')
            h_start = int(h_start.split('.')[0])
            row = heights.index(h_start)
            column = widths.index(int(w_start))
            img = cv2.imread(img) if self.decode_to_color else cv2.imread(img, cv2.IMREAD_GRAYSCALE)
            aligned_images[row][column] = img

        # prepare out mask
        out_height = max(heights) + self.input_size
        out_width = max(widths) + self.input_size

        if self.decode_to_color:
            out_slice = np.zeros((out_height, out_width, 3), dtype=np.uint8)
        else:
            out_slice = np.zeros((out_height, out_width), dtype=np.uint8)

        # now combine the aligned images
        for row_idx, row in enumerate(aligned_images):
            h_start = heights[row_idx]
            h_end = h_start + self.input_size
            for col_idx, column in enumerate(row):
                w_start = widths[col_idx]
                w_end = w_start + self.input_size

                out_slice[h_start:h_end, w_start:w_end] = aligned_images[row_idx][col_idx]

        return out_slice
