import math
import os

import cv2
import torch

from pytorch_segmentation.file_handling.utils import remove_dir
from pytorch_segmentation.inference.predictor_base import SlicedImage
from pytorch_segmentation.inference.simple_predictors.preditor_base_simple import SimplePredictorBase
from pytorch_segmentation.multicore_utils import slice_list_equally


class SimpleImagePredictor(SimplePredictorBase):

    def __init__(self,
                 folder,
                 input_size,
                 model,
                 batch_size,
                 channels=1,
                 testcase=False,
                 decode_to_color=True,
                 nice_color=True,
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """
        Predicts 2D images of a volume without 3D information or simply a folder containing some images

        Args:
            folder: folder of the volume or the images
            input_size: input size of the images
            model: the model
            batch_size: in this case: how many images will be predicted simultaneously
            channels: 1 for gray, 3 rgb, .. others currently not supported
            testcase: true to mute multiprocess print outs while testing
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """
        super(SimpleImagePredictor, self).__init__(
            folder,
            input_size,
            model,
            channels,
            testcase,
            decode_to_color,
            nice_color=nice_color,
            batch_size=batch_size,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
        self.image_stack_height = None
        self.image_stack_width = None

        self.batch_size = batch_size
        for stack in self.image_stacks:
            stack.slice_into_parts(batch_size)

    def load_images_to_tensor(self, image_paths):
        images = []
        # if else outside to speed up loop
        if self.channels == 1:
            for image in image_paths:
                # load img, convert to tensor, add to list
                # each img is transformed to be shaped like [C, H, W]
                images.append(self.img_transform(cv2.imread(image, cv2.IMREAD_GRAYSCALE)))
        elif self.channels == 3:
            for image in image_paths:
                # converting bgr to rgb
                images.append(self.img_transform(cv2.cvtColor(cv2.imread(image, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)))
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        # [B, C, H, W]
        return torch.stack(images)

    @staticmethod
    def decode_logits(out_logits, classes):
        """Assumption: Only one class is true not multiple"""
        if classes == 1:
            # binary classification, out_logits of shape: B, C, H, W
            out = torch.sigmoid(out_logits)
            segmentation_map = (out.squeeze(dim=1) > 0.5).float().cpu().numpy()
            classes = 2

        else:
            # out_logits of shape: B, C, H, W
            values, predictions = torch.max(out_logits, dim=1)
            # predictions of shape: B, H, W
            segmentation_map = predictions.cpu().numpy()

        return segmentation_map, classes

    def save_out_batch(self, out_logits, target_paths):
        batch_size, classes, h, w = out_logits.shape
        decoded_batch, classes = self.decode_logits(out_logits, classes)

        for i in range(batch_size):
            current_map = decoded_batch[i, :, :]
            out_mask = self.decode_segmentation_map(current_map, classes)
            cv2.imwrite(target_paths[i], out_mask)

    def create_segmented_images(self):

        print('Recreating images from predictions...')
        for idx, sliced_image in enumerate(self.image_stacks):
            print(
                f'Process: {idx + 1}/{len(self.image_stacks)} ({(idx + 1) / len(self.image_stacks) * 100:.1f}%)',
                end='\r'
            )
            assert isinstance(sliced_image, SlicedImage)
            combined_out_img = self.combine_sub_images_to_mask(sliced_image.predicted_images)

            file_name = sliced_image.img_base_name
            file_name = file_name + '_mask.png'
            file_name = os.path.join(self.out_folder, file_name)
            cv2.imwrite(file_name, combined_out_img)

        if os.path.isdir(self.temp_folder) and not self.testcase:
            remove_dir(self.temp_folder)

        self.remove_temp_folder_if_not_testcase()
        print('\nFinished prediction!')

    def create_batched_input_and_output_paths(self):
        in_batches = []
        out_batches = []

        buffer_in = []
        buffer_out = []

        for sliced_image in self.image_stacks:
            buffer_in += sliced_image.images
            buffer_out += sliced_image.predicted_images

            if len(buffer_in) >= self.batch_size:
                add_all = True if (len(buffer_in) / self.batch_size).is_integer() else False
                batched_parts = math.ceil(len(buffer_in) / self.batch_size)

                in_paths = slice_list_equally(buffer_in, batched_parts)
                out_paths = slice_list_equally(buffer_out, batched_parts)

                if add_all:
                    in_batches += in_paths
                    out_batches += out_paths
                    buffer_in = []
                    buffer_out = []
                else:
                    in_batches += in_paths[:-1]
                    out_batches += out_paths[:-1]
                    buffer_in = in_paths[-1]
                    buffer_out = out_paths[-1]

        if buffer_in:
            in_batches += [buffer_in]
            out_batches += [buffer_out]

        return in_batches, out_batches

    def predict(self, device='cuda'):
        device = torch.device(device)
        self.model.to(device)

        batched_input_paths, batched_output_paths = self.create_batched_input_and_output_paths()
        total_iterations = len(batched_input_paths)

        print('\n\nStarting prediction of preprocessed images...')
        with torch.no_grad():
            for i, (in_batch_paths, out_batch_paths) in enumerate(zip(batched_input_paths, batched_output_paths)):
                print(
                    f'Process: {i + 1}/{total_iterations} '
                    f'({(i + 1) / total_iterations * 100:.1f}%)', end='\r'
                )
                input_batch = self.load_images_to_tensor(in_batch_paths)
                input_batch = input_batch.to(device)
                out_logits = self.model(input_batch)
                self.save_out_batch(out_logits, out_batch_paths)
        print('\n')
        self.create_segmented_images()
