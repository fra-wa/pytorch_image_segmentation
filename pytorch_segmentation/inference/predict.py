import os
import torch

from pytorch_segmentation.inference.sliding_predictors.sliding_image_predictor import SlidingImagePredictor

from .. import constants
from ..file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.models.loading import load_model_from_checkpoint


def predict(model_path,
            data_folder,
            batch_size=4,
            input_size=512,
            device='cuda',
            decode_to_color=True,
            nice_color=True,
            overlap=0,
            ):
    model, image_channels, dataset_mean, dataset_std = load_model_from_checkpoint(model_path, device, False)

    if model.__class__.__name__ in constants.TWO_D_NETWORKS:
        predictor = SlidingImagePredictor(
            data_folder,
            input_size,
            model,
            batch_size,
            image_channels,
            decode_to_color=decode_to_color,
            nice_color=nice_color,
            overlap=overlap,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
    else:
        raise RuntimeError('The loaded model is not one of the preconfigured models.')

    if device == 'cuda' or device == torch.device('cuda'):
        if torch.cuda.is_available():
            device = torch.device('cuda')
        else:
            print('Cuda is not available on your system. Using cpu.')
            device = torch.device('cpu')

    elif device == 'cpu' or device == torch.device('cpu'):
        device = torch.device('cpu')

    predictor.predict(device)


def start_prediction(parameters, interact_with_user=True):
    if interact_with_user:
        print('Please set the model path and the folder! Other values are good by default but you can play around.\n'
              'The quality of prediction will not change but you might speed up the process.')
        params = parameters.start_interaction_guide()
    else:
        try:
            params = parameters.get_fixed_parameters()
        except AttributeError:
            # already got fixed parameters
            params = parameters

    model_path = params.model_path
    data_folder = params.data_folder
    batch_size = params.batch_size
    input_size = params.input_size
    depth_3d = params.depth_3d
    device = params.device
    decode_to_color = params.save_color
    overlap = params.overlap

    if not os.path.isfile(model_path):
        raise ValueError('Could not find model path')
    if not os.path.isdir(data_folder):
        raise ValueError('Could not find folder to predict')
    if not get_file_paths_in_folder(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
        raise ValueError(
            f"Could not find images in folder. Images must be of type: "
            f"{', '.join([constants.SUPPORTED_INPUT_IMAGE_TYPES])}!")

    predict(model_path, data_folder, batch_size, input_size, depth_3d, device, decode_to_color, overlap)
