import cv2
import math
import numpy as np
import os
import torch

from pytorch_segmentation.file_handling.read import read_binary_to_numpy_array
from pytorch_segmentation.file_handling.write import save_numpy_array_to_binary
from pytorch_segmentation.inference.predictor_base import SlicedImage
from pytorch_segmentation.inference.sliding_predictors.predictor_base_sliding_window import SlidingPredictorBase
from pytorch_segmentation.multicore_utils import slice_list_equally


class SlidingImageCalculator:

    def __init__(self, classes, height, width):
        self.logits_image = np.zeros((classes, height, width), dtype=np.float32)
        self.counting_image = np.zeros((classes, height, width), dtype=np.uint16)

    def add_current_logits(self, logits, h_start, w_start, padding):
        """

        Args:
            logits: current logits representing a sub image
            h_start: start coordinate in height
            w_start: start coordinate in width
            padding: first layer padding -> will be removed from calculation

        Returns:

        """
        # Todo: the cunt volume can be replaced by a different weighting strategy
        count_volume = np.ones_like(logits, dtype=self.counting_image.dtype)
        n_logits, h_logits, w_logits = logits.shape

        h_end = h_start + h_logits
        w_end = w_start + w_logits

        # zero out padding values
        if h_start != 0:
            count_volume[:, 0:padding, :] = 0
        if h_end != self.logits_image.shape[1]:
            count_volume[:, h_logits - padding:, :] = 0

        if w_start != 0:
            count_volume[:, :, 0:padding] = 0
        if w_end != self.logits_image.shape[2]:
            count_volume[:, :, w_logits - padding:] = 0

        padding_removed_logits = count_volume * logits

        self.logits_image[:, h_start:h_end, w_start:w_end] += padding_removed_logits
        self.counting_image[:, h_start:h_end, w_start:w_end] += count_volume

    def get_avg_logits_volume(self):
        return np.divide(
            self.logits_image,
            self.counting_image,
            out=np.zeros(shape=self.logits_image.shape, dtype=np.float32),
            where=self.counting_image > 0)


class SlidingImagePredictor(SlidingPredictorBase):

    def __init__(self,
                 folder,
                 input_size,
                 model,
                 batch_size,
                 channels=1,
                 testcase=False,
                 decode_to_color=True,
                 nice_color=True,
                 overlap=0,
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """
        Predicts small sub volumes of shape: input_size x input_size x depth

        Args:
            folder: folder of the volume
            input_size: input size of the model
            model: the model
            batch_size: in this case: how many images will be predicted simultaneously
            channels: 1 for gray, 3 for rgb, ..
            testcase: true to mute multiprocess print outs while testing
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """
        super(SlidingImagePredictor, self).__init__(
            folder,
            input_size,
            model,
            channels,
            testcase,
            decode_to_color,
            nice_color=nice_color,
            overlap=overlap,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )

        self.total_volumes = len(self.image_stacks)
        self.batch_size = batch_size

        for sliced_image in self.image_stacks:
            sliced_image.slice_into_parts(self.batch_size)

    def load_images_to_tensor(self, image_paths):
        images = []
        # if else outside to speed up loop
        if self.channels == 1:
            for image in image_paths:
                # load img, convert to tensor, add to list
                # each img is transformed to be shaped like [C, H, W]
                images.append(self.img_transform(cv2.imread(image, cv2.IMREAD_GRAYSCALE)))
        elif self.channels == 3:
            for image in image_paths:
                # converting bgr to rgb
                images.append(self.img_transform(cv2.cvtColor(cv2.imread(image, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)))
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        # [B, C, H, W]
        return torch.stack(images)

    @staticmethod
    def decode_logits(out_logits, classes):
        """Assumption: Only one class is true not multiple"""
        if isinstance(out_logits, np.ndarray):
            out_logits = out_logits.astype(np.float32)
            out_logits = torch.from_numpy(out_logits)

        if classes == 1:
            # binary classification, out_logits of shape: B, C, H, W
            out = torch.sigmoid(out_logits)
            segmentation_map = (out.squeeze(dim=1) > 0.5).float().cpu().numpy()
            classes = 2

        else:
            # out_logits of shape: B, C, H, W
            values, predictions = torch.max(out_logits, dim=1)
            # predictions of shape: B, H, W
            segmentation_map = predictions.cpu().numpy()

        return segmentation_map, classes

    @staticmethod
    def save_out_batch(out_logits, out_paths):
        for i, path in enumerate(out_paths):
            save_numpy_array_to_binary(out_logits[i], path)

    def create_batched_input_and_output_paths(self):
        in_batches = []
        out_batches = []

        buffer_in = []
        buffer_out = []

        for sliced_image in self.image_stacks:
            buffer_in += sliced_image.images
            buffer_out += sliced_image.predicted_images

            if len(buffer_in) >= self.batch_size:
                add_all = True if (len(buffer_in) / self.batch_size).is_integer() else False
                batched_parts = math.ceil(len(buffer_in) / self.batch_size)

                in_paths = slice_list_equally(buffer_in, batched_parts)
                out_paths = slice_list_equally(buffer_out, batched_parts)

                if add_all:
                    in_batches += in_paths
                    out_batches += out_paths
                    buffer_in = []
                    buffer_out = []
                else:
                    in_batches += in_paths[:-1]
                    out_batches += out_paths[:-1]
                    buffer_in = in_paths[-1]
                    buffer_out = out_paths[-1]

        if buffer_in:
            in_batches += [buffer_in]
            out_batches += [buffer_out]

        return in_batches, out_batches

    def combine_sub_logits_to_mask(self, binary_paths):
        height_list = []
        width_list = []

        for binary in binary_paths:
            w_start, h_start = os.path.basename(binary).split('width')[1].split('height')
            height_list.append(int(h_start.split('.')[0]))
            width_list.append(int(w_start))

        heights = sorted(list(set(height_list)))
        widths = sorted(list(set(width_list)))

        rows = len(heights)
        columns = len(widths)

        # create a list holding the logits for every row and column. Example with 9 images:
        # aligned_logits = [
        #       [bin_top_left,      bin_top_middle,     bin_top_right],
        #       [bin_mid_left,      bin_middle,         bin_mid_right],
        #       [bin_bottom_left,   bin_bottom_middle,  bin_bottom_right],
        #   ]
        # initialize here since loaded images are not sorted in correct order due to naming
        aligned_logits = [columns * [np.array(1)] for _ in range(rows)]
        for binary in binary_paths:
            w_start, h_start = os.path.basename(binary).split('width')[1].split('height')
            h_start = int(h_start.split('.')[0])
            row = heights.index(h_start)
            column = widths.index(int(w_start))
            logits = read_binary_to_numpy_array(binary)
            aligned_logits[row][column] = logits

        # prepare out mask
        out_height = max(heights) + self.input_size
        out_width = max(widths) + self.input_size

        _, final_conv = self.model.get_last_layer_information()
        sliding_window = SlidingImageCalculator(final_conv.out_channels, out_height, out_width)

        # now combine the aligned logits
        for row_idx, row in enumerate(aligned_logits):
            h_start = heights[row_idx]
            for col_idx, column in enumerate(row):
                w_start = widths[col_idx]

                sliding_window.add_current_logits(
                    aligned_logits[row_idx][col_idx],
                    h_start,
                    w_start,
                    self.first_layer_padding,
                )

        out_logits = sliding_window.get_avg_logits_volume()
        out_logits = np.expand_dims(out_logits, 0)  # add batch information to decode properly
        segmentation_map, classes = self.decode_logits(out_logits, final_conv.out_channels)
        out_mask = self.decode_segmentation_map(segmentation_map, classes)[0]  # batch size is 1 in this case

        return out_mask

    def create_segmented_images(self):

        print('Recreating images from predictions...')
        for idx, sliced_image in enumerate(self.image_stacks):
            print(
                f'Process: {idx + 1}/{len(self.image_stacks)} ({(idx + 1) / len(self.image_stacks) * 100:.1f}%)',
                end='\r'
            )
            assert isinstance(sliced_image, SlicedImage)
            combined_out_img = self.combine_sub_logits_to_mask(sliced_image.predicted_images)

            file_name = sliced_image.img_base_name
            file_name = file_name + '_mask.png'
            file_name = os.path.join(self.out_folder, file_name)
            cv2.imwrite(file_name, combined_out_img)

        self.remove_temp_folder_if_not_testcase()
        print('\nFinished prediction!')

    def predict(self, device='cuda'):
        device = torch.device(device)
        self.model.to(device)

        batched_input_paths, batched_output_paths = self.create_batched_input_and_output_paths()
        total_iterations = len(batched_input_paths)

        print('Starting prediction of preprocessed volume...')
        with torch.no_grad():
            for i, (in_batch_paths, out_batch_paths) in enumerate(zip(batched_input_paths, batched_output_paths)):
                print(
                    f'Process: {(i + 1)}/{total_iterations} '
                    f'({(i + 1) / total_iterations * 100:.1f}%)', end='\r'
                )

                in_batch = self.load_images_to_tensor(in_batch_paths)
                in_batch = in_batch.to(device)
                out_logits = self.model(in_batch)
                out_logits = out_logits.float().cpu().numpy()
                self.save_out_batch(out_logits, out_batch_paths)

        print('\n')
        self.create_segmented_images()
