import os

import torch.nn

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference.predictor_base import PredictorBase, multiprocess_images, SlicedVolume, SlicedImage
from pytorch_segmentation.multicore_utils import multiprocess_function
from pytorch_segmentation.multicore_utils import slice_multicore_parts
from pytorch_segmentation.utils import get_max_threads


class SlidingPredictorBase(PredictorBase):
    def __init__(self,
                 folder,
                 input_size,
                 model,
                 channels=1,
                 testcase=False,
                 decode_to_color=True,
                 nice_color=True,
                 overlap=0,
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """

        Args:
            folder: folder containing images of the volume or 2D images
            input_size: input size of images to pass in (the model should take any squared input size)
            model:
            channels: image channels of the data to predict
            testcase: only to mute output while testing: simply redirecting stdout does not work at multiprocessing
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            nice_color: False results in a simpler decoded map
            overlap: overlap in percentage (for h, w)
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """
        super(SlidingPredictorBase, self).__init__(
            folder,
            input_size,
            model,
            channels,
            testcase,
            decode_to_color,
            nice_color,
            dataset_mean=dataset_mean,
            dataset_std=dataset_std,
        )
        if 0 > overlap >= 100:
            raise ValueError(f'Overlap percentage must be 0 or greater and smaller than 100!')

        self.image_stacks = []
        self.first_layer_padding = self.first_layer_conv.padding[0]
        self.overlap = overlap  # the padding will be cut away in every case. You can additionally add an overlap here.

        if isinstance(self.first_layer_conv, torch.nn.Conv3d):
            assert self.first_layer_conv.padding[0] == self.first_layer_conv.padding[1]
            assert self.first_layer_conv.padding[0] == self.first_layer_conv.padding[2]
            self.prepare_sub_stacks_3d()
        else:
            assert self.first_layer_conv.padding[0] == self.first_layer_conv.padding[1]
            self.prepare_sub_stacks_2d()

    def prepare_input_images(self, threads=None):
        """
        Args:
            threads (int): you can set the used threads by yourself

        Returns: prepared image paths

        """
        if threads is None:
            threads = get_max_threads()

        image_paths = get_file_paths_in_folder(self.folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

        image_paths_map = slice_multicore_parts(image_paths, total_maps=threads)
        target_folder_map = len(image_paths_map) * [self.temp_in_folder]
        in_size_map = len(image_paths_map) * [self.input_size]
        process_nr_map = [i + 1 for i in range(len(image_paths_map))]
        testcase_map = [self.testcase for i in range(len(image_paths_map))]
        original_folder_map = len(image_paths_map) * [None]
        overlap_h_w = round((self.input_size - 2 * self.first_layer_padding) * self.overlap / 100)
        overlap_h_map = len(image_paths_map) * [self.first_layer_padding + 1 + overlap_h_w]
        overlap_w_map = len(image_paths_map) * [self.first_layer_padding + 1 + overlap_h_w]

        maps = [
            image_paths_map,
            target_folder_map,
            in_size_map,
            process_nr_map,
            testcase_map,
            original_folder_map,
            overlap_h_map,
            overlap_w_map,
        ]

        print('Preprocessing images for model')
        print(f'Some images might be enlarged to fit the model input size.'
              f'\nIf so, the original data will be moved to: {self.original_folder}')
        results = multiprocess_function(multiprocess_images, maps)
        print('')

        new_image_paths = []
        for image_paths in results:
            new_image_paths += image_paths

        return sorted(new_image_paths)

    def prepare_sub_stacks_3d(self):
        """
        This can be used for volume or sequence prediction.
        Assumption: each image has the same height and width

        Prepares the image stack of self.folder for the self.model.
        This function will create temporary folders for in and out

        Returns: longitudinal split sub stacks of the real img slices (ImageStack instances for each sub stack)

        """
        new_image_paths = self.prepare_input_images()

        image_stacks = [SlicedVolume() for i in range(len(new_image_paths[0]))]
        for split_images in new_image_paths:
            for idx, img in enumerate(sorted(split_images)):
                w_start, h_start = os.path.basename(img).split('width')[1].split('height')
                image_stacks[idx].w_start = int(w_start)
                image_stacks[idx].h_start = int(h_start.split('.')[0])
                image_stacks[idx].images.append(img)
                out_path = self.get_out_path_of_in_path(img).split('.')[0] + '.bin'
                image_stacks[idx].predicted_images.append(out_path)

        self.image_stacks = image_stacks

    def prepare_sub_stacks_2d(self):
        """
        This is used for standard prediction. For 3D, you can use the optimized version for 3D.
        Assumption: The images in the folder are not of same size (but can)
        When using this, you need a custom image recreation. Example at: SimplePredictor

        Prepares the image stack of self.folder for the self.model.
        This function will create temporary folders for in and out

        Returns: longitudinal split sub stacks of the real img slices (ImageStack instances for each sub stack)

        """
        new_image_paths = self.prepare_input_images()

        image_stacks = [SlicedImage() for i in range(len(new_image_paths))]

        for idx, split_images in enumerate(new_image_paths):
            for img in sorted(split_images):
                w_start, h_start = os.path.basename(img).split('width')[1].split('height')
                image_stacks[idx].w_start_list.append(int(w_start))
                image_stacks[idx].h_start_list.append(int(h_start.split('.')[0]))
                image_stacks[idx].images.append(img)
                out_path = self.get_out_path_of_in_path(img).split('.')[0] + '.bin'
                image_stacks[idx].predicted_images.append(out_path)

        self.image_stacks = image_stacks

    def predict(self, device='cuda'):
        raise NotImplementedError('implement at your class')
