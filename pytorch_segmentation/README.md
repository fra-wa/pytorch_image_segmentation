This project is a reduced version of https://gitlab.com/fra-wa/pytorch_segmentation (maybe not published, ask for 
permission). The code might have many references to 3d data, but I dropped the 3d networks and 3d/rnn support since it 
is not part of the publication.